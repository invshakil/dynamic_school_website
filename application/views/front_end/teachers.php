<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="sub_banner_hdg">
                    <h3>Our Teachers</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ct_breadcrumb">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Our Teachers</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <?php $teachers = $this->db->get('teacher')->result();
            foreach ($teachers as $row) {
            ?>
            <div class="col-md-3 col-sm-4 col-xs-12" >
                <div class="ct_teacher_outer_wrap" style="height: 320px;">
                    <figure>
                        <img alt="" src="<?php echo $this->crud_model->get_image_url('teacher',$row->teacher_id);?>" width="200">
                    </figure>
                    <div class="ct_teacher_wrap">
                        <h5><a href="#"><?php echo $row->name;?></a></h5>
                        <span><?php echo $row->designation.' / '.$row->phone;?></span>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php } ?>

        </div>
</section>