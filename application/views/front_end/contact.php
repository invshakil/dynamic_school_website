<section class="sub_banner_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub_banner_hdg" style="text-align: center;">
                    <h3>Admission Information</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section>
                    <div class="container">
                        <div class="get_touch_wrap">
                            <h4>GET IN TOUCH WITH US:</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ct_contact_form">
                                    <form>
                                        <div class="form_field">
                                            <label class="fa fa-user"></label>
                                            <input class="conatct_plchldr" type="text" placeholder="Your Name">
                                        </div>
                                        <div class="form_field">
                                            <label class="fa fa-envelope-o"></label>
                                            <input class="conatct_plchldr" type="text" placeholder="Email Address">
                                        </div>
                                        <div class="form_field">
                                            <label class="fa fa-edit"></label>
                                            <textarea class="conatct_plchldr" placeholder="Write Detail"></textarea>
                                        </div>
                                        <div class="form_field">
                                            <button>Send Now <i class="fa fa-arrow-right"></i> </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="bottom_border">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="ct_contact_address">
                                                <h5><i class="fa fa-map-o"></i>Address</h5>
                                                <p><?php $address = $this->db->get_where('system_settings', array('info_type' => 'school_address'))->row();?>
                                                    <?php echo $address->description;?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="ct_contact_address">
                                                <h5><i class="fa fa-envelope-o"></i>Contact</h5>
                                                <ul class="fax_info">
                                                    <?php $contact = $this->db->get_where('system_settings', array('info_type' => 'school_contact'))->row();?>
                                                    <p>Contact: <?php echo $contact->description;?></p>
                                                    <?php $email = $this->db->get_where('system_settings', array('info_type' => 'school_email'))->row();?>
                                                    <p>Email: <?php echo $email->description;?></p>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>

            </div>
        </div>


    </div>
</div>