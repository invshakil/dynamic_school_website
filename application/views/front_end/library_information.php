<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section class="sub_banner_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="sub_banner_hdg" style="text-align: center;">
                                    <h3>Library Information</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="15%">Book</th>
                            <th width="30%">Description</th>
                            <th width="20%">Author</th>
                            <th width="20%">Class</th>
                            <th width="15%">Price</th>
                            <th width="10%">status</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $library = $this->db->get_where('book',array('status' => 'available'))->result();

                        foreach ($library as $row) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $row->book_id;?></th>
                            <th scope="row"><?php echo $row->name;?></th>
                            <th scope="row"><?php echo $row->description;?></th>
                            <th scope="row"><?php echo $row->author;?></th>
                            <th scope="row"><?php $id = $row->class_id; echo $this->db->get_where('class',array('class_id' => $id))->row('name');?></th>
                            <th scope="row"><?php echo $row->price.' TK';?></th>
                            <td><a class="btn btn-success">Available</a></td>
                        </tr>

                        <?php } ?>


                        </tbody>
                    </table>
            </div>
        </div>

        <div class="col-md-4">
            <aside class="gt_aside_outer_wrap">
                <!--Category Wrap Start-->
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>More Pages</h5>
                    <ul>
                        <li><a href="http://amarpathshala.com" target="_blank">Amar Pathshala</a></li>
                        <li><a href="<?php echo base_url() ?>our-teachers">Our Teachers</a></li>
                        <li><a href="<?php echo base_url() ?>notices">Notices</a></li>
                        <li><a href="<?php echo base_url() ?>class-routine">Class Routine</a></li>
                        <li><a href="<?php echo base_url() ?>gallery">Gallery</a></li>
                    </ul>
                </div>
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>School Hours</h5>
                    <div>
                        <?php $full = $this->db->get_where('system_settings', array('info_type' => 'school_hour_full'))->row();?>
                        <p><?php echo $full->description;?></p>

                        <?php $half = $this->db->get_where('system_settings', array('info_type' => 'school_hour_half'))->row();?>
                        <p><?php echo $half->description;?></p>
                        <p></p>

                        <?php $holiday = $this->db->get_where('system_settings', array('info_type' => 'school_holiday'))->row();?>
                        <p><?php echo $holiday->description;?>: CLOSED</p>
                    </div>
                </div>

            </aside>

        </div>

    </div>
</div>