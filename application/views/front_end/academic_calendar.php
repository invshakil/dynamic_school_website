
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section class="sub_banner_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="sub_banner_hdg" style="text-align: center;">
                                    <h3>Academic Calendar</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <?php $logo = $this->db->get_where('academic_calendar', array('info_type' => 'image' ))->row();
                    if ($logo->description != '') { ?>
                    <img src="<?php echo base_url() . $logo->description ?>" alt="...">
                    <?php } else { ?>
                        <img src="http://placehold.it/1200x1600" alt="...">
                    <?php } ?>
                </section>
                    
            </div>
        </div>

        <div class="col-md-4">
            <aside class="gt_aside_outer_wrap">
                <!--Category Wrap Start-->
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>More Pages</h5>
                    <ul>
                        <li><a href="http://amarpathshala.com" target="_blank">Amar Pathshala</a></li>
                        <li><a href="<?php echo base_url()?>our-teachers">Our Teachers</a></li>
                        <li><a href="<?php echo base_url()?>notices">Notices</a></li>
                        <li><a href="<?php echo base_url()?>class-routine">Class Routine</a></li>
                        <li><a href="<?php echo base_url()?>gallery">Gallery</a></li>
                    </ul>
                </div>
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>Admission Links</h5>
                    <ul>
                        <li><a href="<?php echo base_url()?>admission-information">Admission Information</a></li>
                        <li><a href="<?php echo base_url()?>admission-result">Admission Test Result</a></li>
                        <li><a href="<?php echo base_url()?>fees-and-payment">Fees And Payment</a></li>
                        <li><a href="<?php echo base_url()?>scholarship-information">Scholarship Information</a></li>
                    </ul>
                </div>

            </aside>

        </div>

    </div>
</div>