
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section class="sub_banner_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="sub_banner_hdg" style="text-align: center;">
                                    <h3>Scholarship Information</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <?php $title = $this->db->get_where('scholarship_information', array('info_type' => 'title'))->row(); ?>
                    <br/>
                    <h3><?php echo $title->description; ?></h3>
                    <br/>
                    <?php $msg = $this->db->get_where('scholarship_information', array('info_type' => 'desc'))->row(); ?>
                    <p align="justify"><?php echo $msg->description; ?></p>

                    <?php $file = $this->db->get_where('scholarship_information', array('info_type' => 'file'))->row();?>
                    <p><b>File:</b> <a class="btn btn-primary" target="_blank" href="<?php echo base_url() . $file->description ?>">Download
                            File</a></p>
                </section>

            </div>
        </div>

        <div class="col-md-4">
            <aside class="gt_aside_outer_wrap">
                <!--Category Wrap Start-->
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>More Pages</h5>
                    <ul>
                        <li><a href="<?php echo base_url()?>our-teachers">Our Teachers</a></li>
                        <li><a href="<?php echo base_url()?>notices">Notices</a></li>
                        <li><a href="<?php echo base_url()?>class-routine">Class Routine</a></li>
                        <li><a href="<?php echo base_url()?>gallery">Gallery</a></li>
                    </ul>
                </div>
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>School Hours</h5>
                    <div>
                        <?php $full = $this->db->get_where('system_settings', array('info_type' => 'school_hour_full'))->row();?>
                        <p><?php echo $full->description;?></p>

                        <?php $half = $this->db->get_where('system_settings', array('info_type' => 'school_hour_half'))->row();?>
                        <p><?php echo $half->description;?></p>
                        <p></p>

                        <?php $holiday = $this->db->get_where('system_settings', array('info_type' => 'school_holiday'))->row();?>
                        <p><?php echo $holiday->description;?>: CLOSED</p>
                    </div>
                </div>

            </aside>

        </div>

    </div>
</div>