<!DOCTYPE html>
<html lang="en">
  <head>

      <?php echo $header;?>
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <header>
    	<!--Top Strip Wrap Start-->
        <div class="top_strip">
            <?php echo $banner_top;?>
        </div>
        <!--Top Strip Wrap End-->
        
        <!--Navigation Wrap Start-->
        <div class="logo_nav_outer_wrap">
        	<?php echo $navigation?>
        </div>
        <!--Navigation Wrap End-->
    </header>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <div class="banner_outer_wrap">
        <?php echo $slider;?>
    </div>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <?php echo $main_content?>

    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <footer>
        <?php echo $footer_content;?>

    </footer>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



   <?php echo $footer_script;?>

  </body>
</html>
