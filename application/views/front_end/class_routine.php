<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section class="sub_banner_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sub_banner_hdg" style="text-align: center;">
                                    <h3>Class Routine</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <a href="javascript:window.print();" class="btn btn-primary pull-right">
                        Save Class Routine As PDF
                        <i class="fa fa-print"></i>
                    </a>
                </section>
                <section>

                        <?php
                        $toggle = true;
                        $classes = $this->db->get('class')->result_array();
                        foreach ($classes as $row):
                            ?>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-2"
                                           href="#collapse<?php echo $row['class_id']; ?>">
                                            <i class="entypo-rss"></i>
                                            <?php $id = $row['class_id'];
                                            echo $this->db->get_where('class', array('class_id' => $id))->row('name'); ?>
                                        </a>
                                    </h4>
                                </div>

                                <div id="collapse<?php echo $row['class_id']; ?>"
                                     class="panel-collapse collapse <?php if ($toggle) {
                                         echo 'in';
                                         $toggle = false;
                                     } ?>">
                                    <div class="panel-body table-responsive">
                                        <?php $id = $row['class_id'];
                                        $section = $this->db->get_where('section', array('class_id' => $id))->result_array();
                                        foreach ($section as $sec) {
                                            ?>

                                            <h4><i class="entypo-rss"></i>
                                                <?php $id = $sec['section_id'];
                                                echo $this->db->get_where('section', array('section_id' => $id))->row('section_name'); ?>
                                            </h4>
                                            <hr/>
                                            <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
                                                <tbody>
                                                <?php
                                                for ($d = 1; $d <= 7; $d++):

                                                    if ($d == 1) $day = 'sunday';
                                                    else if ($d == 2) $day = 'monday';
                                                    else if ($d == 3) $day = 'tuesday';
                                                    else if ($d == 4) $day = 'wednesday';
                                                    else if ($d == 5) $day = 'thursday';
                                                    else if ($d == 6) $day = 'friday';
                                                    else if ($d == 7) $day = 'saturday';
                                                    ?>
                                                    <tr class="gradeA">
                                                        <td width="100"><?php echo strtoupper($day); ?></td>
                                                        <td>
                                                            <?php

                                                            $this->db->where('day', $day);
                                                            $this->db->where('class_id', $row['class_id']);
                                                            $routines = $this->db->get_where('class_routine', array('section_id' => $sec['section_id']))->result_array();
                                                            foreach ($routines as $row2):
                                                                ?>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-primary dropdown-toggle"
                                                                            data-toggle="dropdown">
                                                                        <?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']); ?>
                                                                        <?php echo '(' . $row2['time_start'] . '-' . $row2['time_end'] . ')'; ?>

                                                                    </button>
                                                                </div>
                                                            <?php endforeach; ?>

                                                        </td>
                                                    </tr>
                                                <?php endfor; ?>

                                                </tbody>
                                            </table>

                                        <?php } ?>

                                    </div>
                                </div>
                            </div>&nbsp;
                            <?php
                        endforeach;
                        ?>
            </div>
        </div>



    </div>
</div>
