<section>
    <div class="container">
        <div class="row">

            <!--AJAX REQUEST WILL LOAD THE CONTENT -->

            <div id="results"></div>
            <div class="loader_image text-center" style="display:none">
                <p><img src="<?php echo base_url()?>webAssets/images/bx_loader.gif" height="50px">Loading More image</p>
            </div>

        </div>
    </div>
</section>

<?php include 'modal.php';?>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var total_record = 0;
        var total_groups = <?php echo $total_data; ?>;


        $('#results').load("<?php echo base_url() ?>welcome/load_more_image",
            {'group_no': total_record}, function () {
                total_record++;
            });
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (total_record <= total_groups) {
                    loading = true;
                    $(document).ready(function () {
                        setTimeout(function () {
                            $('.loader_image').show();
                        }, 1000);
                    });

                    $('.loader_image').show();
                    $.post('<?php echo site_url() ?>welcome/load_more_image', {'group_no': total_record},
                        function (data) {
                            if (data != "") {
                                $("#results").append(data);
                                $('.loader_image').hide();
                                total_record++;
                            }

                        });
                }
                else
                {
                    $('.loader_image').html("<h3>No more image found!</h3>");
                }
            }
        });
    });
</script>