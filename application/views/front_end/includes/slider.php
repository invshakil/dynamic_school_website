<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <ul class="main_slider">

                <?php $slider_image = $this->db->limit(3)
                    ->order_by('image_id','desc')
                    ->get_where('image_gallery', array('slider_status' => 1))
                    ->result_array();
                foreach ($slider_image as $row) {
                    ?>

                    <li >
                        <img src="<?php echo base_url().$row['image']?>" class="img-thumbnail img-responsive" alt="">
                        <div class="ct_banner_caption" style="padding-left: 50px">
                            <?php $name = $this->db->get_where('system_settings', array('info_type' => 'school_name'))->row();?>
                            <h4 class="fadeInDown">WELCOME TO <span><?php echo $name->description;?></span></h4>

                            <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'welcome_message'))->row();?>
                            <span class="fadeInDown"><?php echo $msg->description;?></span>
                            <p></p>
                            <a class="active fadeInDown" href="<?php echo base_url()?>admission-information">ADMISSION</a>
                            <a class="active fadeInDown" href="<?php echo base_url()?>gallery">GALLERY</a>
                        </div>
                    </li>

                <?php } ?>

            </ul>
        </div>
    </div>
</div>
