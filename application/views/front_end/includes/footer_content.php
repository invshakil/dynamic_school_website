<!--Footer Col Wrap Start-->
<div class="ct_footer_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer_col_1 widget">
                    <p>Meet our profesionals they are passonate about. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer adipis elit, aliquam eget nibh etlibura.</p>
                    <span>Email : info@techno-71.com</span>
                    <span>Contact : +88 01977 304 386</span>
                    <div class="foo_get_qoute">
                        <a href="#">GET A Touch</a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="foo_col_2 widget">
                    <h5>More Pages</h5>
                    <ul>
                        <li><a href="http://amarpathshala.com" target="_blank">Amar Pathshala</a></li>
                        <li><a href="<?php echo base_url()?>our-teachers">Our Teachers</a></li>
                        <li><a href="<?php echo base_url()?>notices">Notices</a></li>
                        <li><a href="<?php echo base_url()?>class-routine">Class Routine</a></li>
                        <li><a href="<?php echo base_url()?>gallery">Gallery</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="foo_col_2 widget">
                    <h5>Admission Links</h5>
                    <ul>
                        <li><a href="<?php echo base_url()?>admission-information">Admission Information</a></li>
                        <li><a href="<?php echo base_url()?>admission-result">Admission Test Result</a></li>
                        <li><a href="<?php echo base_url()?>fees-and-payment">Fees And Payment</a></li>
                        <li><a href="<?php echo base_url()?>scholarship-information">Scholarship Information</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="foo_col_4 widget">
                    <h5>School Hours</h5>
                    <ul>
                        <?php $full = $this->db->get_where('system_settings', array('info_type' => 'school_hour_full'))->row();?>
                        <li><?php echo $full->description;?></li>
                        <?php $half = $this->db->get_where('system_settings', array('info_type' => 'school_hour_half'))->row();?>
                        <li><?php echo $half->description;?></li>
                        <li></li>
                        <?php $holiday = $this->db->get_where('system_settings', array('info_type' => 'school_holiday'))->row();?>
                        <li><?php echo $holiday->description;?>: CLOSED</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!--Footer Col Wrap End-->

<!--Footer Copyright Wrap Start-->
<div class="ct_copyright_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="copyright_text">
                    Built with <a class="fa fa-heart" href="#"></a> from <a href="#">Techno 71</a>. All right reserved
                </div>
            </div>
            <div class="col-md-6">
                <div class="copyright_social_icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer Copyright Wrap End-->
<div class="back_to_top">
    <a href="#"><i class="fa fa-angle-up"></i></a>
</div>