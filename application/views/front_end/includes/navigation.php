<div class="container">


    <div class="logo_wrap">
        <?php $logo = $this->db->get_where('system_settings', array('info_type' => 'school_logo' ))->row(); ?>
        <a href="<?php echo base_url();?>"><img src="<?php echo base_url().$logo->description;?>"  width="120" alt=""></a>
    </div>

    <div class="top_search_wrap" style="padding-top:5px;">
        <i class="fa fa-search search-fld"></i>
        <div class="search-wrapper-area">
            <form class="search-area">
                <input type="text" placeholder="search here"/>
                <input type="submit" value="Go"/>
            </form>
        </div>
    </div>

    <nav class="main_navigation" style="padding-top:5px;">
        <ul>
            <li><a href="<?php echo base_url();?>">Home</a></li>

            <li><a href="<?php echo base_url()?>our-teachers">Teachers</a></li>
            <li><a href="#">Academic Information</a>
                <ul>
                    <li><a href="<?php echo base_url()?>notices">Notices</a></li>
                    <li><a href="<?php echo base_url()?>class-routine">Class Routine</a></li>
                    <li><a href="<?php echo base_url()?>academic-calendar">Academic Calendar</a></li>
                    <li><a href="<?php echo base_url()?>library-information">Library Information</a></li>
                </ul>
            </li>
            <li><a href="#">Admission</a>
                <ul>
                    <li><a href="<?php echo base_url()?>admission-information">Admission Information</a></li>
                    <li><a href="<?php echo base_url()?>admission-result">Admission Test Result</a></li>
                    <li><a href="<?php echo base_url()?>fees-and-payment">Fees And Payment</a></li>
                    <li><a href="<?php echo base_url()?>scholarship-information">Scholarship Information</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url()?>gallery">Gallery</a></li>
            <li><a href="#">About Us</a>
                <ul>
                    <li><a href="<?php echo base_url()?>message-from-principal">Message From Principal</a></li>
                    <li><a href="<?php echo base_url()?>message-from-chairman">Message From Chairman</a></li>
                    <!--                    <li><a href="--><?php //echo base_url()?><!--governing-body">Governing Body</a></li>-->
                    <li><a href="<?php echo base_url()?>history-of-school">History of School</a></li>
                    <!--                    <li><a href="--><?php //echo base_url()?><!--former-headmasters">Former Headmaster</a></li>-->
                </ul>
            </li>
            <li><a href="<?php echo base_url()?>contact">Contact</a></li>
        </ul>

    </nav>


    <!--DL Menu Start-->
    <div id="kode-responsive-navigation" class="dl-menuwrapper">
        <button class="dl-trigger">Open Menu</button>
        <ul class="dl-menu">
            <li class="active"><a href="<?php echo base_url();?>">Home</a></li>

            <li class="menu-item kode-parent-menu"><a href="<?php echo base_url()?>our_teachers">Teachers</a></li>

            <li class="menu-item kode-parent-menu"><a href="#">Academic Information</a>
                <ul class="dl-submenu">
                    <li><a href="<?php echo base_url()?>notices">Notices</a></li>
                    <li><a href="<?php echo base_url()?>class_routine">Class Routine</a></li>
                    <li><a href="<?php echo base_url()?>academic_calendar">Academic Calendar</a></li>
                    <li><a href="<?php echo base_url()?>library_information">Library Information</a></li>
                </ul>
            </li>
            <li class="menu-item kode-parent-menu"><a href="#">Admission</a>
                <ul class="dl-submenu">
                    <li><a href="<?php echo base_url()?>admission_information">Admission Information</a></li>
                    <li><a href="<?php echo base_url()?>admission_result">Admission Test Result</a></li>
                    <li><a href="<?php echo base_url()?>fees_and_payment">Fees And Payment</a></li>
                    <li><a href="<?php echo base_url()?>scholarship_information">Scholarship Information</a></li>
                </ul>
            </li>
            <li class="menu-item kode-parent-menu"><a href="<?php echo base_url()?>gallery">Gallery</a></li>
            <li class="menu-item kode-parent-menu"><a href="#">About Us</a>
                <ul class="dl-submenu">
                    <li><a href="<?php echo base_url()?>message_from_principal">Message From Principal</a></li>
                    <li><a href="<?php echo base_url()?>message_from_chairman">Message From Chairman</a></li>
                    <!--                    <li><a href="--><?php //echo base_url()?><!--governing_body">Governing Body</a></li>-->
                    <li><a href="<?php echo base_url()?>history_of_school">History of School</a></li>
                    <!--                    <li><a href="--><?php //echo base_url()?><!--former_headmasters">Former Headmaster</a></li>-->
                </ul>
            </li>
            <li class="menu-item kode-parent-menu"><a href="<?php echo base_url()?>contact">Contact</a></li>
        </ul>
    </div>
    <!--DL Menu END-->
</div>