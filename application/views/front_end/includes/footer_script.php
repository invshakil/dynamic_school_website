<!--Bootstrap core JavaScript-->
<script src="<?php echo base_url()?>webAssets/js/jquery.js"></script>
<script src="<?php echo base_url()?>webAssets/js/bootstrap.min.js"></script>
<!--Bx-Slider JavaScript-->
<script src="<?php echo base_url()?>webAssets/js/jquery.bxslider.min.js"></script>
<!--Dl Menu Script-->
<script src="<?php echo base_url()?>webAssets/js/dl-menu/modernizr.custom.js"></script>
<script src="<?php echo base_url()?>webAssets/js/dl-menu/jquery.dlmenu.js"></script>
<!--Owl Carousel JavaScript-->
<script src="<?php echo base_url()?>webAssets/js/owl.carousel.js"></script>
<!--Time Counter Javascript-->
<script src="<?php echo base_url()?>webAssets/js/jquery.downCount.js"></script>
<!--Pretty Photo Javascript-->
<script src="<?php echo base_url()?>webAssets/js/jquery.prettyPhoto.js"></script>
<!--Way Points Javascript-->
<script src="<?php echo base_url()?>webAssets/js/waypoints-min.js"></script>
<!--Custom JavaScript-->
<script src="<?php echo base_url()?>webAssets/js/custom.js"></script>