<div class="container">
    <div class="top_location_wrap">
        <?php $address = $this->db->get_where('system_settings', array('info_type' => 'school_address'))->row();?>
        <p><i class="fa fa-map-marker"></i><?php echo $address->description;?></p>
    </div>
    <div class="top_ui_element">
        <ul>
            <?php $email = $this->db->get_where('system_settings', array('info_type' => 'school_email'))->row();?>
            <li><i class="fa fa-envelope"></i><a href="#"><?php echo $email->description;?></a></li>

            <?php $contact = $this->db->get_where('system_settings', array('info_type' => 'school_contact'))->row();?>
            <li><i class="fa fa-phone"></i><?php echo $contact->description;?></li>
            <li><a class="btn btn-blue btn-sm" href="<?php echo base_url()?>software" target="_blank"><i class="fa fa-lock"> Software Login</i></a></li>
        </ul>
    </div>

</div>