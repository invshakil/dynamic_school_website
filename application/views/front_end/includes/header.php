<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>School Management ERP</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url() ?>webAssets/css/bootstrap.min.css" rel="stylesheet">
<!-- Bx-Slider StyleSheet CSS -->
<link href="<?php echo base_url() ?>webAssets/css/jquery.bxslider.css" rel="stylesheet">
<!-- Font Awesome StyleSheet CSS -->
<link href="<?php echo base_url() ?>webAssets/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url() ?>webAssets/css/svg-style.css" rel="stylesheet">
<!-- Pretty Photo CSS -->
<link href="<?php echo base_url() ?>webAssets/css/prettyPhoto.css" rel="stylesheet">
<!-- Widget CSS -->
<link href="<?php echo base_url() ?>webAssets/css/widget.css" rel="stylesheet">
<!-- DL Menu CSS -->
<link href="<?php echo base_url() ?>webAssets/js/dl-menu/component.css" rel="stylesheet">
<!-- Typography CSS -->
<link href="<?php echo base_url() ?>webAssets/css/typography.css" rel="stylesheet">
<!-- Animation CSS -->
<link href="<?php echo base_url() ?>webAssets/css/animate.css" rel="stylesheet">
<!-- Owl Carousel CSS -->
<link href="<?php echo base_url() ?>webAssets/css/owl.carousel.css" rel="stylesheet">
<!-- Shortcodes CSS -->
<link href="<?php echo base_url() ?>webAssets/css/shortcodes.css" rel="stylesheet">
<!-- Custom Main StyleSheet CSS -->
<link href="<?php echo base_url() ?>webAssets/style.css" rel="stylesheet">
<!-- Color CSS -->
<link href="<?php echo base_url() ?>webAssets/css/color.css" rel="stylesheet">
<!-- Responsive CSS -->
<link href="<?php echo base_url() ?>webAssets/css/responsive.css" rel="stylesheet">

<style type="text/css">
    .loader_image {
        padding: 10px 0px;
        width: 100%;
    }
</style