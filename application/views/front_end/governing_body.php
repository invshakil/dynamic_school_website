<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister1.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister2.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister3.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister4.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister5.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img src="<?php echo base_url()?>webAssets/images/minister6.jpg" alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a href="#">View Detail</a>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="popular_course_des">
                        <h5><a href="#">Mr. M R Hasan</a></h5>
                        <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec tetuer
                            adipis elit, aliquam eget nibhd hfdush etlibura.</p>

                    </div>
                </div>
            </div>



            <div class="ct_pagination">
                <ul>
                    <li>
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#" class="next">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>