<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <h3><?php echo $name;?></h3>
                <div align="justify"><?php echo $message;?></div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="img-responsive img-thumbnail">
                <img class="img-responsive img-responsive" src="<?php echo $image;?>" alt="">

            </div>

        </div>

    </div>

</div>