<!--Get Started Wrap Start-->
<section>
    <div class="container">
        <div class="get_started_outer_wrap">
            <div class="row">
                <div class="col-md-6">
                    <div class="get_started_content_wrap ct_blog_detail_des_list">
                        <h3>Principal's Message</h3>
                        <?php $msg = $this->db->get_where('principal_speech', array('info_type' => 'speech'))->row();?>
                        <?php echo $msg->description;?>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="img-responsive img-thumbnail">

                        <?php $logo = $this->db->get_where('principal_speech', array('info_type' => 'image' ))->row(); ?>
                        <img src="<?php echo base_url().$logo->description?>" alt="">

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php $notice = $this->db->order_by('notice_id','desc')->limit(3)->get('noticeboard')->result();
            foreach ($notice as $row) {
            ?>

                <div class="col-md-4 col-sm-12">
                <div class="get_started_services">
                    <div class="get_started_icon">
                        <i class="fa fa-television"></i>
                    </div>
                    <div class="get_icon_des">
                        <h5>NOTICE: <?php echo $row->notice_title;?></h5>
                        <p>
                            <?php

                            $str = $row->notice;
                            $length = strlen($str);
                            if ($length > 100) {
                                $strCut = substr($str, 0, 200);
                                $str = substr($strCut, 0, strrpos($strCut, " ")) . '...<a href='.base_url().'notices>View Details</a>';
                            }

                            echo $str;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>

    </div>
</section>
<!--Get Started Wrap End-->


<!--Figures & Facts Wrap Start-->
<section class="ct_facts_bg">
    <ul>
        <li>
            <i class="icon-avatar"></i>
            <?php $teacher = $this->db->get_where('system_settings', array('info_type' => 'total_teacher'))->row();?>
            <h2 class="counter"><?php echo $teacher->description;?></h2>
            <span>Certified Teachers</span>
        </li>
        <li>
            <i class="icon-command"></i>
            <?php $student = $this->db->get_where('system_settings', array('info_type' => 'total_student'))->row();?>
            <h2 class="counter"><?php echo $student->description;?></h2>
            <span>Students Enrolled</span>
        </li>
        <li>
            <i class="icon-open-book"></i>
            <?php $book = $this->db->get_where('system_settings', array('info_type' => 'total_books'))->row();?>
            <h2 class="counter"><?php echo $book->description;?></h2>
            <span>Library Collection</span>
        </li>
        <li>
            <i class="fa fa-university"></i>
            <?php $room = $this->db->get_where('system_settings', array('info_type' => 'total_room'))->row();?>
            <h2 class="counter"><?php echo $room->description;?></h2>
            <span>Class Room</span>
        </li>
    </ul>
</section>
<!--Figures & Facts Wrap End-->

<section class="sub_banner_wrap" style="margin-top: 10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub_banner_hdg" style="text-align: center;">
                    <h3>Amar Pathshala Free Video Tutorial: Visit <a href="http://amarpathshala.com">Amar Pathshala</a></h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/12VQYnuIgrM" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/8LKSA-NvtgY" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/4zfXQc921to" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/c7p50hl4P7c" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>