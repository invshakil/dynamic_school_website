<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="get_started_content_wrap ct_blog_detail_des_list">
                <section class="sub_banner_wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="sub_banner_hdg" style="text-align: center;">
                                    <h3>Notices</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="20%">Title</th>
                                <th width="45%">Notice</th>
                                <th width="20%">Date</th>
                                <th width="10%" type="button" data-toggle="modal" data-target="#myModal">view</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $notices = $this->db->order_by('notice_id', 'desc')->limit(10)->get('noticeboard')->result();
                            foreach ($notices as $row) {
                                ?>

                                <tr>
                                    <th scope="row"><?php echo $row->notice_id; ?></th>
                                    <th scope="row"><?php echo $row->notice_title; ?></th>
                                    <td>
                                        <?php

                                        $str = $row->notice;
                                        $length = strlen($str);
                                        if ($length > 100) {
                                            $strCut = substr($str, 0, 200);
                                            $str = substr($strCut, 0, strrpos($strCut, " ")) . '......';
                                        }

                                        echo $str;
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo date('d-M-Y', $row->create_timestamp) ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-success" type="button" data-toggle="modal"
                                           onclick="showAjaxModal('<?php echo base_url();?>f_modal/popup/modal_notice/<?php echo $row->notice_id;?>');"
                                        >Details</a>


                                    </td>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
            </div>
        </div>

        <div class="col-md-4">
            <aside class="gt_aside_outer_wrap">
                <!--Category Wrap Start-->
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>More Pages</h5>
                    <ul>
                        <li><a href="http://amarpathshala.com" target="_blank">Amar Pathshala</a></li>
                        <li><a href="<?php echo base_url() ?>our-teachers">Our Teachers</a></li>
                        <li><a href="<?php echo base_url() ?>notices">Notices</a></li>
                        <li><a href="<?php echo base_url() ?>class-routine">Class Routine</a></li>
                        <li><a href="<?php echo base_url() ?>gallery">Gallery</a></li>
                    </ul>
                </div>
                <div class="gt_aside_category gt_detail_hdg aside_margin_bottom">
                    <h5>Admission Links</h5>
                    <ul>
                        <li><a href="<?php echo base_url() ?>admission-information">Admission Information</a></li>
                        <li><a href="<?php echo base_url() ?>admission-result">Admission Test Result</a></li>
                        <li><a href="<?php echo base_url() ?>fees-and-payment">Fees And Payment</a></li>
                        <li><a href="<?php echo base_url() ?>scholarship-information">Scholarship Information</a></li>
                    </ul>
                </div>

            </aside>

        </div>

    </div>
</div>

<?php include 'modal.php';?>