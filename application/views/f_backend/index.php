<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title><?php echo $page_name;?></title>

    <?php echo $top;?>


</head>
<body class="page-body page-fade skin-cafe" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <?php echo $navigation;?>

    <div class="main-content">

        <div class="row">

            <!-- Profile Info and Notifications -->
            <div class="col-md-6 col-sm-8 clearfix">
                <h2><?php echo $page_name;?></h2>
            </div>


            <!-- Raw Links -->
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                <ul class="list-inline links-list pull-right">



                    <li class="sep"></li>

                    <li>
                        <a href="<?php echo base_url();?>admin_login/logout">
                            Log Out <i class="entypo-logout right"></i>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

        <hr />





        <div class="col-md-12">

        <?php echo $main_content;?>
        </div>

        <!-- Footer -->
        <footer class="main">

            &copy; 2014 <strong>Software</strong> Developed by <a href="http://techno-71.com" target="_blank">Techno-71</a>

        </footer>
    </div>




</div>

<?php //echo $modal;?>
<?php include 'modal.php';?>
<?php echo $bottom;?>
</body>
</html>