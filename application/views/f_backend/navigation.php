<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo base_url()?>">
                    <?php $logo = $this->db->get_where('system_settings', array('info_type' => 'school_logo' ))->row(); ?>
                    <img src="<?php echo base_url().$logo->description;?>" width="120" alt="" />

                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'Admin Dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/index">
                    <i class="entypo-publish"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- Extra Information -->
            <li class="<?php if ($page_name == 'School Information') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/school_information">
                    <i class="fa fa-hand-o-right"></i>
                    <span>School Information</span>
                </a>
            </li>

            <!-- Principal Speech -->
            <li class="<?php if ($page_name == "Principal's Speech") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/principal_speech">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Principal's Speech</span>
                </a>
            </li>

            <!-- Principal Speech -->
            <li class="<?php if ($page_name == "Chairman's Speech") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/chairman_speech">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Chairman's Speech</span>
                </a>
            </li>

            <!-- Gallery Image -->
            <li class="<?php if ($page_name == "Image Gallery") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/image_gallery">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Image Gallery</span>
                </a>
            </li>

            <!-- History of School -->
            <li class="<?php if ($page_name == "History of School") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/history_of_school">
                    <i class="fa fa-hand-o-right"></i>
                    <span>History of School</span>
                </a>
            </li>

            <!-- Academic Calendar -->
            <li class="<?php if ($page_name == "Academic Calendar") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/academic_calendar">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Academic Calendar</span>
                </a>
            </li>

            <!-- Admission Information -->
            <li class="<?php if ($page_name == "Admission Information") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/admission_information">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Admission Information</span>
                </a>
            </li>

            <!-- Admission Test Result -->
            <li class="<?php if ($page_name == "Admission Test Result") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/admission_test_result">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Admission Test Result</span>
                </a>
            </li>

            <!-- Fees AND Payment -->
            <li class="<?php if ($page_name == "Fees And Payment") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/fees_n_payment">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Fees And Payment</span>
                </a>
            </li>

            <!-- Scholarship Information -->
            <li class="<?php if ($page_name == "Scholarship Information") echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/scholarship_information">
                    <i class="fa fa-hand-o-right"></i>
                    <span>Scholarship Information</span>
                </a>
            </li>
        </ul>

    </div>

</div>