<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Imported styles on this page -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="<?php echo base_url()?>assets/js/gsap/main-gsap.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/joinable.js"></script>
<script src="<?php echo base_url()?>assets/js/resizeable.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-api.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/TableTools.min.js"></script>


<!-- Imported scripts on this page -->
<script src="<?php echo base_url()?>assets/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/lodash.min.js"></script>
<script src="<?php echo base_url()?>assets/js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="<?php echo base_url()?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url()?>assets/js/neon-chat.js"></script>
<script src="<?php echo base_url()?>assets/js/fileinput.js"></script>

<script src="<?php echo base_url()?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url()?>assets/js/ckeditor/adapters/jquery.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="<?php echo base_url()?>assets/js/neon-demo.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<?php if ($this->session->flashdata('message') != "") { ?>
    <script>
        toastr.info('<?php echo $this->session->flashdata('message');?>');
    </script>>
<?php } ?>