<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage System</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">System Information</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add School Information</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">System List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th data-hide="phone">ID</th>
                                <th>School Name</th>
                                <th>School Address</th>
                                <th>Contact Number</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Message</th>

                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->get('system_settings')->result();
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->school_id; ?></td>
                                    <td><?php echo $row->school_name; ?></td>
                                    <td><?php echo $row->school_address; ?></td>
                                    <td><?php echo $row->school_contact; ?></td>
                                    <td><?php echo $row->school_email; ?></td>
                                    <td><img src="<?php echo base_url(). $row->school_logo; ?>" width="100" alt="<?php echo $row->school_name; ?>"></td>
                                    <td><?php echo $row->welcome_message ?></td>

                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_system/<?php echo $row->school_id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>cms/city/delete/<?php echo $row->school_id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add School Information</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>cms/system_settings/create" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">School Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="school_name" class="form-control" id="field-1"
                                           placeholder="Enter School Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">School Address</label>

                                <div class="col-sm-5">
                                    <textarea name="school_address" class="form-control" id="field-ta"
                                              placeholder="School Address"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">Contact Number</label>

                                <div class="col-sm-5">
                                    <input type="text" name="school_contact" class="form-control" id="field-1"
                                           placeholder="Enter School Contact Number">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">School Email</label>

                                <div class="col-sm-5">
                                    <input type="email" name="school_email" class="form-control" id="field-1"
                                           placeholder="Enter School Email Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">School Logo</label>

                                <div class="col-sm-5">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; "
                                             data-trigger="fileinput">
                                            <img src="http://placehold.it/400x150" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px"></div>
                                        <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                            <a href="#" class="btn btn-orange fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>

                                </div>
                                <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Logo Dimension: width 400px and height 150px and image size maximum 200kb.</span>
                            </div>

                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">Welcome Message</label>

                                <div class="col-sm-5">
                                    <textarea name="welcome_message" class="form-control" id="field-ta"
                                              placeholder="School Address"></textarea>
                                </div>
                                <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Write welcome message within 1 line. for example - WE ARE THE BEST IN TOWN</span>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add School Information</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>