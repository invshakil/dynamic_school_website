<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">School Logo Settings</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/school_information/logo_upload" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">School Logo</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">

                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php
                                $logo = $this->db->get_where('system_settings', array('info_type' => 'school_logo' ))->row();
                                if ($logo != '') { ?>
                                    <img src="<?php echo base_url() . $logo->description ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/200x150" alt="...">
                                <?php } ?>
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
                                                <?php if ($logo != '') { ?>
                                                    <span class="fileinput-new">Select New Image</span>
                                                <?php } else { ?>
                                                    <span class="fileinput-new">Select Image</span>
                                                <?php } ?>
                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Logo Dimension: width 400px and height 150px and image size maximum 200kb.</span>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save Logo</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">School Information Settings</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/school_information/do_update" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">School Name</label>

                    <?php $name = $this->db->get_where('system_settings', array('info_type' => 'school_name'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="school_name" value="<?php echo $name->description;?>" class="form-control" id="field-1"
                               placeholder="Enter School Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">School Address</label>

                    <?php $address = $this->db->get_where('system_settings', array('info_type' => 'school_address'))->row();?>
                    <div class="col-sm-5">
                                    <textarea name="school_address" class="form-control" id="field-ta"
                                              placeholder="School Address"><?php echo $address->description;?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Contact Number</label>

                    <?php $contact = $this->db->get_where('system_settings', array('info_type' => 'school_contact'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="school_contact" value="<?php echo $contact->description;?>" class="form-control" id="field-1"
                               placeholder="Enter School Contact Number">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">School Email</label>

                    <?php $email = $this->db->get_where('system_settings', array('info_type' => 'school_email'))->row();?>
                    <div class="col-sm-5">
                        <input type="email" name="school_email" value="<?php echo $email->description;?>" class="form-control" id="field-1"
                               placeholder="Enter School Email Address">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Welcome Message</label>

                    <?php $msg = $this->db->get_where('system_settings', array('info_type' => 'welcome_message'))->row();?>
                    <div class="col-sm-5">
                                    <textarea name="welcome_message" class="form-control" id="field-ta"
                                              placeholder="School Address"><?php echo $msg->description;?></textarea>
                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Write welcome message within 1 line. for example - WE ARE THE BEST IN TOWN</span>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Total Teacher</label>

                    <?php $teacher = $this->db->get_where('system_settings', array('info_type' => 'total_teacher'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="total_teacher" value="<?php echo $teacher->description;?>" class="form-control" id="field-1"
                               placeholder="Enter Number of Teacher">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Total Student</label>

                    <?php $student = $this->db->get_where('system_settings', array('info_type' => 'total_student'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="total_student" value="<?php echo $student->description;?>" class="form-control" id="field-1"
                               placeholder="Enter Number of Student">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Total Library Books</label>

                    <?php $book = $this->db->get_where('system_settings', array('info_type' => 'total_books'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="total_books" value="<?php echo $book->description;?>" class="form-control" id="field-1"
                               placeholder="Enter Number of Library Books">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Total Class Room</label>

                    <?php $room = $this->db->get_where('system_settings', array('info_type' => 'total_room'))->row();?>
                    <div class="col-sm-5">
                        <input type="text" name="total_room" value="<?php echo $room->description;?>" class="form-control" id="field-1"
                               placeholder="Enter Number of Class Room">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Full School Hours</label>

                    <?php $full = $this->db->get_where('system_settings', array('info_type' => 'school_hour_full'))->row();?>
                    <div class="col-sm-5">
                                    <textarea name="school_hour_full" class="form-control" id="field-ta"
                                              placeholder="School Hour (full day)"><?php echo $full->description;?></textarea>
                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">For example: Saturday to Thursday: 9 AM to 5 PM</span>
                </div>


                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Half School Hours</label>

                    <?php $half = $this->db->get_where('system_settings', array('info_type' => 'school_hour_half'))->row();?>
                    <div class="col-sm-5">
                                    <textarea name="school_hour_half" class="form-control" id="field-ta"
                                              placeholder="School Hour (full day)"><?php echo $half->description;?></textarea>
                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">For example: Thursday: 9 AM to 1 PM</span>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Weekly Holiday</label>

                    <?php $holiday = $this->db->get_where('system_settings', array('info_type' => 'school_holiday'))->row();?>
                    <div class="col-sm-5">
                                    <textarea name="school_holiday" class="form-control" id="field-ta"
                                              placeholder="Weekly Holiday"><?php echo $holiday->description;?></textarea>
                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">For example: Friday</span>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save Information</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>