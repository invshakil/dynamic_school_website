<?php
$edit_data		=	$this->db->get_where('image_gallery' , array('image_id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>cms/image_gallery/do_update/<?php echo $row['image_id']?>" enctype="multipart/form-data">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Image</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php if ($row['image'] != '') { ?>
                                    <img src="<?php echo base_url() . $row['image'] ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/200x150" alt="...">
                                <?php } ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
                                                <?php if ($row['image'] != '') { ?>
                                                    <span class="fileinput-new">Select New Image</span>
                                                <?php } else { ?>
                                                    <span class="fileinput-new">Select Image</span>
                                                <?php } ?>
                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Logo Dimension: width 400px and height 150px and image size maximum 200kb.</span>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Image Caption</label>

                    <div class="col-sm-5">
                                    <textarea name="image_caption" class="form-control" id="field-ta" rows="5"
                                              placeholder="Enter Image Caption"><?php echo $row['caption'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Slider Status</label>

                    <div class="col-sm-5">
                        <select name="slider_status" class="form-control">
                            <option value="1" <?php if ($row['slider_status'] == 1) echo 'selected';?>>Yes</option>
                            <option value="0" <?php if ($row['slider_status'] == 0) echo 'selected';?>>No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-5" style="background-color: #0a001f;color: #fff;font-weight: bold ">Note: If you select slider status yes, then this image will be shown in slider in your website</div>

                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Image Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach;?>
    </div>
</div>