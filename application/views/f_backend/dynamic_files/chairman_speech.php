<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Chairman Image</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/chairman_speech/logo_upload" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Chairman's Image</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">

                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php
                                $logo = $this->db->get_where('chairman_speech', array('info_type' => 'image' ))->row();
                                if ($logo->description != '') { ?>
                                    <img src="<?php echo base_url() . $logo->description ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/1024x700" alt="...">
                                <?php } ?>
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
                                                <?php if ($logo->description != '') { ?>
                                                    <span class="fileinput-new">Select New Image</span>
                                                <?php } else { ?>
                                                    <span class="fileinput-new">Select Image</span>
                                                <?php } ?>
                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image Dimension: width 1024 and height 700px and image size maximum 200kb.</span>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save chairman's Image</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Chairman's Message</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/chairman_speech/do_update" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Chairman's Message</label>

                    <?php $msg = $this->db->get_where('chairman_speech', array('info_type' => 'speech'))->row();?>
                    <div class="col-sm-9">
                        <textarea name="speech" class="form-control ckeditor"><?php echo $msg->description;?></textarea>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save Speech</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>