<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Scholarship File Upload</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/scholarship_information/file_upload" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">File Upload</label>
                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-info btn-file">
											<span class="fileinput-new">Select file</span>
											<span class="fileinput-exists">Change</span>
											<input type="file" name="file">

							</span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none">&times;</a>
                            <?php $file = $this->db->get_where('scholarship_information', array('info_type' => 'file'))->row();
                            if ($file->description != NULL) {
                                ?>
                                <br/>
                                <br/>
                                <a class="btn btn-info" target="_blank"
                                   href="<?php echo base_url() . $file->description ?>">Download
                                    Previous File</a>
                            <?php } ?>
                        </div>
                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: PDF/Doc or Image file can be uploaded as File.</span>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save File</button>
                    </div>
                </div>

            </form>

        </div>

    </div>
</div>
<div class="panel panel-dark" data-collapsed="0">

    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">Scholarship Information</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <form role="form" class="form-horizontal form-groups-bordered" method="post"
              action="<?php echo base_url() ?>cms/scholarship_information/do_update" enctype="multipart/form-data">
            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Scholarship Title</label>

                <?php $title = $this->db->get_where('scholarship_information', array('info_type' => 'title'))->row(); ?>
                <div class="col-sm-5">
                    <input type="text" name="title" value="<?php echo $title->description; ?>" class="form-control"
                           id="field-1"
                           placeholder="Enter Fee Title">
                </div>
            </div>

            <div class="form-group">
                <label for="field-ta" class="col-sm-3 control-label">Description</label>

                <?php $msg = $this->db->get_where('scholarship_information', array('info_type' => 'desc'))->row(); ?>
                <div class="col-sm-9">
                    <textarea name="desc" class="form-control ckeditor"><?php echo $msg->description; ?></textarea>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-success">Save Scholarship Information</button>
                </div>
            </div>
        </form>

    </div>
</div>

