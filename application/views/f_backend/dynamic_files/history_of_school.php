<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">School Image</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/history_of_school/logo_upload" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">School's Image</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">

                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php
                                $logo = $this->db->get_where('history_of_school', array('info_type' => 'image' ))->row();
                                if ($logo->description != '') { ?>
                                    <img src="<?php echo base_url() . $logo->description ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/1024x700" alt="...">
                                <?php } ?>
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
                                                <?php if ($logo->description != '') { ?>
                                                    <span class="fileinput-new">Select New Image</span>
                                                <?php } else { ?>
                                                    <span class="fileinput-new">Select Image</span>
                                                <?php } ?>
                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image Dimension: width 1024 and height 700px and image size maximum 200kb.</span>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save School's Image</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">History of School</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                        class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/history_of_school/do_update" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Description</label>

                    <?php $msg = $this->db->get_where('history_of_school', array('info_type' => 'history'))->row();?>
                    <div class="col-sm-9">
                        <textarea name="history" class="form-control ckeditor"><?php echo $msg->description;?></textarea>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success">Save History</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>
