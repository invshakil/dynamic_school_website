<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
//        $this->load->model('crud_model');
        $this->load->database();
        $this->load->library('session');

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'admin_login', 'refresh');

    }

    function index()
    {
        $data = array();
        $data['page_name'] = 'Admin Dashboard';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/dashboard_content', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** Image Upload **/
    function do_upload()
    {
        if (file_exists($_FILES['image']['tmp_name'])) {

            $this->load->library('image_lib');
            $config['upload_path'] = './image_upload/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1024;
            $config['max_width'] = 1600;
            $config['max_height'] = 1600;
            $config['maintain_ratio'] = TRUE;
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('message', $error['error']);

                redirect(base_url().'cms','refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        } else {
            return false;
        }
    }

    /**! Image Upload **/


    /** School Information Settings **/

    function school_information($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $data['description'] = $this->do_upload();
            $this->db->where('info_type', 'school_logo');
            $this->db->update('system_settings', $data);

            $this->session->set_flashdata('message', '<b>School Logo updated!</b>');

            redirect(base_url() . 'cms/school_information', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('school_name');
            $this->db->where('info_type', 'school_name');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_address');
            $this->db->where('info_type', 'school_address');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_contact');
            $this->db->where('info_type', 'school_contact');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_email');
            $this->db->where('info_type', 'school_email');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('welcome_message');
            $this->db->where('info_type', 'welcome_message');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('total_teacher');
            $this->db->where('info_type', 'total_teacher');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('total_student');
            $this->db->where('info_type', 'total_student');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('total_books');
            $this->db->where('info_type', 'total_books');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('total_room');
            $this->db->where('info_type', 'total_room');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_hour_full');
            $this->db->where('info_type', 'school_hour_full');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_hour_half');
            $this->db->where('info_type', 'school_hour_half');
            $this->db->update('system_settings', $data);

            $data['description'] = $this->input->post('school_holiday');
            $this->db->where('info_type', 'school_holiday');
            $this->db->update('system_settings', $data);

            $this->session->set_flashdata('message', '<b>School Information updated!</b>');

            redirect(base_url() . 'cms/school_information', 'refresh');
        }

        $data['page_name'] = 'School Information';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/school_information', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** Principal's Speech **/

    function principal_speech($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $data['description'] = $this->do_upload();
            $this->db->where('info_type', 'image');
            $this->db->update('principal_speech', $data);

            $this->session->set_flashdata('message', '<b>Principal\'s Image updated!</b>');

            redirect(base_url() . 'cms/principal_speech', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('speech');
            $this->db->where('info_type', 'speech');
            $this->db->update('principal_speech', $data);


            $this->session->set_flashdata('message', '<b>Principal\'s Message updated!</b>');

            redirect(base_url() . 'cms/principal_speech', 'refresh');
        }

        $data['page_name'] = 'Principal\'s Speech';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/principal_speech', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** Chairman's Speech **/

    function chairman_speech($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $data['description'] = $this->do_upload();
            $this->db->where('info_type', 'image');
            $this->db->update('chairman_speech', $data);

            $this->session->set_flashdata('message', '<b>Chairman\'s Image updated!</b>');

            redirect(base_url() . 'cms/chairman_speech', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('speech');
            $this->db->where('info_type', 'speech');
            $this->db->update('chairman_speech', $data);


            $this->session->set_flashdata('message', '<b>Chairman\'s Message updated!</b>');

            redirect(base_url() . 'cms/chairman_speech', 'refresh');
        }

        $data['page_name'] = 'Chairman\'s Speech';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/chairman_speech', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** Image Gallery **/

    function image_gallery($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['image'] = $this->do_upload();
            $data['caption'] = $this->input->post('image_caption');
            $data['slider_status'] = $this->input->post('slider_status');

            $this->db->insert('image_gallery', $data);
            $this->session->set_flashdata('message', '<b>Image information saved!</b>');

            redirect(base_url() . 'cms/image_gallery', 'refresh');
        }
        if ($param == 'do_update') {

            //checks if there is any file to upload otherwise it send previous image
            if ($_FILES['image']['size'] !== 0) {
                $data['image'] = $this->do_upload();
            }
            $data['caption'] = $this->input->post('image_caption');
            $data['slider_status'] = $this->input->post('slider_status');

            $this->db->where('image_id', $param2);
            $this->db->update('image_gallery', $data);

            $this->session->set_flashdata('flash_message', '<b>Image information updated!</b>');
            redirect(base_url() . 'cms/image_gallery', 'refresh');
        } else if ($param == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('image_gallery', array(
                'image_id' => $param2
            ))->result_array();
        }
        if ($param == 'delete') {
            $this->db->where('image_id', $param2);
            $this->db->delete('image_gallery');
            $this->session->set_flashdata('message', '<b>Image information deleted!</b>');
            redirect(base_url() . 'cms/image_gallery', 'refresh');
        }

        $data['page_name'] = 'Image Gallery';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/image_gallery', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /**History of School**/

    function history_of_school($param='')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $data['description'] = $this->do_upload();
            $this->db->where('info_type', 'image');
            $this->db->update('history_of_school', $data);

            $this->session->set_flashdata('message', '<b>Image updated!</b>');

            redirect(base_url() . 'cms/history_of_school', 'refresh');
        } elseif ($param == 'do_update') {
            $data['description'] = $this->input->post('history');
            $this->db->where('info_type', 'history');
            $this->db->update('history_of_school', $data);


            $this->session->set_flashdata('message', '<b>History of School updated!</b>');

            redirect(base_url() . 'cms/history_of_school', 'refresh');
        }

        $data['page_name'] = 'History of School';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/history_of_school', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    function academic_calendar($param='')
    {
        $data = array();

        if ($param == 'logo_upload') {
            $data['description'] = $this->do_upload();
            $this->db->where('info_type', 'image');
            $this->db->update('academic_calendar', $data);

            $this->session->set_flashdata('message', '<b>Image updated!</b>');

            redirect(base_url() . 'cms/academic_calendar', 'refresh');
        }

        $data['page_name'] = 'Academic Calendar';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/academic_calendar', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** File Upload **/
    function file_upload()
    {
        if (file_exists($_FILES['file']['tmp_name'])) {

            $config['upload_path'] = './file_upload/';
            $config['allowed_types'] = 'xlsx|docx|doc|pdf|JPG|JPEG|PNG';
            $config['max_size'] = 1024;

            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', $error['error']);
                redirect(base_url().'cms', 'refresh');

            } else {
                $image = $this->upload->data();
                return $config['upload_path'] . $image['file_name'];
            }

        } else {
            return false;
        }
    }

    /**! File Upload **/

    /**Academic Information**/

    function admission_information($param='',$param2=' ')
    {
        $data = array();

        if ($param == 'file_upload') {
            $data['description'] = $this->file_upload();
            $this->db->where('info_type', 'file');
            $this->db->update('admission_info', $data);

            $this->session->set_flashdata('message', '<b>Admission form Saved!</b>');

            redirect(base_url() . 'cms/admission_information', 'refresh');
        } elseif ($param == 'do_update') {

            $data['description'] = $this->input->post('title');
            $this->db->where('info_type', 'title');
            $this->db->update('admission_info', $data);

            $data['description'] = $this->input->post('desc');
            $this->db->where('info_type', 'desc');
            $this->db->update('admission_info', $data);


            $this->session->set_flashdata('message', '<b>Admission information updated!</b>');

            redirect(base_url() . 'cms/admission_information', 'refresh');
        }

        $data['page_name'] = 'Admission Information';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/admission_information', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /** ADMISSION TEST RESULT **/

    function admission_test_result($param='',$param2='')
    {
        $data = array();

        if ($param == 'create') {
            $data['file'] = $this->file_upload();

            $class_info = $this->input->post('class_info');
            $value = explode('|',$class_info);

            $data['class_id'] = $value[0];
            $data['class_name'] = $value[1];

            $data['status'] = $this->input->post('status');

            $title = $this->db->get_where('admission_info', array('info_type' => 'title'))->row();
            $data['admission_title'] = $title->description;

//            echo '<pre>';print_r($data);exit;

            $this->db->insert('admission_result', $data);
            $this->session->set_flashdata('message', '<b>Test Result Published!</b>');

            redirect(base_url() . 'cms/admission_test_result', 'refresh');
        }
        if ($param == 'do_update') {

            //checks if there is any file to upload otherwise it send previous image
            if ($_FILES['file']['size'] !== 0) {
                $data['file'] = $this->file_upload();
            }

            $data['class_id'] = $this->input->post('class_id');
            $data['class_name'] = $this->input->post('class_name');
            $data['status'] = $this->input->post('status');

            $title = $this->db->get_where('admission_info', array('info_type' => 'title'))->row();
            $data['admission_title'] = $title->description;

            $this->db->where('result_id', $param2);
            $this->db->update('admission_result', $data);

            $this->session->set_flashdata('flash_message', '<b>Test Result updated!</b>');
            redirect(base_url() . 'cms/admission_test_result', 'refresh');
        } else if ($param == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('admission_result', array(
                'result_id' => $param2
            ))->result_array();
        }
        if ($param == 'delete') {
            $this->db->where('result_id', $param2);
            $this->db->delete('admission_result');
            $this->session->set_flashdata('message', '<b>Image information deleted!</b>');
            redirect(base_url() . 'cms/admission_test_result', 'refresh');
        }

        $data['page_name'] = 'Admission Test Result';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/admission_test_result', $data, true);

        return $this->load->view('f_backend/index', $data);

    }

    /**FEES AND PAYMENT **/

    function fees_n_payment($param='')
    {
        $data = array();

        if ($param == 'file_upload') {
            $data['description'] = $this->file_upload();
            $this->db->where('info_type', 'file');
            $this->db->update('fees_n_payment', $data);

            $this->session->set_flashdata('message', '<b>File Saved!</b>');

            redirect(base_url() . 'cms/fees_n_payment', 'refresh');
        } elseif ($param == 'do_update') {

            $data['description'] = $this->input->post('title');
            $this->db->where('info_type', 'title');
            $this->db->update('fees_n_payment', $data);

            $data['description'] = $this->input->post('desc');
            $this->db->where('info_type', 'desc');
            $this->db->update('fees_n_payment', $data);


            $this->session->set_flashdata('message', '<b>Fees information updated!</b>');

            redirect(base_url() . 'cms/fees_n_payment', 'refresh');
        }

        $data['page_name'] = 'Fees And Payment';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/fees_n_payment', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

    /**Scholarship INFORMATION**/

    function scholarship_information($param='')
    {
        $data = array();

        if ($param == 'file_upload') {
            $data['description'] = $this->file_upload();
            $this->db->where('info_type', 'file');
            $this->db->update('scholarship_information', $data);

            $this->session->set_flashdata('message', '<b>File Saved!</b>');

            redirect(base_url() . 'cms/scholarship_information', 'refresh');
        } elseif ($param == 'do_update') {

            $data['description'] = $this->input->post('title');
            $this->db->where('info_type', 'title');
            $this->db->update('scholarship_information', $data);

            $data['description'] = $this->input->post('desc');
            $this->db->where('info_type', 'desc');
            $this->db->update('scholarship_information', $data);


            $this->session->set_flashdata('message', '<b>Fees information updated!</b>');

            redirect(base_url() . 'cms/scholarship_information', 'refresh');
        }

        $data['page_name'] = 'Scholarship Information';

        $data['top'] = $this->load->view('f_backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('f_backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('f_backend/navigation', $data, true);
        $data['modal'] = $this->load->view('f_backend/modal', $data, true);

        $data['main_content'] = $this->load->view('f_backend/dynamic_files/scholarship_information', $data, true);

        return $this->load->view('f_backend/index', $data);
    }

}