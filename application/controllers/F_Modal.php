<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class F_modal extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {

    }


    /*
    *	$page_name		=	The name of page
    */
    function popup($page_name = '' , $param2 = '' , $param3 = '')
    {

        $page_data['param2']		=	$param2;
        $page_data['param3']		=	$param3;
        $this->load->view( 'front_end/'.$page_name.'.php' ,$page_data);

        echo '<script src="'.base_url().'assets/js/neon-custom-ajax.js"></script>';
    }
}
