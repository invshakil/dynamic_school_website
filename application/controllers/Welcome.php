<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index()
	{
        $data = array();
        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);

        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);

        $data['slider'] = $this->load->view('front_end/includes/slider','',true);

        $data['main_content'] = $this->load->view('front_end/index_content','',true);
		$this->load->view('front_end/index', $data);
	}

	public function message_from_principal()
    {
        $data = array();

        $data['name'] = 'Principal\'s Message';
        $msg = $this->db->get_where('principal_speech', array('info_type' => 'speech'))->row();
        $data['message'] = '<p align="justify">'.$msg->description.'</p>';
        $logo = $this->db->get_where('principal_speech', array('info_type' => 'image' ))->row();
        $data['image'] = base_url() . $logo->description;

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/message',$data,true);
        $this->load->view('front_end/index', $data);
    }

    public function message_from_chairman()
    {
        $data = array();

        $data['name'] = 'Chairman\'s Message';
        $msg = $this->db->get_where('chairman_speech', array('info_type' => 'speech'))->row();
        $data['message'] = '<p align="justify">'.$msg->description.'</p>';
        $logo = $this->db->get_where('chairman_speech', array('info_type' => 'image' ))->row();
        $data['image'] = base_url() . $logo->description;

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/message',$data,true);
        $this->load->view('front_end/index', $data);
    }

    public function governing_body()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/governing_body','',true);
        $this->load->view('front_end/index', $data);
    }

    public function history_of_school()
    {
        $data = array();

        $data['name'] = 'History of Coaching';
        $msg = $this->db->get_where('history_of_school', array('info_type' => 'history'))->row('description');
        $data['message'] = $msg;
        $data['image'] = base_url().'webAssets/images/school.jpg';

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/message',$data,true);
        $this->load->view('front_end/index', $data);
    }

    public function former_headmasters()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/governing_body','',true);
        $this->load->view('front_end/index', $data);
    }
    public function our_teachers()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/teachers','',true);
        $this->load->view('front_end/index', $data);
    }

    public function notices()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);

        $data['info'] = "Notice Information";

        $data['main_content'] = $this->load->view('front_end/notices',$data,true);
        $this->load->view('front_end/index', $data);
    }

    public function class_routine()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/class_routine','',true);
        $this->load->view('front_end/index', $data);
    }

    public function academic_calendar()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/academic_calendar','',true);
        $this->load->view('front_end/index', $data);
    }

    public function library_information()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/library_information','',true);
        $this->load->view('front_end/index', $data);
    }

    public function admission_information()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/admission_information','',true);
        $this->load->view('front_end/index', $data);
    }

    public function admission_result()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/admission_result','',true);
        $this->load->view('front_end/index', $data);
    }

    public function fees_and_payment()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/fees_and_payment','',true);
        $this->load->view('front_end/index', $data);
    }

    public function scholarship_information()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/scholarship_information','',true);
        $this->load->view('front_end/index', $data);
    }

    public function gallery()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);

        $content_per_page = 6;
        $count = $this->db->count_all_results('image_gallery');
        $data['total_data'] = ceil($count / $content_per_page);
        $data['info'] = "Image Gallery";

        $data['main_content'] = $this->load->view('front_end/gallery',$data,true);
        $this->load->view('front_end/index', $data);
    }

    public function load_more_image()
    {
        $group_no = $this->input->post('group_no');
        $content_per_page = 6;
        $start = ceil($group_no * $content_per_page);

        ?>
        <?php $image = $this->db
        ->order_by('image_id', 'desc')
        ->limit($content_per_page,$start)
        ->get('image_gallery')
        ->result_array();

        foreach ($image as $row) {
            ?>
            <div class="col-md-4 col-sm-6">
                <div class="ct_course_list_wrap">
                    <figure>
                        <img class="img-responsive img-responsive" src="<?php echo base_url() . $row['image'] ?>"
                             alt="">
                        <figcaption class="course_list_img_des">
                            <div class="ct_course_review">
                                <span><?php echo $row['caption'] ?></span>
                            </div>
                            <div class="ct_zoom_effect"></div>
                            <div class="ct_course_link">
                                <a type="button" class="btn btn-info" data-toggle="modal"
                                   onclick="showAjaxModal('<?php echo base_url();?>f_modal/popup/modal_image/<?php echo $row['image_id'];?>');"
                                >View Image</a>
                            </div>
                        </figcaption>
                    </figure>

                </div>
            </div>

        <?php } ?>


        <?php
    }

    public function contact()
    {
        $data = array();

        $data['header'] = $this->load->view('front_end/includes/header','',true);
        $data['banner_top'] = $this->load->view('front_end/includes/banner_top','',true);
        $data['navigation'] = $this->load->view('front_end/includes/navigation','',true);
        $data['slider'] = FALSE;
        $data['footer_content'] = $this->load->view('front_end/includes/footer_content','',true);
        $data['footer_script'] = $this->load->view('front_end/includes/footer_script','',true);


        $data['main_content'] = $this->load->view('front_end/contact','',true);
        $this->load->view('front_end/index', $data);
    }
}
