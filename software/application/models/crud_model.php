<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function clear_cache()
    {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function get_type_name_by_id($type, $type_id = '', $field = 'name')
    {
        return $this->db->get_where($type, array($type . '_id' => $type_id))->row()->$field;
    }

    ////////STUDENT/////////////
    function get_students($class_id, $section_id)
    {
        $query = $this->db->get_where('student', array('class_id' => $class_id, 'section_id' => $section_id));
        return $query->result_array();
    }

    function get_student_info($student_id)
    {
        $query = $this->db->get_where('student', array('student_id' => $student_id));
        return $query->result_array();
    }

    /////////TEACHER/////////////
    function get_teachers()
    {
        $query = $this->db->get('teacher');
        return $query->result_array();
    }

    function get_teacher_name($teacher_id)
    {
        $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name'];
    }

    function get_teacher_info($teacher_id)
    {
        $query = $this->db->get_where('teacher', array('teacher_id' => $teacher_id));
        return $query->result_array();
    }

    //////////SUBJECT/////////////
    function get_subjects()
    {
        $query = $this->db->get('subject');
        return $query->result_array();
    }

    function get_subject_info($subject_id)
    {
        $query = $this->db->get_where('subject', array('subject_id' => $subject_id));
        return $query->result_array();
    }

    function get_subjects_by_class($class_id)
    {
        $query = $this->db->get_where('subject', array('class_id' => $class_id));
        return $query->result_array();
    }

    function get_subject_name_by_id($subject_id)
    {
        $query = $this->db->get_where('subject', array('subject_id' => $subject_id))->row();
        return $query->name;
    }

    ////////////CLASS///////////
    function get_class_name($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name'];
    }

    function get_class_name_numeric($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        $res = $query->result_array();
        foreach ($res as $row)
            return $row['name_numeric'];
    }

    function get_classes()
    {
        $query = $this->db->get('class');
        return $query->result_array();
    }

    function get_class_info($class_id)
    {
        $query = $this->db->get_where('class', array('class_id' => $class_id));
        return $query->result_array();
    }

    //////////EXAMS/////////////
    function get_exams()
    {
        $query = $this->db->get('exam');
        return $query->result_array();
    }

    function get_exam_info($exam_id)
    {
        $query = $this->db->get_where('exam', array('exam_id' => $exam_id));
        return $query->result_array();
    }

    //////////GRADES/////////////
    function get_grades()
    {
        $query = $this->db->get('grade');
        return $query->result_array();
    }

    function get_grade_info($grade_id)
    {
        $query = $this->db->get_where('grade', array('grade_id' => $grade_id));
        return $query->result_array();
    }

    function get_grade($mark_obtained)
    {
        $query = $this->db->get('grade');
        $grades = $query->result_array();
        foreach ($grades as $row) {
            if ($mark_obtained >= $row['mark_from'] && $mark_obtained <= $row['mark_upto'])
                return $row;
        }
    }

    function create_log($data)
    {
        $data['timestamp'] = strtotime(date('Y-m-d') . ' ' . date('H:i:s'));
        $data['ip'] = $_SERVER["REMOTE_ADDR"];
        $location = new SimpleXMLElement(file_get_contents('http://freegeoip.net/xml/' . $_SERVER["REMOTE_ADDR"]));
        $data['location'] = $location->City . ' , ' . $location->CountryName;
        $this->db->insert('log', $data);
    }

    function get_system_settings()
    {
        $query = $this->db->get('settings');
        return $query->result_array();
    }


    ////////BACKUP RESTORE/////////
    function create_backup($type)
    {
        $this->load->dbutil();


        $options = array(
            'format' => 'txt',             // gzip, zip, txt
            'add_drop' => TRUE,              // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE,              // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );


        if ($type == 'all') {
            $tables = array('');
            $file_name = 'system_backup';
        } else {
            $tables = array('tables' => array($type));
            $file_name = 'backup_' . $type;
        }

        $backup =& $this->dbutil->backup(array_merge($options, $tables));


        $this->load->helper('download');
        force_download($file_name . '.sql', $backup);
    }


    /////////RESTORE TOTAL DB/ DB TABLE FROM UPLOADED BACKUP SQL FILE//////////
    function restore_backup()
    {
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/backup.sql');
        $this->load->dbutil();


        $prefs = array(
            'filepath' => 'uploads/backup.sql',
            'delete_after_upload' => TRUE,
            'delimiter' => ';'
        );
        $restore =& $this->dbutil->restore($prefs);
        unlink($prefs['filepath']);
    }

    /////////DELETE DATA FROM TABLES///////////////
    function truncate($type)
    {
        if ($type == 'all') {
            $this->db->truncate('student');
            $this->db->truncate('mark');
            $this->db->truncate('teacher');
            $this->db->truncate('subject');
            $this->db->truncate('class');
            $this->db->truncate('exam');
            $this->db->truncate('grade');
        } else {
            $this->db->truncate($type);
        }
    }


    ////////IMAGE URL//////////
    function get_image_url($type = '', $id = '')
    {
        if (file_exists('uploads/' . $type . '_image/' . $id . '.jpg'))
            $image_url = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
        else
            $image_url = base_url() . 'uploads/user.jpg';

        return $image_url;
    }

    //

    function get_student_profile_details($student_id)
    {
        $this->db->select('*');
        $this->db->from('student');
//        $this->db->join('invoice i', 'i.student_id = s.student_id');
        $this->db->where('student_id', $student_id);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    function get_student_parent_details($student_id)
    {
        $this->db->select('*');
        $this->db->from('student s');
        $this->db->join('parent p', 'p.student_id = s.student_id');
        $this->db->where('s.student_id', $student_id);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();


            return $result;
        }
        return false;
    }

    function get_student_invoice_details($student_id)
    {
        $this->db->select('*');
        $this->db->from('invoice');
        $this->db->where('student_id', $student_id);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_student_total_invoice($student_id)
    {
        $this->db->where('student_id', $student_id);
        $result = $this->db->count_all_results('invoice');

        if ($result) {
            return $result;
        } else
            return false;
    }

    public function get_total_amount_by_student($student_id)
    {
        $this->db->select_sum('amount', 'total_amount');
        $this->db->where('student_id', $student_id);
        $query = $this->db->get('invoice');
        $result = $query->row();

        if ($result) {
            return $result;
        } else
            return false;
    }

    public function get_total_due_by_student($student_id)
    {
        $this->db->select_sum('due', 'due_amount');
        $this->db->where('student_id', $student_id);
        $query = $this->db->get('invoice');
        $result = $query->row();

        if ($result) {
            return $result;
        } else
            return false;
    }

    public function get_total_student_in_class($class_id)
    {
        $this->db->where('class_id', $class_id);
        $query = $this->db->count_all_results('student');

        if ($query) {
            return $query;
        } else
            return false;
    }

    //getting name and info for salary of admin

    public function get_salaries_for_admin()
    {
        $this->db->select('*');
        $this->db->from('salary');
        $this->db->join('admin_staff', 'employee_id = admin_staff_id');
        $where = "employee_type = '1'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_salaries_for_teacher()
    {
        $this->db->select('*');
        $this->db->from('salary');
        $this->db->join('teacher', 'employee_id = teacher_id');
        $where = "employee_type = '2'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_salaries_for_staff()
    {
        $this->db->select('*');
        $this->db->from('salary');
        $this->db->join('staff', 'employee_id = staff_id');
        $where = "employee_type = '3'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function attendance_check_emp($date, $employee_type)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance');
        $where = "employee_type = '$employee_type' AND date = '$date'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function admin_attendance_of_emp($date, $employee_type)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance e');
        $where = "employee_type = '$employee_type' AND date = '$date'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function teacher_attendance_of_emp($date, $employee_type)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance e');
        $where = "date = '$date' AND employee_type = '$employee_type'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function staff_attendance_of_emp($date, $employee_type)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance e');
        $where = "date = '$date' AND employee_type = '$employee_type'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_leave_info_for_admin()
    {
        $this->db->select('*');
        $this->db->from('employee_leave');
        $this->db->join('admin_staff', 'employee_id = admin_staff_id');
        $where = "employee_type = '1'";
        $this->db->where($where);
        $this->db->order_by('leave_id');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_leave_info_for_teacher()
    {
        $this->db->select('*');
        $this->db->from('employee_leave');
        $this->db->join('teacher', 'employee_id = teacher_id');
        $where = "employee_type = '2'";
        $this->db->where($where);
        $this->db->order_by('leave_id');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_leave_info_for_staff()
    {
        $this->db->select('*');
        $this->db->from('employee_leave');
        $this->db->join('staff', 'employee_id = staff_id');
        $where = "employee_type = '3'";
        $this->db->where($where);
        $this->db->order_by('leave_id');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_income_info()
    {
        $this->db->select('*');
        $this->db->from('income i');
        $this->db->join('income_category d', 'd.income_category_id = i.income_category_id');
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_fee_info()
    {
        $this->db->select('*');
        $this->db->from('student_fee i');
        $this->db->join('fee_category d', 'd.fee_category_id = i.fee_category_id');
        $this->db->join('student s', 's.student_id = i.student_id');
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_admit_info($exam_id,$class_id)
    {
        $this->db->select('*');
        $this->db->from('student_fee i');
        $this->db->join('fee_category d', 'd.fee_category_id = i.fee_category_id');
        $this->db->join('student s', 's.student_id = i.student_id');
        $this->db->where('exam_id', $exam_id);
        $this->db->where('i.class_id', $class_id);
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
//            echo '<pre>';
//            print_r($result);
//            exit();
            return $result;
        }
        return false;
    }


    public function get_expense_info()
    {
        $this->db->select('*');
        $this->db->from('expense i');
        $this->db->join('expense_category d', 'd.expense_category_id = i.expense_category_id');
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }

    public function get_room_booking_info($hall_id)
    {
        $this->db->select('*');
        $this->db->from('room_booking r');
        $this->db->join('dormitory d', 'd.hall_id = r.hall_id');
        $this->db->join('student s', 's.student_id = r.student_id');
        $this->db->where('r.hall_id', $hall_id);
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();
        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_salary_report_admin($date1, $date2)
    {
        $this->db->select('*');
        $this->db->from('salary s');
        $this->db->join('admin_staff a', 'a.admin_staff_id = s.employee_id');
        $this->db->where('date >=', $date1);
        $this->db->where('date <=', $date2);
        $this->db->where('employee_type', 1);
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_salary_report_teacher($date1, $date2)
    {
        $this->db->select('*');
        $this->db->from('salary s');
        $this->db->join('teacher a', 'a.teacher_id = s.employee_id');
        $this->db->where('date >=', $date1);
        $this->db->where('date <=', $date2);
        $this->db->where('employee_type', 2);
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_salary_report_staff($date1, $date2)
    {
        $this->db->select('*');
        $this->db->from('salary s');
        $this->db->join('staff a', 'a.staff_id = s.employee_id');
        $this->db->where('date >=', $date1);
        $this->db->where('date <=', $date2);
        $this->db->where('employee_type', 3);
        $this->db->order_by('date', 'desc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_unpaid_report_admin($date1, $date2)
    {

        $sql = "select t1.* from admin_staff t1 where not EXISTS 
      (select t2.* from salary t2 where t2.employee_id = t1.admin_staff_id AND employee_type = '1' AND (date BETWEEN '$date1' AND '$date2'))";
        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function get_unpaid_report_teacher($date1, $date2)
    {

        $sql = "select t1.* from teacher t1 where not EXISTS 
      (select t2.* from salary t2 where t2.employee_id = t1.teacher_id AND employee_type = '2' AND (date BETWEEN '$date1' AND '$date2'))";
        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function get_unpaid_report_staff($date1, $date2)
    {

        $sql = "select t1.* from staff t1 where not EXISTS 
      (select t2.* from salary t2 where t2.employee_id = t1.staff_id AND employee_type = '3' AND (date BETWEEN '$date1' AND '$date2'))";
        $result = $this->db->query($sql)->result();

        return $result;
    }

    public function get_admin_attendance($date1)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance');
        $this->db->where('date', $date1);
        $this->db->where('employee_type', 1);
        $this->db->order_by('date', 'asc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;

    }

    public function get_teacher_attendance($date1)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance');
        $this->db->where('date', $date1);
        $this->db->where('employee_type', 2);
        $this->db->order_by('date', 'asc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;

    }

    public function get_device_attendance($day_start, $day_end)
    {
        $this->db->select('*');
        $this->db->from('device_attendance');
        $this->db->where('datetime >=',$day_start);
        $this->db->where('datetime <=',$day_end);
        $this->db->order_by('staff_id', 'asc');
        $this->db->order_by('datetime', 'asc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result_array();

            return $result;
        }
        return false;

    }

    public function get_staff_attendance($date1)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance');
        $this->db->where('date', $date1);
        $this->db->where('employee_type', 3);
        $this->db->order_by('date', 'asc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;

    }

    public function get_attendance_info_by_id($employee_type, $employee_id, $date1, $date2)
    {
        $this->db->select('*');
        $this->db->from('employee_attendance');
        $this->db->where('date >=', $date1);
        $this->db->where('date <=', $date2);
        $this->db->where('employee_type', $employee_type);
        $this->db->where('employee_id', $employee_id);
        $this->db->order_by('date', 'asc');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();

            return $result;
        }
        return false;
    }

    public function get_device_attendance_report_by_id($staff_code, $date_start, $date_end)
    {
        $this->db->select('*');
        $this->db->from('device_attendance');
        $this->db->where('datetime >=',$date_start);
        $this->db->where('datetime <=',$date_end);
        $this->db->where('staff_id', $staff_code);
        $this->db->order_by('staff_id', 'asc');
        $this->db->order_by('datetime', 'asc');
        $query_result = $this->db->get();


        if ($query_result->num_rows() > 0) {
            $result = $query_result->result_array();

            return $result;
        }
        return false;
    }

    public function student_attendance_report($class_id, $section_id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('class_id', $class_id);
        $this->db->where('section_id', $section_id);
        $this->db->order_by('roll', 'asc');
        $query_result = $this->db->get();


        $result = $query_result->result();

        return $result;
    }

    public function attendance_date($date1, $date2,$section_id)
    {
        $this->db->distinct();
        $this->db->select('date');
        $this->db->from('attendance');
        $this->db->where('date >=', $date1);
        $this->db->where('date <=', $date2);
        $this->db->where('section_id', $section_id);
        $this->db->order_by('date', 'asc');
        $query_result = $this->db->get();

        $result = $query_result->result();

        return $result;
    }
}

