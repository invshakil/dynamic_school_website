<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Employee_attendance extends CI_Controller
{
    //http://localhost/school_front/software/index.php?employee_attendance/index/1/2/1510048289

    function index()
    {

        if ($this->uri->segment(3) && $this->uri->segment(4) && $this->uri->segment(5)) {
            $device_id = $this->uri->segment(3);
            $staff_id = $this->uri->segment(4);
            $date_time = $this->uri->segment(5);

            $date = date("d.m.Y");

            $date = strtotime($date);

            $this->load->database();

            $day_data = $this->db->get_where('device_attendance', array('date' => $date));


            /*
             * This will check if today we have any attendance
             * If yes then will insert data one by one
             * If not then it will create automatically table row for all employee
             */

            if ($day_data->num_rows() > 0) {

                $att_check = $this->db->get_where('device_attendance',
                    array(
                        'device_id' => $device_id,
                        'staff_id' => $staff_id,
                        'date' => $date
                    )
                );


                if ($att_check->num_rows() > 0) {
                    echo 'Attendance Taken Already!';
                } else {
                    $data = array(
                        'device_id' => $device_id,
                        'staff_id' => $staff_id,
                        'datetime' => $date_time,
                        'date' => $date,
                        'status' => 1
                    );

                    $this->db->where('staff_id', $staff_id)->where('date', $date)
                        ->update('device_attendance', $data);

                    echo 'Attendance Taken!';
                }

            } else {

                $teacher_list = $this->db->get('teacher')->result();
                $staff_list = $this->db->get('staff')->result();


                foreach ($teacher_list as $teacher)
                {
                    $data = array(
                        'device_id' => 0,
                        'staff_id' => $teacher->staff_code,
                        'datetime' => NULL,
                        'date' => $date,
                        'status' => 0
                    );

                    $this->db->insert('device_attendance', $data);
                }

                foreach ($staff_list as $staff)
                {
                    $data = array(
                        'device_id' => 0,
                        'staff_id' => $staff->staff_code,
                        'datetime' => NULL,
                        'date' => $date,
                        'status' => 0
                    );

                    $this->db->insert('device_attendance', $data);
                }

                $att_check = $this->db->get_where('device_attendance',
                    array(
                        'device_id' => $device_id,
                        'staff_id' => $staff_id,
                        'date' => $date
                    )
                );


                if ($att_check->num_rows() > 0) {
                    echo 'Attendance Taken Already!';
                } else {
                    $data = array(
                        'device_id' => $device_id,
                        'staff_id' => $staff_id,
                        'datetime' => $date_time,
                        'date' => $date,
                        'status' => 1
                    );

                    $this->db->where('staff_id', $staff_id)->where('date', $date)
                        ->update('device_attendance', $data);

                    echo 'Attendance Taken!';
                }

            }

        } else {
            echo "Don't be smart! URL is protected!";
        }


    }
}

