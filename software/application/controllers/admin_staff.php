<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Admin_staff extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->database();

        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

    }

    /***default functin, redirects to login page if no admin_staff logged in yet***/
    public function index()
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_staff_login') == 1)
            redirect(base_url() . 'index.php?admin_staff/dashboard', 'refresh');
    }

    /***admin_staff DASHBOARD***/
    function dashboard()
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('admin_staff_dashboard');
        $this->load->view('backend/index', $page_data);
    }


    /****MANAGE STUDENTS CLASSWISE*****/
    function student_add()
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'modal/student_add';
        $page_data['page_title'] = get_phrase('add_student');
        $this->load->view('backend/index', $page_data);
    }

    function student_information($class_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');

        $page_data['page_name'] = 'student_information';
        $page_data['page_title'] = get_phrase('student_information') . " - " . get_phrase('class') . " : " .
            $this->crud_model->get_class_name($class_id);
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet($class_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');

        $page_data['page_name'] = 'student_marksheet';
        $page_data['page_title'] = get_phrase('student_marksheet') . " - " . get_phrase('class') . " : " .
            $this->crud_model->get_class_name($class_id);
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['class_id'] = $this->input->post('class_id');
            $data['section_id'] = $this->input->post('section_id');
            $data['roll'] = $this->input->post('roll');
            $data['reg_id'] = $this->input->post('reg_id');
            $data['previous_school'] = $this->input->post('previous_school');

            $this->db->insert('student', $data);

            $this->session->set_flashdata('success', '<b>student information saved!</b>');

            $student_id = mysql_insert_id();
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');
            $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            redirect(base_url() . 'index.php?admin_staff/student_add/' . $data['class_id'], 'refresh');
        }
        if ($param2 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['class_id'] = $this->input->post('class_id');
            $data['section_id'] = $this->input->post('section_id');
            $data['roll'] = $this->input->post('roll');
            $data['reg_id'] = $this->input->post('reg_id');
            $data['previous_school'] = $this->input->post('previous_school');

            $this->db->where('student_id', $param3);
            $this->db->update('student', $data);

            $this->session->set_flashdata('success', '<b>student information updated!</b>');

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $param3 . '.jpg');
            $this->crud_model->clear_cache();

            redirect(base_url() . 'index.php?admin_staff/student_information/' . $param1, 'refresh');
        }

        if ($param2 == 'delete') {
            $this->db->where('student_id', $param3);
            $this->db->delete('student');
            $this->session->set_flashdata('success', '<b>student information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/student_information/' . $param1, 'refresh');
        }
    }

    function get_class_id($class_id)
    {
        $id = $class_id;

        $result = $this->db->get_where("section",array('class_id' => $id))->result();
        echo json_encode($result);


    }

    function get_latest_reg_id($reg_id)
    {
        $result = $this->db->get_where('student', array('reg_id' => $reg_id))->row('name');
        echo json_encode($result);
    }

    function get_latest_roll_by_section($class_id, $section_id, $roll)
    {
        $result = $this->db->get_where('student',
            array(
                'class_id' => $class_id,
                'section_id' => $section_id,
                'roll' => $roll,
            ))
            ->row('name');
        echo json_encode($result);
    }

    function get_subject_id($class_id)
    {
        $id = $class_id;

        $result = $this->db->get_where("subject",array('class_id' => $id))->result();
        echo json_encode($result);


    }

    function get_student_id($section_id)
    {
        $id = $section_id;

        $result = $this->db->get_where("student",array('section_id' => $id))->result();
        echo json_encode($result);


    }

    /****Student Profile Details ****/

    function student_print_view($student_id)
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'student_details';
        $page_data['page_title'] = get_phrase('admin_staff_dashboard');
        $page_data['student_id'] = $student_id;

        $page_data['student_total_invoice'] = $this->crud_model->get_student_total_invoice($student_id);
        $page_data['student_profile_details'] = $this->crud_model->get_student_profile_details($student_id);
        $page_data['student_parent_details'] = $this->crud_model->get_student_parent_details($student_id);
        $page_data['student_invoice_details'] = $this->crud_model->get_student_invoice_details($student_id);

        $page_data['total_amount_by_student'] = $this->crud_model->get_total_amount_by_student($student_id);
        $page_data['total_due_by_student'] = $this->crud_model->get_total_due_by_student($student_id);

        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE PARENTS CLASSWISE*****/
    function parent($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['student_id'] = $param2;
            $data['relation_with_student'] = $this->input->post('relation_with_student');
            $data['phone'] = $this->input->post('phone');
            $data['address'] = $this->input->post('address');
            $data['profession'] = $this->input->post('profession');
            $this->db->insert('parent', $data);

            $this->session->set_flashdata('success', '<b>Parent information saved!</b>');

            $this->email_model->account_opening_email('parent', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL

            $class_id = $this->db->get_where('student', array('student_id' => $data['student_id']))->row()->class_id;
            redirect(base_url() . 'index.php?admin_staff/parent/' . $class_id, 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');

            if ($this->input->post('password') != "")
                $data['password'] = $this->input->post('password');
            $data['relation_with_student'] = $this->input->post('relation_with_student');
            $data['phone'] = $this->input->post('phone');
            $data['address'] = $this->input->post('address');
            $data['profession'] = $this->input->post('profession');

            $this->db->where('parent_id', $param2);
            $this->db->update('parent', $data);

            $this->session->set_flashdata('success', '<b>Parent information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/parent/' . $param3, 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('parent', array(
                'parent_id' => $param3
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('parent_id', $param2);
            $this->db->delete('parent');

            $this->session->set_flashdata('success', '<b>Parent information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/parent/' . $param3, 'refresh');
        }
        $page_data['class_id'] = $param1;
        $page_data['students'] = $this->db->get_where('student', array(
            'class_id' => $param1))->result_array();
        $page_data['page_title'] = get_phrase('parent_information') . " - " . get_phrase('class') . " : " .
            $this->crud_model->get_class_name($param1);
        $page_data['page_name'] = 'parent';
        $this->load->view('backend/index', $page_data);
    }


    /****MANAGE TEACHERS*****/
    function teacher($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['staff_code'] = $this->input->post('staff_code');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $this->db->insert('teacher', $data);

            $this->session->set_flashdata('success', '<b>Teacher information saved!</b>');

            $teacher_id = mysql_insert_id();
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $teacher_id . '.jpg');
            $this->email_model->account_opening_email('teacher', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            redirect(base_url() . 'index.php?admin_staff/teacher/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['staff_code'] = $this->input->post('staff_code');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');

            $this->db->where('teacher_id', $param2);
            $this->db->update('teacher', $data);

            $this->session->set_flashdata('success', '<b>Teacher information updated!</b>');

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $param2 . '.jpg');
            redirect(base_url() . 'index.php?admin_staff/teacher/', 'refresh');
        } else if ($param1 == 'personal_profile') {
            $page_data['personal_profile'] = true;
            $page_data['current_teacher_id'] = $param2;
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('teacher', array(
                'teacher_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('teacher_id', $param2);
            $this->db->delete('teacher');

            $this->session->set_flashdata('success', '<b>Teacher information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/teacher/', 'refresh');
        }
        $page_data['teachers'] = $this->db->get('teacher')->result_array();
        $page_data['page_name'] = 'teacher';
        $page_data['page_title'] = get_phrase('manage_teacher');
        $this->load->view('backend/index', $page_data);
    }

    function get_latest_staff_code($staff_code)
    {

        $t_check = $this->db->get_where('teacher', array('staff_code' => $staff_code))->row('name');

        if ($t_check)
        {
            echo json_encode($t_check);
        }
        else
        {
            $s_check = $this->db->get_where('staff', array('staff_code' => $staff_code))->row('name');
            echo json_encode($s_check);
        }



    }

    /****MANAGE STAFF *****/
    function staff($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['staff_type'] = $this->input->post('staff_type');
            $data['staff_code'] = $this->input->post('staff_code');
            $data['name'] = $this->input->post('name');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $this->db->insert('staff', $data);

            $this->session->set_flashdata('success', '<b>Staff information saved!</b>');

            $staff_id = mysql_insert_id();
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/staff_image/' . $staff_id . '.jpg');
            $this->email_model->account_opening_email('staff', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            redirect(base_url() . 'index.php?admin_staff/staff/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['staff_type'] = $this->input->post('staff_type');
            $data['staff_code'] = $this->input->post('staff_code');
            $data['name'] = $this->input->post('name');
            $data['birthday'] = $this->input->post('birthday');
            $data['sex'] = $this->input->post('sex');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');

            $this->db->where('staff_id', $param2);
            $this->db->update('staff', $data);

            $this->session->set_flashdata('success', '<b>Staff information updated!</b>');

            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/staff_image/' . $param2 . '.jpg');
            redirect(base_url() . 'index.php?admin_staff/staff/', 'refresh');
        } else if ($param1 == 'personal_profile') {
            $page_data['personal_profile'] = true;
            $page_data['current_staff_id'] = $param2;
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('staff', array(
                'staff_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('staff_id', $param2);
            $this->db->delete('staff');

            $this->session->set_flashdata('success', '<b>Staff information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/staff/', 'refresh');
        }
        $page_data['staffs'] = $this->db->get('staff')->result_array();
        $page_data['page_name'] = 'staff';
        $page_data['page_title'] = get_phrase('manage_staff');
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE SALARY *****/
    function salary($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['employee_type'] = $this->input->post('employee_type');
            $data['employee_id'] = $this->input->post('employee_id');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['salary_description'] = $this->input->post('salary_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('salary', $data);

            $this->session->set_flashdata('success', '<b>Salary information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/salary/', 'refresh');
        }
        if ($param1 == 'do_update') {
//            $data['employee_type']        = $this->input->post('employee_type');
//            $data['employee_id']        = $this->input->post('employee_id');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['salary_description'] = $this->input->post('salary_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('salary_id', $param2);
            $this->db->update('salary', $data);

            $this->session->set_flashdata('success', '<b>Salary information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/salary/', 'refresh');
        } else if ($param1 == 'personal_profile') {
            $page_data['personal_profile'] = true;
            $page_data['current_salary_id'] = $param2;
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('salary', array(
                'salary_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('salary_id', $param2);
            $this->db->delete('salary');

            $this->session->set_flashdata('success', '<b>Salary information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/salary/', 'refresh');
        }
        $page_data['salaries_for_admin'] = $this->crud_model->get_salaries_for_admin();
        $page_data['salaries_for_teacher'] = $this->crud_model->get_salaries_for_teacher();
        $page_data['salaries_for_staff'] = $this->crud_model->get_salaries_for_staff();

        $page_data['page_name'] = 'salary';
        $page_data['page_title'] = get_phrase('manage_salary');
        $this->load->view('backend/index', $page_data);
    }

    function salary_report($date = '', $date2 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($_POST) {
            $date1 = $this->input->post('date1');
            $date2 = $this->input->post('date2');
            $page_data['date1'] = $date1;
            $page_data['date2'] = $date2;

            $page_data['salary_report_admin'] = $this->crud_model->get_salary_report_admin($date1, $date2);
            $page_data['salary_report_teacher'] = $this->crud_model->get_salary_report_teacher($date1, $date2);
            $page_data['salary_report_staff'] = $this->crud_model->get_salary_report_staff($date1, $date2);

            $page_data['unpaid_report_admin'] = $this->crud_model->get_unpaid_report_admin($date1, $date2);
            $page_data['unpaid_report_teacher'] = $this->crud_model->get_unpaid_report_teacher($date1, $date2);
            $page_data['unpaid_report_staff'] = $this->crud_model->get_unpaid_report_staff($date1, $date2);
        }

        $page_data['page_name'] = 'salary_report';
        $page_data['page_title'] = get_phrase('salary_report');
        $this->load->view('backend/index', $page_data);
    }

    /** dependent dropdown menu function for admin, staff, teacher */

    function getid($employee_type)
    {
        $id = $employee_type;
        if ($id == 1) {
            $result = $this->db->get("admin_staff")->result();
            echo json_encode($result);
        } elseif ($id == 2) {
            $result = $this->db->get("teacher")->result();
            echo json_encode($result);
        } else {
            $result = $this->db->get("staff")->result();
            echo json_encode($result);
        }

    }

    /****MANAGE EMPLOYEE ATTENDANCE*****/
    function employee_attendance($date = '', $month = '', $year = '', $employee_type = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1) redirect('login', 'refresh');

        $page_data['employee_type'] = $employee_type;
        $page_data['date'] = $date;
        $page_data['month'] = $month;
        $page_data['year'] = $year;
        $full_date = $year . '-' . $month . '-' . $date;
        if ($_POST) {
            $number_of_rows = count($this->input->post('employee_id'));
            $data = array();
            for ($i = 0; $i < $number_of_rows; $i++) {
                $data[$i] = array(
                    'employee_type' => $this->input->post('employee_type')[$i],
                    'employee_id' => $this->input->post('employee_id')[$i],
                    'employee_name' => $this->input->post('employee_name')[$i],
                    'status' => $this->input->post('status')[$i],
                    'date' => strtotime($this->input->post('date')[$i])
                );

            }

            $this->db->insert_batch('employee_attendance', $data);

            $this->session->set_flashdata('success', '<b>Employee attendance taken!</b>');

            redirect(base_url() . 'index.php?admin_staff/employee_attendance_report/' . strtotime($full_date) . '/' . $employee_type, 'refresh');

        }

        $page_data['page_name'] = 'employee_attendance';
        $page_data['page_title'] = get_phrase('employe_attendance');
        $this->load->view('backend/index', $page_data);
    }

    function emp_attendance_selector()
    {
        $today = $this->input->post('date');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $date = strtotime($year . '-' . $month . '-' . $today);
        $employee_type = $this->input->post('employee_type');
        $check_attendance = $this->crud_model->attendance_check_emp($date, $employee_type);

        if ($check_attendance == '') {

            $this->session->set_flashdata('success', '<b>Please Employee attendance carefully!</b>');

            redirect(base_url() . 'index.php?admin_staff/employee_attendance/' . $today . '/' .
                $month . '/' .
                $year . '/' .
                $this->input->post('employee_type'), 'refresh');
        } else {
            $this->session->set_flashdata('success', '<b>Employee attendance taken already, check the report!</b>');
            redirect(base_url() . 'index.php?admin_staff/employee_attendance_report/' . $date . '/' . $employee_type);
        }

    }

    function employee_attendance_report($date, $employee_type)
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($employee_type == 1) {
            $page_data['attendance_info_admin'] = $this->crud_model->admin_attendance_of_emp($date, $employee_type);
        } elseif ($employee_type == 2) {
            $page_data['attendance_info_teacher'] = $this->crud_model->teacher_attendance_of_emp($date, $employee_type);
        } else {
            $page_data['attendance_info_staff'] = $this->crud_model->staff_attendance_of_emp($date, $employee_type);
        }

        $page_data['page_name'] = 'employee_attendance_report';
        $page_data['page_title'] = get_phrase('employee_attendance_report');
        $this->load->view('backend/index', $page_data);
    }

    function device_attendance_report($date1 = '', $date2 = '', $employee_type = '', $staff_code = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['page_name'] = 'device_attendance_report';
        $page_data['page_title'] = get_phrase('attendance_report');
        $this->load->view('backend/index', $page_data);
    }

    function attendance_report($param1)
    {
        $page_data = array();
        $page_data['date_attendance'] = '';
        $page_data['attendance_info_by_employee'] = '';

        if ($param1 == 'daily_report') {
            $date = $this->input->post('date1');


            $date_start = $date . ' 00:00:00';
            $date_end = $date . ' 23:59:59';

            $page_data['date1'] = $this->input->post('date1');

            $day_start = strtotime($date_start) - 21600;
            $day_end = strtotime($date_end) - 21600;

            $array = $this->crud_model->get_device_attendance($day_start, $day_end);

            $result = array();

            date_default_timezone_set('Asia/Dhaka');

            for ($i = 0; $i < count($array) - 1; $i++) {

                $staff_name = $this->db->get_where('teacher', array(
                    'staff_code' => $array[$i]['staff_id']
                ))->row('name');

                $designation = 'Teacher';

                if ($staff_name == NULL)
                {
                    $staff_name = $this->db->get_where('staff', array(
                        'staff_code' => $array[$i]['staff_id']
                    ))->row('name');
                    $designation = 'Staff';
                }

                $name = $staff_name;

                if ($array[$i]['staff_id'] == $array[$i + 1]['staff_id']) {

                    $result[] = array(
                        'employee_name' => $name,
                        'designation' => $designation,
                        'date' => date('l, dS F/Y', $array[$i]['datetime']),
                        'entry_time' => date('h:i A', $array[$i]['datetime']),
                        'leave_time' => date('h:i A', $array[$i + 1]['datetime']),
                        'status' => 1
                    );

                } elseif ($array[$i]['staff_id'] != isset($array[$i - 1]['staff_id'])) {

                    if (date('A', $array[$i]['datetime']) == 'AM') {
                        $result[] = array(
                            'employee_name' => $name,
                            'designation' => $designation,
                            'date' => date('l, dS F/Y', $array[$i]['datetime']),
                            'entry_time' => date('h:i A', $array[$i]['datetime']),
                            'leave_time' => 'No Log Found!',
                            'status' => 1
                        );
                    } else {
                        $result[] = array(
                            'employee_name' => $name,
                            'designation' => $designation,
                            'date' => date('l, dS F/Y', $array[$i]['datetime']),
                            'entry_time' => 'No Log Found!',
                            'leave_time' => date('h:i A', $array[$i]['datetime']),
                            'status' => 1
                        );
                    }


                }

            }


            $page_data['date_attendance'] = $result;

        } elseif ($param1 == 'employee_report') {

            $employee_type = $this->input->post('employee_type');
            $staff_code = $this->input->post('staff_code');

            if ($employee_type == 2)
            {
                $page_data['employee_name'] = $this->db->get_where('teacher', array('staff_code' => $staff_code))->row('name');
                $page_data['designation'] = 'Teacher';
            }
            elseif ($employee_type == 3)
            {
                $page_data['employee_name'] = $this->db->get_where('staff', array('staff_code' => $staff_code))->row('name');
                $page_data['designation'] = 'Staff';
            }

            $date1 = $this->input->post('date1');
            $date2 = $this->input->post('date2');

            date_default_timezone_set('Asia/Dhaka');

            $date_start = $date1 . ' 00:00:00';
            $date_end = $date2 . ' 23:59:59';

            $date_start = strtotime($date_start);
            $date_end = strtotime($date_end);

            $page_data['date1'] = $this->input->post('date1');
            $page_data['date2'] = $this->input->post('date2');

            $page_data['employee_id'] = $staff_code;

            $array = $this->crud_model->get_device_attendance_report_by_id($staff_code, $date_start, $date_end);


            $total = count($array);
            for ($i = 0; $i < $total; $i++) {
                $first = date('dS, F Y', $array[$i]['datetime']);
                if (isset($array[$i + 1])) {
                    $second = date('dS, F Y', $array[$i + 1]['datetime']);
                } else {
                    $array[$i + 1] = null;
                }
                if (isset($array[$i - 1])) {
                    $before_first = date('dS, F Y', $array[$i - 1]['datetime']);
                } else {
                    $before_first = null;
                }

                if ($first == $second && $first != $before_first) {
                    $result[] = [
                        'date' => $first,
                        'entry_time' => date('h:i A', $array[$i]['datetime']),
                        'leave_time' => date('h:i A', $array[$i + 1]['datetime']),
                        'status' => 1
                    ];

                } elseif ($first != $second && $first != $before_first) {
                    if (date('A', $array[$i]['datetime']) === 'AM') {
                        $result[] = [
                            'date' => $first,
                            'entry_time' => date('h:i A', $array[$i]['datetime']),
                            'leave_time' => 'No Log Found!',
                            'status' => 1
                        ];
                    } else {
                        $result[] = [
                            'date' => $first,
                            'entry_time' => 'No Log Found',
                            'leave_time' => date('h:i A', $array[$i]['datetime']),
                            'status' => 1
                        ];
                    }

                }

            }

            $page_data['attendance_info_by_employee'] = $result;

        }


        $page_data['page_name'] = 'view_attendance_report';
        $page_data['page_title'] = get_phrase('view_attendance_report');
        $this->load->view('backend/index', $page_data);
    }

    function manage_emp_attendance($param='', $param2='')
    {
        if ($param == 'do_update') {
//            $data['staff_id'] = $this->input->post('staff_id');
            $time = $this->input->post('time');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $data['datetime'] = $data['date'].' '.$time;
            $data['date'] = $this->input->post('date');

            $this->db->where('device_attendance_id', $param2);
            $this->db->update('device_attendance', $data);
            $this->session->set_flashdata('success', '<b>Employee Attendance information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/manage_emp_attendance/', 'refresh');
        } else if ($param == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('device_attendance', array(
                'device_attendance_id' => $param2
            ))->result_array();
        }
        if ($param == 'delete') {
            $this->db->where('device_attendance_id', $param2);
            $this->db->delete('device_attendance');
            $this->session->set_flashdata('success', '<b>Employee Attendance information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/manage_emp_attendance/', 'refresh');
        }

        $page_data['page_name'] = 'manage_emp_attendance';
        $page_data['page_title'] = get_phrase('manage_employee_attendance');
        $this->load->view('backend/index', $page_data);

    }

    function emp_attendance_report($date1 = '', $date2 = '', $employee_type = '', $employee_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($_POST) {
            $date1 = strtotime($this->input->post('date1'));
            $page_data['date1'] = $date1;

            $page_data['admin_attendance'] = $this->crud_model->get_admin_attendance($date1);
            $page_data['teacher_attendance'] = $this->crud_model->get_teacher_attendance($date1);
            $page_data['staff_attendance'] = $this->crud_model->get_staff_attendance($date1);

            $employee_type = $this->input->post('employee_type');
            $employee_id = $this->input->post('employee_id');
            $page_data['employee_name'] = $this->db->get_where(employee_attendance, array('employee_type' => $employee_type, 'employee_id' => $employee_id))->row('employee_name');

            $page_data['employee_type'] = $employee_type;

            $date2 = strtotime($this->input->post('date2'));
            $page_data['date2'] = $date2;
            $page_data['attendance_info_by_employee'] = $this->crud_model->get_attendance_info_by_id($employee_type, $employee_id, $date1, $date2);

        }

        $page_data['page_name'] = 'emp_attendance_report';
        $page_data['page_title'] = get_phrase('attendance_report');
        $this->load->view('backend/index', $page_data);
    }


    /***admin_staff LEAVE MANAGEMENT***/
    function leave_management($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['employee_type'] = $this->input->post('employee_type');
            $data['employee_id'] = $this->input->post('employee_id');
            $data['application_title'] = $this->input->post('application_title');
            $data['application_description'] = $this->input->post('application_description');
            $data['leave_date'] = $this->input->post('leave_date');
            $data['rejoin_date'] = $this->input->post('rejoin_date');
            $data['status'] = $this->input->post('status');

            $this->db->insert('employee_leave', $data);

            $this->session->set_flashdata('success', '<b>Employee Leave information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/leave_management/', 'refresh');
        }
        if ($param1 == 'do_update') {
//            $data['employee_type']        = $this->input->post('employee_type');
//            $data['employee_id']        = $this->input->post('employee_id');
            $data['application_title'] = $this->input->post('application_title');
            $data['application_description'] = $this->input->post('application_description');
            $data['leave_date'] = $this->input->post('leave_date');
            $data['rejoin_date'] = $this->input->post('rejoin_date');
            $data['status'] = $this->input->post('status');

            $this->db->where('leave_id', $param2);
            $this->db->update('employee_leave', $data);
            $this->session->set_flashdata('success', '<b>Employee Leave information updated!</b>');
            redirect(base_url() . 'index.php?admin_staff/leave_management/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('employee_leave', array(
                'leave_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('leave_id', $param2);
            $this->db->delete('employee_leave');
            $this->session->set_flashdata('success', '<b>Employee Leave information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/leave_management/', 'refresh');
        }
        $page_data['leave_for_admin'] = $this->crud_model->get_leave_info_for_admin();
        $page_data['leave_for_teacher'] = $this->crud_model->get_leave_info_for_teacher();
        $page_data['leave_for_staff'] = $this->crud_model->get_leave_info_for_staff();

        $page_data['page_name'] = 'leave_management';
        $page_data['page_title'] = get_phrase('manage_leave_application');
        $this->load->view('backend/index', $page_data);
    }

    /***admin_staff INCOME MANAGEMENT***/
    function income_category($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['income_category_id'] = $this->input->post('income_category_id');
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('income_category', $data);
            $this->session->set_flashdata('success', '<b>Income category information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/income_category', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('income_category_id', $param2);
            $this->db->update('income_category', $data);
            $this->session->set_flashdata('success', '<b>Income Category information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/income_category', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('income_category', array(
                'income_category_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('income_category_id', $param2);
            $this->db->delete('income_category');
            $this->session->set_flashdata('success', '<b>Income Category information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/income_category', 'refresh');
        }
        $page_data['page_name'] = 'income_category';
        $page_data['page_title'] = get_phrase('manage_cash_in_category');
        $page_data['income_category'] = $this->db->get('income_category')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function income($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['income_category_id'] = $this->input->post('income_category_id');
            $data['income_title'] = $this->input->post('income_title');
            $data['income_description'] = $this->input->post('income_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $this->db->insert('income', $data);
            $this->session->set_flashdata('success', '<b>Income information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/income/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['income_title'] = $this->input->post('income_title');
            $data['income_description'] = $this->input->post('income_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $this->db->where('income_id', $param2);
            $this->db->update('income', $data);

            $this->session->set_flashdata('success', '<b>Income information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/income/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('income', array(
                'income_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('income_id', $param2);
            $this->db->delete('income');
            $this->session->set_flashdata('success', '<b>Income information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/income/', 'refresh');
        }
        $page_data['income_info'] = $this->crud_model->get_income_info();

        $page_data['page_name'] = 'income';
        $page_data['page_title'] = get_phrase('manage_cash_in');
        $this->load->view('backend/index', $page_data);
    }

    /***admin_staff EXPENSE MANAGEMENT***/
    function expense_category($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['expense_category_id'] = $this->input->post('expense_category_id');
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('expense_category', $data);
            $this->session->set_flashdata('success', '<b>Expense Category information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/expense_category', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('expense_category_id', $param2);
            $this->db->update('expense_category', $data);
            $this->session->set_flashdata('success', '<b>Expense Category information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/expense_category', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('expense_category', array(
                'expense_category_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('expense_category_id', $param2);
            $this->db->delete('expense_category');
            $this->session->set_flashdata('success', '<b>Expense Category information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/expense_category', 'refresh');
        }
        $page_data['page_name'] = 'expense_category';
        $page_data['page_title'] = get_phrase('manage_cash_out_category');
        $page_data['expense_category'] = $this->db->get('expense_category')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function expense($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['expense_category_id'] = $this->input->post('expense_category_id');
            $data['expense_title'] = $this->input->post('expense_title');
            $data['expense_description'] = $this->input->post('expense_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $this->db->insert('expense', $data);
            $this->session->set_flashdata('success', '<b>Expense information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/expense/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['expense_title'] = $this->input->post('expense_title');
            $data['expense_description'] = $this->input->post('expense_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $this->db->where('expense_id', $param2);
            $this->db->update('expense', $data);
            $this->session->set_flashdata('success', '<b>Expense information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/expense/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('expense', array(
                'expense_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('expense_id', $param2);
            $this->db->delete('expense');
            $this->session->set_flashdata('success', '<b>Expense information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/expense/', 'refresh');
        }
        $page_data['expense_info'] = $this->crud_model->get_expense_info();

        $page_data['page_name'] = 'expense';
        $page_data['page_title'] = get_phrase('manage_cash_out');
        $this->load->view('backend/index', $page_data);
    }

    /****BANK DETAILS*****/

    function bank_details($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {

            $data['bank_name'] = $this->input->post('bank_name');
            $data['branch_address'] = $this->input->post('branch_address');
            $data['status'] = $this->input->post('status');

            $this->db->insert('bank_details', $data);
            $this->session->set_flashdata('success', '<b>Bank information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/bank_details', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['bank_name'] = $this->input->post('bank_name');
            $data['branch_address'] = $this->input->post('branch_address');
            $data['status'] = $this->input->post('status');

            $this->db->where('bank_id', $param2);
            $this->db->update('bank_details', $data);
            $this->session->set_flashdata('success', '<b>Bank information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/bank_details', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('bank_details', array(
                'bank_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('bank_id', $param2);
            $this->db->delete('bank_details');
            $this->session->set_flashdata('success', '<b>Bank information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/bank_details', 'refresh');
        }
        $page_data['page_name'] = 'bank_details';
        $page_data['page_title'] = get_phrase('manage_bank_details');
        $page_data['bank_details'] = $this->db->get('bank_details')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE SUBJECTS*****/
    function subject($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['class_id'] = $this->input->post('class_id');
            $data['teacher_id'] = $this->input->post('teacher_id');
            $this->db->insert('subject', $data);
            $this->session->set_flashdata('success', '<b>Subject information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/subject/' . $data['class_id'], 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['class_id'] = $this->input->post('class_id');
            $data['teacher_id'] = $this->input->post('teacher_id');

            $this->db->where('subject_id', $param2);
            $this->db->update('subject', $data);
            $this->session->set_flashdata('success', '<b>Subject information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/subject/' . $data['class_id'], 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('subject', array(
                'subject_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('subject_id', $param2);
            $this->db->delete('subject');
            $this->session->set_flashdata('success', '<b>Subject information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/subject/' . $param3, 'refresh');
        }
        $page_data['class_id'] = $param1;
        $page_data['subjects'] = $this->db->get_where('subject', array('class_id' => $param1))->result_array();
        $page_data['page_name'] = 'subject';
        $page_data['page_title'] = get_phrase('manage_subject');
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE CLASSES*****/
    function classes($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['number_of_section'] = $this->input->post('number_of_section');
            $this->db->insert('class', $data);
            $this->session->set_flashdata('success', '<b>Class information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/classes/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['number_of_section'] = $this->input->post('number_of_section');

            $this->db->where('class_id', $param2);
            $this->db->update('class', $data);
            $this->session->set_flashdata('success', '<b>Class information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/classes/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('class', array(
                'class_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('class_id', $param2);
            $this->db->delete('class');
            $this->session->set_flashdata('success', '<b>Class information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/classes/', 'refresh');
        }
        $page_data['classes'] = $this->db->get('class')->result_array();
        $page_data['page_name'] = 'class';
        $page_data['page_title'] = get_phrase('manage_class');
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE SECTIONS*****/
    function sections($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['section_name'] = $this->input->post('section_name');
            $data['class_id'] = $this->input->post('class_id');
            $data['teacher_id'] = $this->input->post('teacher_id');
            $this->db->insert('section', $data);
            $this->session->set_flashdata('success', '<b>Section information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/sections/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['section_name'] = $this->input->post('section_name');
            $data['class_id'] = $this->input->post('class_id');
            $data['teacher_id'] = $this->input->post('teacher_id');

            $this->db->where('section_id', $param2);
            $this->db->update('section', $data);
            $this->session->set_flashdata('success', '<b>Section information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/sections/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('section', array(
                'section_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('section_id', $param2);
            $this->db->delete('section');
            $this->session->set_flashdata('success', '<b>Section information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/sections/', 'refresh');
        }
        $page_data['section'] = $this->db->get('section')->result_array();

        $page_data['page_name'] = 'section';
        $page_data['page_title'] = get_phrase('manage_section');
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE EXAMS*****/
    function exam($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['date'] = $this->input->post('date');
            $data['comment'] = $this->input->post('comment');
            $this->db->insert('exam', $data);
            $this->session->set_flashdata('success', '<b>Examination information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/exam/', 'refresh');
        }
        if ($param1 == 'edit' && $param2 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['date'] = $this->input->post('date');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('exam_id', $param3);
            $this->db->update('exam', $data);
            $this->session->set_flashdata('success', '<b>Examination information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/exam/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('exam', array(
                'exam_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('exam_id', $param2);
            $this->db->delete('exam');
            $this->session->set_flashdata('success', '<b>Examination information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/exam/', 'refresh');
        }
        $page_data['exams'] = $this->db->get('exam')->result_array();
        $page_data['page_name'] = 'exam';
        $page_data['page_title'] = get_phrase('manage_exam');
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE EXAM MARKS*****/
    function marks($exam_id = '', $class_id = '',$section_id='', $subject_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($this->input->post('operation') == 'selection') {
            $page_data['exam_id'] = $this->input->post('exam_id');
            $page_data['class_id'] = $this->input->post('class_id');
            $page_data['section_id'] = $this->input->post('section_id');
            $page_data['subject_id'] = $this->input->post('subject_id');

//            echo '<pre>';print_r($page_data);exit();

            if ($page_data['exam_id'] > 0 && $page_data['class_id'] > 0 && $page_data['section_id'] > 0 && $page_data['subject_id'] > 0) {
                redirect(base_url() . 'index.php?admin_staff/marks/' . $page_data['exam_id'] . '/' . $page_data['class_id'] . '/'. $page_data['section_id'] . '/' . $page_data['subject_id'], 'refresh');
            } else {
                $this->session->set_flashdata('success', 'Choose exam, class and subject');
                redirect(base_url() . 'index.php?admin_staff/marks/', 'refresh');
            }
        }
        if ($this->input->post('operation') == 'update') {
            $page_data['exam_id'] = $this->input->post('exam_id');
            $page_data['class_id'] = $this->input->post('class_id');
            $page_data['section_id'] = $this->input->post('section_id');
            $page_data['subject_id'] = $this->input->post('subject_id');

            $data['mark_obtained'] = $this->input->post('mark_obtained');
            $data['attendance'] = $this->input->post('attendance');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('mark_id', $this->input->post('mark_id'));
            $this->db->update('mark', $data);
            $this->session->set_flashdata('success', '<b>Mark information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/marks/' . $page_data['exam_id'] . '/' . $page_data['class_id'] . '/'. $page_data['section_id'] . '/' . $page_data['subject_id'], 'refresh');
        }
        $page_data['exam_id'] = $exam_id;
        $page_data['class_id'] = $class_id;
        $page_data['section_id'] = $section_id;
        $page_data['subject_id'] = $subject_id;

//        echo '<pre>';print_r($page_data);exit();

        $page_data['page_info'] = 'Exam marks';

        $page_data['page_name'] = 'marks';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }


    /****MANAGE GRADES*****/
    function grade($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['grade_point'] = $this->input->post('grade_point');
            $data['mark_from'] = $this->input->post('mark_from');
            $data['mark_upto'] = $this->input->post('mark_upto');
            $data['comment'] = $this->input->post('comment');
            $this->db->insert('grade', $data);
            $this->session->set_flashdata('success', '<b>Grade information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/grade/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['grade_point'] = $this->input->post('grade_point');
            $data['mark_from'] = $this->input->post('mark_from');
            $data['mark_upto'] = $this->input->post('mark_upto');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('grade_id', $param2);
            $this->db->update('grade', $data);
            $this->session->set_flashdata('success', '<b>Grade information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/grade/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('grade', array(
                'grade_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('grade_id', $param2);
            $this->db->delete('grade');
            $this->session->set_flashdata('success', '<b>Grade information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/grade/', 'refresh');
        }
        $page_data['grades'] = $this->db->get('grade')->result_array();
        $page_data['page_name'] = 'grade';
        $page_data['page_title'] = get_phrase('manage_grade');
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGING CLASS ROUTINE******************/
    function class_routine($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['class_id'] = $this->input->post('class_id');
            $data['section_id'] = $this->input->post('section_id');
            $data['subject_id'] = $this->input->post('subject_id');
            $data['time_start'] = $this->input->post('time_start');
            $data['time_end'] = $this->input->post('time_end');
            $data['day'] = $this->input->post('day');

            $this->db->insert('class_routine', $data);
            $this->session->set_flashdata('success', '<b>Class routine information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/class_routine/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['class_id'] = $this->input->post('class_id');
            $data['section_id'] = $this->input->post('section_id');
            $data['subject_id'] = $this->input->post('subject_id');
            $data['time_start'] = $this->input->post('time_start');
            $data['time_end'] = $this->input->post('time_end');
            $data['day'] = $this->input->post('day');

            $this->db->where('class_routine_id', $param2);
            $this->db->update('class_routine', $data);
            $this->session->set_flashdata('success', '<b>Class routine information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/class_routine/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('class_routine', array(
                'class_routine_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('class_routine_id', $param2);
            $this->db->delete('class_routine');
            $this->session->set_flashdata('success', '<b>Class routine information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/class_routine/', 'refresh');
        }
        $page_data['page_name'] = 'class_routine';
        $page_data['page_title'] = get_phrase('manage_class_routine');
        $this->load->view('backend/index', $page_data);
    }

    /****** DAILY ATTENDANCE *****************/
    function manage_attendance($date = '', $month = '', $year = '', $class_id = '',$section_id ='')
    {
        if ($this->session->userdata('admin_staff_login') != 1) redirect('login', 'refresh');

        if ($_POST) {
            $verify_data = array('student_id' => $this->input->post('student_id'),'section_id' => $this->input->post('section_id'),
                'date' => $this->input->post('date'));
            $attendance = $this->db->get_where('attendance', $verify_data)->row();
            $attendance_id = $attendance->attendance_id;

            $this->db->where('attendance_id', $attendance_id);
            $this->db->update('attendance', array('status' => $this->input->post('status')));
            $this->session->set_flashdata('success', '<b>Attendance information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/manage_attendance/' . $date . '/' . $month . '/' . $year . '/' . $class_id.'/'.$section_id, 'refresh');
        }
        $page_data['date'] = $date;
        $page_data['month'] = $month;
        $page_data['year'] = $year;
        $page_data['class_id'] = $class_id;
        $page_data['section_id'] = $section_id;


        $page_data['page_name'] = 'manage_attendance';
        $page_data['page_title'] = get_phrase('manage_daily_attendance');
        $this->load->view('backend/index', $page_data);
    }

    function student_attendance_report($date1 = '', $date2 = '', $class_id = '', $section_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($_POST) {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $source1 = $this->input->post('date1');
            $source2 = $this->input->post('date2');
            $date1 = new DateTime($source1);
            $date2 = new DateTime($source2);
            $page_data['date1'] = $date1->format('Y-m-d');
            $page_data['date2'] = $date2->format('Y-m-d');
            $page_data['class_id'] = $class_id;
            $page_data['section_id'] = $section_id;

            $page_data['student_id'] = $this->crud_model->student_attendance_report($class_id,$section_id);
            $page_data['attendance_date'] = $this->crud_model->attendance_date($page_data['date1'],$page_data['date2'],$section_id);

        }

        $page_data['page_name'] = 'student_attendance_report';
        $page_data['page_title'] = get_phrase('student_attendance_report');
        $this->load->view('backend/index', $page_data);
    }

    function attendance_selector()
    {
        redirect(base_url() . 'index.php?admin_staff/manage_attendance/' . $this->input->post('date') . '/' .
            $this->input->post('month') . '/' .
            $this->input->post('year') . '/' .
            $this->input->post('class_id') . '/' .
            $this->input->post('section_id'), 'refresh');
    }

    /******MANAGE BILLING / INVOICES WITH STATUS*****/
    function invoice($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['student_id'] = $this->input->post('student_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['amount'] = $this->input->post('amount');
            $data['due'] = $this->input->post('due');
            $data['status'] = $this->input->post('status');
            $data['creation_timestamp'] = strtotime($this->input->post('date'));

            $this->db->insert('invoice', $data);
            $this->session->set_flashdata('success', '<b>Invoice information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/invoice', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['student_id'] = $this->input->post('student_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['amount'] = $this->input->post('amount');
            $data['due'] = $this->input->post('due');
            $data['status'] = $this->input->post('status');
            $data['creation_timestamp'] = strtotime($this->input->post('date'));

            $this->db->where('invoice_id', $param2);
            $this->db->update('invoice', $data);
            $this->session->set_flashdata('success', '<b>Invoice information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/invoice', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('invoice', array(
                'invoice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('invoice_id', $param2);
            $this->db->delete('invoice');
            $this->session->set_flashdata('success', '<b>Invoice information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/invoice', 'refresh');
        }
        $page_data['page_name'] = 'invoice';
        $page_data['page_title'] = get_phrase('manage_invoice/payment');
        $this->db->order_by('creation_timestamp', 'desc');
        $page_data['invoices'] = $this->db->get('invoice')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /**
     * STUDENT FEE MANAGEMENT
     */
    function fee_category($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['fee_category_id'] = $this->input->post('fee_category_id');
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->insert('fee_category', $data);
            $this->session->set_flashdata('success', '<b>Fee category information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/fee_category', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['category_name'] = $this->input->post('category_name');
            $data['category_description'] = $this->input->post('category_description');
            $data['status'] = $this->input->post('status');

            $this->db->where('fee_category_id', $param2);
            $this->db->update('fee_category', $data);
            $this->session->set_flashdata('success', '<b>Fee Category information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/fee_category', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('fee_category', array(
                'fee_category_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('fee_category_id', $param2);
            $this->db->delete('fee_category');
            $this->session->set_flashdata('success', '<b>Fee Category information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/fee_category', 'refresh');
        }
        $page_data['page_name'] = 'fee_category';
        $page_data['page_title'] = get_phrase('manage_fee_category');
        $page_data['fee_category'] = $this->db->get('fee_category')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function issue_student_fee($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['fee_category_id'] = $this->input->post('fee_category_id');
            $data['exam_id'] = $this->input->post('exam_id');
            $data['student_id'] = $this->input->post('student_id');

            $class_id = $this->db->get_where('student', array('student_id' => $data['student_id']))->row('class_id');
            $data['class_id'] = $class_id;
            $data['fee_title'] = $this->input->post('fee_title');
            $data['fee_description'] = $this->input->post('fee_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = strtotime($this->input->post('date'));
            $data['status'] = $this->input->post('status');


            $this->db->insert('student_fee', $data);
            $this->session->set_flashdata('success', '<b>Fee information saved!</b>');
            redirect(base_url() . 'index.php?admin_staff/issue_student_fee/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['fee_category_id'] = $this->input->post('fee_category_id');
            $data['exam_id'] = $this->input->post('exam_id');
            $data['fee_title'] = $this->input->post('fee_title');
            $data['fee_description'] = $this->input->post('fee_description');
            $data['amount'] = $this->input->post('amount');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');

            $this->db->where('fee_id', $param2);
            $this->db->update('student_fee', $data);

            $this->session->set_flashdata('success', '<b>Fee information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/issue_student_fee/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('student_fee', array(
                'fee_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('fee_id', $param2);
            $this->db->delete('student_fee');
            $this->session->set_flashdata('success', '<b>Fee information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/issue_student_fee/', 'refresh');
        }

        $page_data['fee_info'] = $this->crud_model->get_fee_info();

        $page_data['page_name'] = 'issue_student_fee';
        $page_data['page_title'] = get_phrase('manage_fee');
        $this->load->view('backend/index', $page_data);
    }

    function student_admit_issue($class_id, $exam_id = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');

        $page_data['class_id'] = $class_id;
        $page_data['page_name'] = 'student_admit_issue';
        $page_data['page_title'] = get_phrase('student_admit_issue');

        if ($_POST) {
            $class_id = $this->input->post('exam_id');
            $page_data['exam_id'] = $this->input->post('exam_id');

            $page_data['admit_card_info'] = $this->crud_model->get_admit_info($page_data['exam_id'],$class_id);
        }

        $this->load->view('backend/index', $page_data);

    }

    function student_admit_card($student_id, $exam_id)
    {
        $page_data['student_id'] = $student_id;
        $page_data['exam_id'] = $exam_id;
        $page_data['page_name'] = 'student_admit_card';
        $page_data['page_title'] = get_phrase('student_admit_card');
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGE LIBRARY / BOOKS********************/
    function book($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['price'] = $this->input->post('price');
            $data['author'] = $this->input->post('author');
            $data['class_id'] = $this->input->post('class_id');
            $data['status'] = $this->input->post('status');
            $this->db->insert('book', $data);
            $this->session->set_flashdata('success', '<b>Book information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/book', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['price'] = $this->input->post('price');
            $data['author'] = $this->input->post('author');
            $data['class_id'] = $this->input->post('class_id');
            $data['status'] = $this->input->post('status');

            $this->db->where('book_id', $param2);
            $this->db->update('book', $data);
            $this->session->set_flashdata('success', '<b>Book information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/book', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('book', array(
                'book_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('book_id', $param2);
            $this->db->delete('book');
            $this->session->set_flashdata('success', '<b>Book information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/book', 'refresh');
        }
        $page_data['books'] = $this->db->get('book')->result_array();
        $page_data['page_name'] = 'book';
        $page_data['page_title'] = get_phrase('manage_library_books');
        $this->load->view('backend/index', $page_data);

    }

    /**********MANAGE TRANSPORT / VEHICLES / ROUTES********************/
    function transport($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['route_name'] = $this->input->post('route_name');
            $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
            $data['description'] = $this->input->post('description');
            $data['route_fare'] = $this->input->post('route_fare');
            $this->db->insert('transport', $data);
            $this->session->set_flashdata('success', '<b>Transport information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/transport', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['route_name'] = $this->input->post('route_name');
            $data['number_of_vehicle'] = $this->input->post('number_of_vehicle');
            $data['description'] = $this->input->post('description');
            $data['route_fare'] = $this->input->post('route_fare');

            $this->db->where('transport_id', $param2);
            $this->db->update('transport', $data);
            $this->session->set_flashdata('success', '<b>Transport information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/transport', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('transport', array(
                'transport_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('transport_id', $param2);
            $this->db->delete('transport');
            $this->session->set_flashdata('success', '<b>Transport information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/transport', 'refresh');
        }
        $page_data['transports'] = $this->db->get('transport')->result_array();
        $page_data['page_name'] = 'transport';
        $page_data['page_title'] = get_phrase('manage_transport');
        $this->load->view('backend/index', $page_data);

    }

    /**********MANAGE DORMITORY / HOSTELS / ROOMS ********************/
    function dormitory($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['name'] = $this->input->post('name');
            $data['hall_id'] = $this->input->post('hall_id');
            $data['max_number_of_bed'] = $this->input->post('max_number_of_bed');
            $data['description'] = $this->input->post('description');
            $this->db->insert('dormitory', $data);
            $this->session->set_flashdata('success', '<b>Dormitory information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/dormitory', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name'] = $this->input->post('name');
            $data['hall_id'] = $this->input->post('hall_id');
            $data['max_number_of_bed'] = $this->input->post('max_number_of_bed');
            $data['description'] = $this->input->post('description');

            $this->db->where('dormitory_id', $param2);
            $this->db->update('dormitory', $data);
            $this->session->set_flashdata('success', '<b>Dormitory information updated!</b>');

            redirect(base_url() . 'index.php?admin_staff/dormitory', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('dormitory', array(
                'dormitory_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('dormitory_id', $param2);
            $this->db->delete('dormitory');
            $this->session->set_flashdata('success', '<b>Dormitory information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/dormitory', 'refresh');
        }
        $page_data['dormitories'] = $this->db->get('dormitory')->result_array();
        $page_data['page_name'] = 'dormitory';
        $page_data['page_title'] = get_phrase('manage_dormitory');
        $this->load->view('backend/index', $page_data);

    }

    //** Information of Individual Halls */

    function room_information($hall_id, $param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');

        $page_data['hall_id'] = $hall_id;

        if ($param1 == 'create') {
            $data['hall_id'] = $hall_id;
            $data['room_id'] = $this->input->post('room_id');
            $data['floor_id'] = $this->input->post('floor_id');
            $data['number_of_bed'] = $this->input->post('number_of_bed');
            $data['room_fee'] = $this->input->post('room_fee');
            $data['description'] = $this->input->post('description');

            $max_limit = $this->db->get_where('dormitory', array('hall_id' => $hall_id))->row();
            $count_room_created = $this->db->select_sum('number_of_bed')->get_where('hall_room_info', array('hall_id' => $hall_id))->row();

            $limit = $max_limit->max_number_of_bed;
            $created = $count_room_created->number_of_bed;

            if ($limit > $created) {
                $total = $created + $data['number_of_bed'];


                if ($limit < $total) {
                    $can_create = $limit - $created;
                    $this->session->set_flashdata('success', '<b>you can create maximum ' . $limit . 'room! You can now create ' . $can_create . ' more room.</b>');

                    redirect(base_url() . 'index.php?admin_staff/room_information/' . $hall_id, 'refresh');

                } else {
                    $this->db->insert('hall_room_info', $data);
                    $this->session->set_flashdata('success', '<b>Room information saved!</b>');
                    redirect(base_url() . 'index.php?admin_staff/room_information/' . $hall_id, 'refresh');
                }

            } else {

                $this->session->set_flashdata('success', '<b>You have reached the maximum limit of room in this hall, you can not add any more room!</b>');
                redirect(base_url() . 'index.php?admin_staff/room_information/' . $hall_id, 'refresh');

            }


        }
        if ($param1 == 'do_update') {

            $data['hall_id'] = $hall_id;
            $data['room_id'] = $this->input->post('room_id');
            $data['floor_id'] = $this->input->post('floor_id');
            $data['number_of_bed'] = $this->input->post('number_of_bed');
            $data['room_fee'] = $this->input->post('room_fee');
            $data['description'] = $this->input->post('description');

            $this->db->where('room_info', $param2);
            $this->db->update('hall_room_info', $data);
            $this->session->set_flashdata('success', '<b>Room information updated!</b>');
            redirect(base_url() . 'index.php?admin_staff/room_information/' . $hall_id, 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('hall_room_info', array(
                'room_info' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('room_info', $param2);
            $this->db->delete('hall_room_info');
            $this->session->set_flashdata('success', '<b>Room information deleted!</b>');
            redirect(base_url() . 'index.php?admin_staff/room_information/' . $hall_id, 'refresh');
        }

        $max_limit = $this->db->get_where('dormitory', array('hall_id' => $hall_id))->row();
        $count_room_created = $this->db->select_sum('number_of_bed')->get_where('hall_room_info', array('hall_id' => $hall_id))->row();

        $page_data['total_room'] = $max_limit->max_number_of_bed;
        $page_data['total_room_created'] = $count_room_created->number_of_bed;

        $page_data['hall_rooms'] = $this->db->get_where('hall_room_info', array('hall_id' => $hall_id))->result_array();
        $page_data['page_name'] = 'room_information';
        $page_data['page_title'] = get_phrase('room_information');
        $this->load->view('backend/index', $page_data);

    }

    //** Information of Individual Halls */

    function room_booking($hall_id, $param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect('login', 'refresh');

        $page_data['hall_id'] = $hall_id;

        if ($param1 == 'create') {
            $data['student_id'] = $this->input->post('student_id');

            $hall_name = $this->db->get_where('dormitory', array('hall_id' => $hall_id))->row();

            $data['hall_name'] = $hall_name->name;
            $data['hall_id'] = $hall_id;
            $data['room_id'] = $this->input->post('room_id');
            $data['floor_id'] = $this->input->post('floor_id');
            $data['room_fee'] = $this->input->post('room_fee');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');
            $data['comment'] = $this->input->post('comment');

            $this->db->insert('room_booking', $data);
//            $this->session->set_flashdata('success','<b>Room reservation information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/room_booking/' . $hall_id, 'refresh');
        }
        if ($param1 == 'do_update') {

            $data['student_id'] = $this->input->post('student_id');
            $data['hall_name'] = $this->input->post('hall_name');
            $data['hall_id'] = $hall_id;
            $data['room_id'] = $this->input->post('room_id');
            $data['floor_id'] = $this->input->post('floor_id');
            $data['room_fee'] = $this->input->post('room_fee');
            $data['date'] = $this->input->post('date');
            $data['status'] = $this->input->post('status');
            $data['comment'] = $this->input->post('comment');

            $this->db->where('booking_info', $param2);
            $this->db->update('room_booking', $data);
            $this->session->set_flashdata('success', '<b>Room reservation information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/room_booking/' . $hall_id, 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('room_booking', array(
                'booking_info' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('booking_info', $param2);
            $this->db->delete('room_booking');
            $this->session->set_flashdata('success', '<b>Room reservation information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/room_booking/' . $hall_id, 'refresh');
        }


        $page_data['booked_room'] = $this->crud_model->get_room_booking_info($hall_id);

        $max_limit = $this->db->get_where('dormitory', array('hall_id' => $hall_id))->row();
        $count_room_created = $this->db->where('hall_id', $hall_id)->count_all_results('room_booking');

        $page_data['total_bed'] = $max_limit->max_number_of_bed;
        $page_data['total_room_created'] = $count_room_created;

        $page_data['page_name'] = 'room_booking';
        $page_data['page_title'] = get_phrase('room_booking');
        $this->load->view('backend/index', $page_data);

    }

    /***MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD**/
    function noticeboard($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['notice_title'] = $this->input->post('notice_title');
            $data['notice'] = $this->input->post('notice');
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->insert('noticeboard', $data);
            $this->session->set_flashdata('success', '<b>Notice information saved!</b>');

            redirect(base_url() . 'index.php?admin_staff/noticeboard/', 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['notice_title'] = $this->input->post('notice_title');
            $data['notice'] = $this->input->post('notice');
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', $data);
            $this->session->set_flashdata('flash_message', get_phrase('notice_updated'));
            redirect(base_url() . 'index.php?admin_staff/noticeboard/', 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
                'notice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('notice_id', $param2);
            $this->db->delete('noticeboard');
            $this->session->set_flashdata('success', '<b>Notice information deleted!</b>');

            redirect(base_url() . 'index.php?admin_staff/noticeboard/', 'refresh');
        }
        $page_data['page_name'] = 'noticeboard';
        $page_data['page_title'] = get_phrase('manage_noticeboard');
        $page_data['notices'] = $this->db->get('noticeboard')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*****SITE/SYSTEM SETTINGS*********/
    function system_settings($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {

            $data['description'] = $this->input->post('system_name');
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('system_title');
            $this->db->where('type', 'system_title');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('address');
            $this->db->where('type', 'address');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('phone');
            $this->db->where('type', 'phone');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('paypal_email');
            $this->db->where('type', 'paypal_email');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('currency');
            $this->db->where('type', 'currency');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('system_email');
            $this->db->where('type', 'system_email');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('buyer');
            $this->db->where('type', 'buyer');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('system_name');
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('purchase_code');
            $this->db->where('type', 'purchase_code');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('language');
            $this->db->where('type', 'language');
            $this->db->update('settings', $data);

            $data['description'] = $this->input->post('text_align');
            $this->db->where('type', 'text_align');
            $this->db->update('settings', $data);

            redirect(base_url() . 'index.php?admin_staff/system_settings/', 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin_staff/system_settings/', 'refresh');
        }
        $page_data['page_name'] = 'system_settings';
        $page_data['page_title'] = get_phrase('system_settings');
        $page_data['settings'] = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*****LANGUAGE SETTINGS*********/
    function manage_language($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'edit_phrase') {
            $page_data['edit_profile'] = $param2;
        }
        if ($param1 == 'update_phrase') {
            $language = $param2;
            $total_phrase = $this->input->post('total_phrase');
            for ($i = 1; $i < $total_phrase; $i++) {
                //$data[$language]	=	$this->input->post('phrase').$i;
                $this->db->where('phrase_id', $i);
                $this->db->update('language', array($language => $this->input->post('phrase' . $i)));
            }
            redirect(base_url() . 'index.php?admin_staff/manage_language/edit_phrase/' . $language, 'refresh');
        }
        if ($param1 == 'do_update') {
            $language = $this->input->post('language');
            $data[$language] = $this->input->post('phrase');
            $this->db->where('phrase_id', $param2);
            $this->db->update('language', $data);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin_staff/manage_language/', 'refresh');
        }
        if ($param1 == 'add_phrase') {
            $data['phrase'] = $this->input->post('phrase');
            $this->db->insert('language', $data);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin_staff/manage_language/', 'refresh');
        }
        if ($param1 == 'add_language') {
            $language = $this->input->post('language');
            $this->load->dbforge();
            $fields = array(
                $language => array(
                    'type' => 'LONGTEXT'
                )
            );
            $this->dbforge->add_column('language', $fields);

            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin_staff/manage_language/', 'refresh');
        }
        if ($param1 == 'delete_language') {
            $language = $param2;
            $this->load->dbforge();
            $this->dbforge->drop_column('language', $language);
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));

            redirect(base_url() . 'index.php?admin_staff/manage_language/', 'refresh');
        }
        $page_data['page_name'] = 'manage_language';
        $page_data['page_title'] = get_phrase('manage_language');
        //$page_data['language_phrases'] = $this->db->get('language')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*****BACKUP / RESTORE / DELETE DATA PAGE**********/
    function backup_restore($operation = '', $type = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url(), 'refresh');

        if ($operation == 'create') {
            $this->crud_model->create_backup($type);
        }
        if ($operation == 'restore') {
            $this->crud_model->restore_backup();
            $this->session->set_flashdata('backup_message', 'Backup Restored');
            redirect(base_url() . 'index.php?admin_staff/backup_restore/', 'refresh');
        }
        if ($operation == 'delete') {
            $this->crud_model->truncate($type);
            $this->session->set_flashdata('backup_message', 'Data removed');
            redirect(base_url() . 'index.php?admin_staff/backup_restore/', 'refresh');
        }

        $page_data['page_info'] = 'Create backup / restore from backup';
        $page_data['page_name'] = 'backup_restore';
        $page_data['page_title'] = get_phrase('manage_backup_restore');
        $this->load->view('backend/index', $page_data);
    }

    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/
    function manage_profile($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_staff_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($param1 == 'update_profile_info') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');

            $this->db->where('admin_staff_id', $this->session->userdata('admin_staff_id'));
            $this->db->update('admin_staff', $data);
            $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            redirect(base_url() . 'index.php?admin_staff/manage_profile/', 'refresh');
        }
        if ($param1 == 'change_password') {
            $data['password'] = $this->input->post('password');
            $data['new_password'] = $this->input->post('new_password');
            $data['confirm_new_password'] = $this->input->post('confirm_new_password');

            $current_password = $this->db->get_where('admin_staff', array(
                'admin_staff_id' => $this->session->userdata('admin_staff_id')
            ))->row()->password;
            if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
                $this->db->where('admin_staff_id', $this->session->userdata('admin_staff_id'));
                $this->db->update('admin_staff', array(
                    'password' => $data['new_password']
                ));
                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
            } else {
                $this->session->set_flashdata('flash_message', get_phrase('password_mismatch'));
            }
            redirect(base_url() . 'index.php?admin_staff/manage_profile/', 'refresh');
        }
        $page_data['page_name'] = 'manage_profile';
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data'] = $this->db->get_where('admin_staff', array(
            'admin_staff_id' => $this->session->userdata('admin_staff_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
    }

}
