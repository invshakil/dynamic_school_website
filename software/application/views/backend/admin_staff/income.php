<div class="row">
    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('manage_cash_in'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_cash_in'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------->
        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box active" id="list">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th width="13%">
                            <div><?php echo get_phrase('cash_in_type'); ?></div>
                        </th>
                        <th width="15%">
                            <div><?php echo get_phrase('cash_in_title'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('cash_in_description'); ?></div>
                        </th>
                        <th width="10%">
                            <div><?php echo get_phrase('amount'); ?></div>
                        </th>
                        <th width="12%">
                            <div><?php echo get_phrase('date'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($income_info as $row): ?>
                        <tr>
                            <td><?php echo $row->category_name; ?></td>
                            <td><?php echo $row->income_title; ?></td>
                            <td><?php echo $row->income_description; ?></td>
                            <td><?php echo $row->amount.' BDT'; ?></td>
                            <td><?php echo date('d M,Y', strtotime($row->date)); ?></td>
                            <td>
                                <?php
                                $status = $row->status;
                                if ($status == 1) {
                                    echo '<span class="label label-success">Payment Received</span>';
                                } else {
                                    echo '<span class="label label-danger">Payment Yet To Receive</span>';
                                } ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <li class="divider"></li>

                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_income/<?php echo $row->income_id; ?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin_staff/income/delete/<?php echo $row->income_id; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url().'index.php?admin_staff/income/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                    <div class="form-group">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('cash_in_category'); ?></label>

                        <div class="col-sm-5">
                            <select name="income_category_id" class="form-control" style="width:150px; float:left;">
                                <option>--Select Category--</option>
                                <?php $categories = $this->db->get('income_category')->result_array();
                                foreach ($categories as $row) {
                                    ?>
                                    <option value="<?php echo $row['income_category_id'];?>"><?php echo $row['category_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('cash_in_title'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="income_title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('cash_in_description'); ?></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="income_description"
                                      placeholder="Textarea" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="amount"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date" value="" data-start-date="-2d" data-end-date="+4w">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                        <div class="col-sm-5">
                            <select name="status" class="form-control" style="width:100%;">
                                <option value="1"><?php echo get_phrase('payment_received'); ?></option>
                                <option value="2"><?php echo get_phrase('payment_yet_to_receive'); ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('add_cash_in'); ?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>

