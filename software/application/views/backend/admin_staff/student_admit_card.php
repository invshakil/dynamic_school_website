<?php
$this->session->flashdata('message');
?>
<div class="invoice">

    <div class="row">
        <div class="col-sm-4 invoice-left">

            <h3 class='alert alert-info'>Term Examination: <?php echo $exam_id;?></h3>

            <h4><b style="color:red" ;>Examinee Details</b></h4>
            Student Name: Shakil
            <br/>
            Class: 1
            <br/>
            Roll: 1

        </div>
        <div class="col-sm-2 invoice-right">

            <a href="#">
                <img src="<?php echo $this->crud_model->get_image_url('student', $student_id); ?>"
                     class="img-responsive img-circle"/>
            </a>

        </div>


    </div>


    <hr class="margin"/>

    <!--    --><?php //foreach ($student_profile_details as $profile) { ?>

    <div class="margin"></div>

    <div class="row">
        <div class="col-sm-6">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Subject List</th>
                    <th class="text-center">Date</th>
                </tr>
                </thead>

                <tbody>
                <!--        --><?php //foreach ($student_invoice_details as $invoice) { ?>
                <tr>
                    <td class="text-center">Bangla</td>
                    <td class="text-center">21/08/2017</td>
                </tr>
                <!--        --><?php //} ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
            Print Admit Card
            <i class="entypo-doc-text"></i>
        </a>
        <br/><br/>

        <div style="opacity: .3;">
            <img src="<?php echo base_url() ?>bg-logo.jpg" id="mainImg" height="75px" width="75" class="pull-right"/>
        </div>
    </div>


</div>
