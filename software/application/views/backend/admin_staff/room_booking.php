<div class="row">
    <div class="col-md-12">
        <?php
            $this->session->flashdata('message');
        ?>
        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('room_booked_history'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('reserve_new_room'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------->

        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box <?php if (!isset($edit_data)) echo 'active'; ?>" id="list">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable"
                       id="table_export">
                    <thead>
                    <tr>
                        <th>
                            <div><?php echo get_phrase('student_name'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('contact'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('room_fee'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('reserve_date'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('hall_name'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('floor_number'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('room_number'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1;
                    foreach ($booked_room as $row): ?>
                        <tr>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->phone; ?></td>
                            <td><?php echo $row->room_fee; ?></td>
                            <!--                            <td>-->
                            <?php //$date = date("d-M-Y", $row->date); echo $date;?><!--</td>-->
                            <td><?php echo $row->date; ?></td>
                            <td><?php echo $row->hall_name; ?></td>
                            <td><?php echo $row->floor_id; ?></td>
                            <td><?php echo $row->room_id; ?></td>
                            <td>
                                <?php if ($row->status == 1) {
                                    echo '<div class="label label-success">reserved</div>';
                                } elseif ($row->status == 2) {
                                    echo '<div class="label label-info">Paid & Booked</div>';
                                } else
                                    echo '<div class="label label-danger">Left Room</div>';
                                ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_room_booking/<?php echo $row->booking_info; ?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin_staff/room_booking/<?php echo $hall_id?>/delete/<?php echo $row->booking_info; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="col-md-12">
                    <B style="color:#3718ff;">Note: This hall can have maximum: <?php echo $total_bed;?> beds.</B><br/>
                    <B style="color:red;">Total students lives here: <?php if($total_room_created != '') {echo $total_room_created;} else{ echo 0;}?></B>
                </div>
            </div>
            <!----TABLE LISTING ENDS--->

            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url().'index.php?admin_staff/room_booking/' . $hall_id . '/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('select_class'); ?></label>
                        <div class="col-sm-5">
                            <select name="class_id" class="form-control">
                                <option value="">Select a class</option>
                                <?php
                                $classes	=	$this->db->get('class')->result_array();
                                foreach($classes as $row):?>
                                    <option value="<?php echo $row['class_id'];?>"
                                        <?php if(isset($class_id) && $class_id==$row['class_id'])echo 'selected="selected"';?>>
                                        <?php echo $row['name'];?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('select_section'); ?></label>
                        <div class="col-sm-5">
                            <select name="section_id" class="form-control" data-live-search="true">
                                <option selected>select section</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('enter_student_name'); ?></label>
                        <div class="col-sm-5">
                            <select name="student_id" class="select2" data-allow-clear="true"
                                    data-placeholder="Select student...">
                                <option></option>
                                <optgroup label="Student List">
<!--                                    --><?php
//                                    $this->db->order_by('class_id','asc');
//                                    $students = $this->db->get('student')->result_array();
//                                    foreach($students as $row):
//                                        ?>
<!--                                        <option value="--><?php //echo $row['student_id'];?><!--">-->
<!--                                            --><?php //echo $this->crud_model->get_class_name($row['class_id']);?><!-- --->
<!--                                            roll --><?php //echo $row['roll'];?><!-- --->
<!--                                            --><?php //echo $row['name'];?>
<!--                                        </option>-->
<!--                                        --><?php
//                                    endforeach;
//                                    ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('hall_name'); ?></label>
                        <div class="col-sm-5">
                            <?php $hall_name = $this->db->get_where('dormitory', array('hall_id' => $hall_id))->row(); ?>
                            <input type="text" class="form-control" name="hall_name"
                                   value="<?php echo $hall_name->name; ?>" placeholder="<?php echo $hall_name->name; ?>"
                                   disabled/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('floor_number'); ?></label>
                        <div class="col-sm-5">
                            <select name="floor_id" class="form-control"  style="float:left;">

                                    <?php $info = $this->db->select('distinct floor_id', false)->get_where('hall_room_info', array('hall_id' => $hall_id))->result_array();

                                    foreach ($info as $row) { ?>
                                        <option
                                            value="<?php echo $row['floor_id'] ?>"><?php echo 'Floor - ' . $row['floor_id'] ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('room_number'); ?></label>
                        <div class="col-sm-5">
                            <select name="room_id" class="form-control"  style="float:left;">

                                    <?php $info = $this->db->get_where('hall_room_info', array('hall_id' => $hall_id))->result_array();

                                    foreach ($info as $row) { ?>
                                        <option
                                            value="<?php echo $row['room_id'] ?>"><?php echo $row['room_id'] ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('room_fee'); ?></label>
                        <div class="col-sm-5">
                            <select name="room_fee" class="form-control"  style="float:left;">

                                    <?php $info = $this->db->select('distinct room_fee', false)->get_where('hall_room_info', array('hall_id' => $hall_id))->result_array();

                                    foreach ($info as $row) { ?>
                                        <option
                                            value="<?php echo $row['room_fee'] ?>"><?php echo $row['room_fee'] ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date" value=""
                                   data-start-date="-2d" data-end-date="+4w">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                        <div class="col-sm-5">
                            <select name="status" class="form-control" style="width:100%;">
                                <option value="1"><?php echo get_phrase('reserved'); ?></option>
                                <option value="2"><?php echo get_phrase('paid & booked'); ?></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('comment'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="comment"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('reserve_the_room'); ?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {
            var class_id = $(this).val();
            console.log(class_id);
            $('select[name="section_id"]').html('<option>select section</option>');
            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_class_id/' + class_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
//                    $('select[name="section_id"]').empty();
                    $.each(data, function (key, value) {

                        $('select[name="section_id"]').append('<option value="' + value.section_id + '">' + value.section_name + '</option>');
                    });
                }
            });


        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="section_id"]').on('change', function () {
            var section_id = $(this).val();
            console.log(section_id);

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_student_id/' + section_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="student_id"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="student_id"]').append('<option value="' + value.student_id + '">' + value.name + '</option>');
                    });
                }
            });

        });
    });
</script>
