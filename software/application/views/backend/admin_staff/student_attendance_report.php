<div class="row">

    <?php if ($date1 == '' && $date2 == ''): ?>

        <div class="col-md-12">
            <h1>Attendance report by Date Range</h1>
            <hr/>
            <div class="box-content">
                <?php echo form_open(base_url().'index.php?admin_staff/student_attendance_report/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('select_class'); ?></label>
                    <div class="col-sm-5">
                        <select name="class_id" class="form-control">
                            <option value="">Select a class</option>
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach ($classes as $row):?>
                                <option value="<?php echo $row['class_id']; ?>"
                                    <?php if (isset($class_id) && $class_id == $row['class_id']) echo 'selected="selected"'; ?>>
                                    <?php echo $row['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('select_section'); ?></label>
                    <div class="col-sm-5">
                        <select name="section_id" class="form-control" data-live-search="true">
                            <option selected>Select Class First</option>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Choose First Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date1" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Choose Second Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date2" value=""
                               data-start-view="1">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('get_report'); ?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    <?php endif; ?>


    <?php if ($date1 != '' && $date2 != ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h1 class="text-center"><?php echo 'Attendance Report: ' . $date1 . ' - ' . $date2; ?></h1>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center" width="10%">
                        <div><?php echo get_phrase('student_name'); ?></div>
                    </th class="text-center">
                    <!--                    <th class="text-center">-->
                    <!--                        <div>--><?php //echo get_phrase('employee_name'); ?><!--</div>-->
                    <!--                    </th class="text-center">-->
                    <?php foreach ($attendance_date as $row) { ?>
                        <th class="text-center"><?php $date = new DateTime($row->date);
                            echo $date->format('d-M-y') ?></th>
                    <?php } ?>
                    <!--                    <th class="text-center">Status</th>-->

                </tr>
                </thead>
                <tbody>


                </tbody>
                <?php
                foreach ($student_id as $row) { ?>

                    <tr>
                        <td class="text-center"><?php echo '<b>' . $row->name . '</b>' ?></td>

                            <?php $id = $row->student_id;
                            $info = $this->db->order_by('date', 'asc')
                                ->get_where('attendance', array('student_id' => $id, 'date >=' => $date1, 'date <=' => $date2))->result();
                            foreach ($info as $row1) {
                                ?>
                                <td class="text-center">

                                    <?php
                                        if ($row1->status == 1) {
                                            echo '<div class="label label-success">Present</div>';
                                        } else echo '<div class="label label-danger">Absent</div>';
                                    }
                                    ?>
                                </td>

                            <?php
                         ?>

                    </tr>
                <?php } ?>


            </table>


            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Attendance Report
                <i class="entypo-doc-text"></i>
            </a>
            <br/><br/>
            <div style="opacity: .3;">
                <img src="<?php echo base_url() ?>bg-logo.jpg" id="mainImg" height="75px" width="75"
                     class="pull-right"/>
            </div>
        </div>

    <?php endif; ?>

    <?php if ($employee_name != '' && $date1 != '' && $date2 != ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h3 class="text-center"><?php echo 'Attendance Report of ' . $employee_name . ': Between ' . date('d.M.Y', $date1) . ' - ' . date('d.M.Y', $date2); ?></h3>
                    <h3 class="text-center">Employee Type: <b><?php if ($employee_type == 1) {
                                echo 'Admin';
                            } elseif ($employee_type == 2) {
                                echo 'Teacher';
                            } else {
                                echo 'Staff';
                            }; ?></b></h3>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">Date</th>
                    <th class="text-center">Status</th>

                </tr>
                </thead>
                <tbody>


                </tbody><?php foreach ($attendance_info_by_employee as $row) { ?>

                    <tr>
                        <td class="text-center">
                            <?php $date = date('d-M-Y', $row->date);
                            echo $date; ?></td>
                        <td class="text-center">
                            <?php if ($row->status == 1) {
                                echo '<div class="label label-success">Present</div>';
                            } else echo '<div class="label label-danger">Absent</div>'; ?>
                        </td>

                    </tr>
                <?php } ?>

            </table>


            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Attendance Report
                <i class="entypo-doc-text"></i>
            </a>
            <br/><br/>
            <div style="opacity: .3;">
                <img src="<?php echo base_url() ?>bg-logo.jpg" id="mainImg" height="75px" width="75"
                     class="pull-right"/>
            </div>
        </div>

    <?php endif; ?>


</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {
            var class_id = $(this).val();
            console.log(class_id);
            $('select[name="section_id"]').html('<option>Select Section</option>');
            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_class_id/' + class_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
//                    $('select[name="section_id"]').empty();
                    $.each(data, function (key, value) {

                        $('select[name="section_id"]').append('<option value="' + value.section_id + '">' + value.section_name + '</option>');
                    });
                }
            });


        });
    });
</script>

<script type="text/javascript">
    function printImg() {
        pwin = window.open(document.getElementById("mainImg").src, "_blank");
        pwin.onload = function () {
            window.print();
        }
    }
</script>