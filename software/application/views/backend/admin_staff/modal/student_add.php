<?php
$this->session->flashdata('message');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('addmission_form'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin_staff/student/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" data-validate="required"
                               data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('class'); ?></label>

                    <div class="col-sm-5">
                        <select name="class_id" id="class_id" class="form-control" data-validate="required"
                                data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach ($classes as $row):
                                ?>
                                <option value="<?php echo $row['class_id']; ?>">
                                    <?php echo $row['name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('section'); ?></label>

                    <div class="col-sm-5">
                        <select name="section_id" id="section_id" class="form-control" data-live-search="true">

                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2"
                           class="col-sm-3 control-label"><?php echo get_phrase('registration_number'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="reg_id" id="reg_no" value="" disabled>
                        <div id="reg_id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('roll_number'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="roll" id="roll_no" value="" disabled>
                        <div id="roll"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="2">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>

                    <div class="col-sm-5">
                        <select name="sex" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="male"><?php echo get_phrase('male'); ?></option>
                            <option value="female"><?php echo get_phrase('female'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2"
                           class="col-sm-3 control-label"><?php echo get_phrase('previous_school'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="previous_school" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo'); ?></label>

                    <div class="col-sm-5">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;"
                                 data-trigger="fileinput">
                                <img src="http://placehold.it/200x200" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('add_student'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<style>
    #reg_id, #roll {
        font-weight: bold;
        color: red;
    }

</style>


<script type="text/javascript">

    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {
            var class_id = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_class_id/' + class_id,
                type: "GET",
                dataType: "json",
                success: function (data) {

                    $('select[name="section_id"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="section_id"]').append('<option value="' + value.section_id + '">' + value.section_name + '</option>');
                    });
                }
            });


        });
    });

    $(document).ready(function () {
        $('input[name="reg_id"]').on('keyup', function () {
            var reg_id = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_latest_reg_id/' + reg_id,
                type: "GET",
                dataType: "json",
                success: function (data) {

                    if (data.length != 0) {

                        $('#reg_id').html('<p class="well">This Registration ID has been assigned to another student, please input new Registration ID!</p>');
                        $("button").attr('disabled', 'disabled');

                    }
                    else {
                        $('#reg_id').html('');
                        $("button").removeAttr('disabled');
                    }
                }
            });


        });
    });

    $(document).ready(function () {
        $('input[name="roll"]').on('keyup', function () {
            var class_id = $("#class_id").val();
            var section_id = $('#section_id').val();
            var roll = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_latest_roll_by_section/' + class_id + '/' + section_id + '/' + roll,
                type: "GET",
                dataType: "json",
                success: function (data) {

                    if (data.length != 0) {

                        $('#roll').html('<p class="well">This Roll Number has been assigned to another student, please input new Roll Number!</p>');
                        $("button").attr('disabled', 'disabled');

                    }
                    else {
                        $('#roll').html('');
                        $("button").removeAttr('disabled');
                    }
                }
            });

        });
    });

    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {

            $("#reg_no").removeAttr('disabled');
            $("#roll_no").removeAttr('disabled');
        })
    });
</script>