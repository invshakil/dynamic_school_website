<?php
$edit_data = $this->db->get_where('invoice', array('invoice_id' => $param2))->result_array();
?>

<div class="tab-pane box active" id="edit" style="padding: 20px">
    <div class="box-content" id="elem">
        <?php foreach ($edit_data as $row): ?>
            <table>
                <tr>
                    <td width="83%">
                        <div class="pull-left">
                            <span style="font-size:20px;font-weight:200;">
                                Payment To
                            </span>
                            <br/>
                            <?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?>
                            <br/>
                            <?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?>
                        </div>
                    </td>
                    <td width="20%">
                        <div class="pull-right">
                            <span style="font-size:20px;font-weight:200;">
                                Bill To
                            </span>
                            <br/>
                            <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name; ?>
                            <br/>
                            <?php echo get_phrase('roll'); ?> :
                            <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->roll; ?>
                            <br/>
                            <?php echo get_phrase('class'); ?> :
                            <?php
                            $class_id = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->class_id;
                            echo $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
                            ?>
                        </div>
                    </td>
                </tr>
            </table>


            <div style="clear:both;"></div>
            <hr/>
            <table width="100%">
                <tr style="background-color:#7087A3; color:#fff; padding:5px;">
                    <td style="padding:5px;">Invoice Report</td>
                    <td width="30%" style="padding:5px;">
                        <div class="pull-right">
                            Bill Amount
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
					<span style="font-size:20px;font-weight:200;">
						<?php echo $row['title']; ?>
                    </span>
                        <br/>
                        <?php echo $row['description']; ?>
                    </td>
                    <td width="30%" style="padding:5px;">
                        <div class="pull-right">
						<span style="font-size:20px;font-weight:200;">
							<?php echo $row['amount']; ?>
                        </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td width="30%" style="padding:5px;">
                        <div class="pull-right">
                            <hr/>
                            Payment Status : <?php echo url_title($row['status']); ?>
                            <br/>
                            Invoice ID : #<?php echo $row['invoice_id']; ?>
                            <br/>
                            Invoice Date : <?php echo date('m/d/Y', $row['creation_timestamp']); ?>
                        </div>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <div style="opacity: .3;">
                <img src="<?php echo base_url() ?>bg-logo.jpg" id="mainImg" height="75px" width="75"
                     class="pull-right"/>
            </div>

        <?php endforeach; ?>
    </div>
    <a onclick="printImg()" class="btn btn-primary btn-icon icon-left hidden-print pull-left">
        Print Student Invoice
        <i class="entypo-doc-text"></i>
    </a>


</div>

<script type="text/javascript">

    function printImg(elem) {
        try {
            var printContent = document.getElementById('elem').innerHTML;
            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=800,height=600');
            printWindow.document.write(printContent);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
        catch (e) {
            self.print();
        }
    }
</script>
