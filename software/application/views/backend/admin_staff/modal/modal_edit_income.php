<?php
$edit_data = $this->db->get_where('income', array('income_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="tab-pane box" id="add" style="padding: 5px">
        <div class="box-content">
            <?php echo form_open(base_url().'index.php?admin_staff/income/do_update/' . $row['income_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="form-group">
                <label for="field-2"
                       class="col-sm-3 control-label"><?php echo get_phrase('income_category'); ?></label>
                <div class="col-sm-5">
                    <select name="income_category_id" class="form-control" style="width:150px; float:left;">
                        <option>--Select Category--</option>
                        <?php $categories = $this->db->get('income_category')->result_array();
                        foreach ($categories as $row1) {
                            ?>
                            <option
                                value="<?php echo $row1['income_category_id']; ?>" <?php if ($row['income_category_id'] == $row1['income_category_id']) echo 'selected'; ?>><?php echo $row1['category_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('income_title'); ?></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="income_title" value="<?php echo $row['income_title']?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="field-2"
                       class="col-sm-3 control-label"><?php echo get_phrase('income_description'); ?></label>
                <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="income_description"
                                      placeholder="Textarea" rows="6"><?php echo $row['income_description'] ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="amount" value="<?php echo $row['amount'] ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                <div class="col-sm-5">
                    <input type="text" class="form-control datepicker" name="date" value="<?php echo $row['date'] ?>"
                           data-start-date="-2d" data-end-date="+4w">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                <div class="col-sm-5">
                    <select name="status" class="form-control" style="width:100%;">
                        <option
                            value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>><?php echo get_phrase('payment_received'); ?></option>
                        <option
                            value="2" <?php if ($row['status'] == 2) echo 'selected'; ?>><?php echo get_phrase('payment_yet_to_receive'); ?></option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit"
                            class="btn btn-info"><?php echo get_phrase('update_income_information'); ?></button>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php endforeach; ?>