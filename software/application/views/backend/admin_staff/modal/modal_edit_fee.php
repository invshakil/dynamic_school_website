<?php
$edit_data		=	$this->db->get_where('student_fee' , array('fee_id' => $param2) )->result_array();

//echo '<pre>';
//print_r($edit_data);
//exit();
?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row1):?>
            <?php echo form_open(base_url().'index.php?admin_staff/issue_student_fee/do_update/'.$row1['fee_id'], array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
            <div class="form-group" >
                <label for="field-2"
                       class="col-sm-3 control-label"><?php echo get_phrase('fee_category'); ?></label>

                <div class="col-sm-5">
                    <select name="fee_category_id" id="fee_type" onchange="feeType(this)" class="form-control" style="width:150px; float:left;">
                        <option>--Select Category--</option>
                        <?php $categories = $this->db->get('fee_category')->result_array();
                        foreach ($categories as $row) {
                            ?>
                            <option
                                value="<?php echo $row['fee_category_id']; ?>" <?php if ($row['fee_category_id'] == $row1['fee_category_id']) echo 'selected'?>>
                                <?php echo $row['category_name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <?php if ($row1['fee_category_id'] == 3) { ?>
            <div class="form-group">
                <label for="field-2"
                       class="col-sm-3 control-label"><?php echo get_phrase('select_term_exam'); ?></label>

                <div class="col-sm-5">
                    <select name="exam_id" class="form-control" style="width:150px; float:left;">
                        <option>--Select term--</option>
                        <?php $categories = $this->db->get('exam')->result_array();
                        foreach ($categories as $row) {
                            ?>
                            <option
                                value="<?php echo $row['exam_id']; ?>" <?php if($row['exam_id'] == $row1['exam_id']) echo 'selected'?>>
                                <?php echo $row['name']; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } else {?>

            <div class="form-group" id="exam_id">
                <label for="field-2"
                       class="col-sm-3 control-label"><?php echo get_phrase('select_term_exam'); ?></label>

                <div class="col-sm-5">
                    <select name="exam_id" class="form-control" style="width:150px; float:left;">
                        <option>--Select term--</option>
                        <?php $categories = $this->db->get('exam')->result_array();
                        foreach ($categories as $row) {
                            ?>
                            <option
                                value="<?php echo $row['exam_id']; ?>"><?php echo $row['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        <?php }  ?>

            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('fee_title'); ?></label>

                <div class="col-sm-5">
                    <input type="text" class="form-control" name="fee_title" value="<?php echo $row1['fee_title']?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('fee_description'); ?></label>

                <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="fee_description" placeholder="Textarea" rows="12"><?php echo $row1['fee_description']; ?>
                            </textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>

                <div class="col-sm-5">
                    <input type="number" class="form-control" name="amount" value="<?php echo $row1['amount']?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                <div class="col-sm-5">
                    <input type="text" class="form-control datepicker" name="date" value="" data-start-view="2">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('status');?></label>
                <div class="col-sm-5">
                    <select name="status" class="form-control">
                        <option value="1" <?php if($row1['status']==1)echo 'selected';?>><?php echo get_phrase('paid');?></option>
                        <option value="2" <?php if($row1['status']==2)echo 'selected';?>><?php echo get_phrase('unpaid');?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_fee_issue');?></button>
                </div>
            </div>
            </form>
        <?php endforeach;?>
    </div>
</div>


<script>
    function feeType(value) {
        var fee_type = value.value;
        if (fee_type != 3)
        {
            $("#exam_id").hide();
        }
        else
        {
            $("#exam_id").show();
        }
    }
</script>