<?php
$edit_data = $this->db->get_where('salary', array('salary_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_salary'); ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php echo form_open(base_url().'index.php?admin_staff/salary/do_update/' . $row['salary_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="amount" value="<?php echo $row['amount']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date" value="<?php echo $row['date']; ?>" data-start-view="2">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('salary_description'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="salary_description" value="<?php echo $row['salary_description']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('payment_status'); ?></label>

                        <div class="col-sm-5">
                            <select name="status" class="form-control">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>><?php echo get_phrase('Paid'); ?></option>
                                <option value="0" <?php if ($row['status'] == 0) echo 'selected'; ?>><?php echo get_phrase('Unpaid'); ?></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('edit_staff'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
endforeach;
?>