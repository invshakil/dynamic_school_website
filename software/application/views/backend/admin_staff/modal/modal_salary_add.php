<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('record_salary_payment'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url().'index.php?admin_staff/salary/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('employee_type'); ?></label>

                    <div class="col-sm-5">
                        <select name="employee_type" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="1" ><?php echo get_phrase('admin_staff'); ?></option>
                            <option value="2"><?php echo get_phrase('teacher'); ?></option>
                            <option value="3"><?php echo get_phrase('staff'); ?></option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                    <div class="col-sm-5">
                        <select name="employee_id" class="form-control" data-live-search="true">

                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="amount" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date" value="" data-start-view="2">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('salary_description'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="salary_description" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('payment_status'); ?></label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="1"><?php echo get_phrase('Paid'); ?></option>
                            <option value="0"><?php echo get_phrase('Unpaid'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('record_salary_payment'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="employee_type"]').on('change', function() {
            var employee_type = $(this).val();
            console.log(employee_type);
            if(employee_type == 1) {
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/'+employee_type,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="employee_id"]').append('<option value="'+ value.admin_staff_id +'">'+ value.name +'</option>');
                        });
                    }
                });
            }
            else if(employee_type == 2){
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/'+employee_type,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="employee_id"]').append('<option value="'+ value.teacher_id +'">'+ value.name +'</option>');
                        });
                    }
                });
            }
            else if(employee_type == 3){
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/'+employee_type,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="employee_id"]').append('<option value="'+ value.staff_id +'">'+ value.name +'</option>');
                        });
                    }
                });
            }
            else{
                $('select[name="employee_id"]').empty();
            }
        });
    });
</script>
