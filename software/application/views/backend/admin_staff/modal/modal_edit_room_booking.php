<?php
$edit_data = $this->db->get_where('room_booking', array(
    'booking_info' => $param2
))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row1): ?>
            <?php echo form_open(base_url().'index.php?admin_staff/room_information/' . $row1['hall_id'] . '/do_update/' . $row1['room_booking'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('room_number'); ?></label>
                    <div class="col-sm-5">
                        <select name="room_id" class="form-control" style="float:left;">

                            <?php $info = $this->db->get_where('hall_room_info', array('hall_id' => $row1['hall_id']))->result_array();

                            foreach ($info as $row) { ?>
                                <option
                                    value="<?php echo $row1['room_id'] ?>"
                                    <?php if ($row['room_id'] = $row1['room_id']) echo 'selected' ?>
                                ><?php echo $row1['room_id'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('room_fee'); ?></label>
                    <div class="col-sm-5">
                        <select name="room_fee" class="form-control" style="float:left;">

                            <?php $info = $this->db->select('distinct room_fee', false)->get_where('hall_room_info', array('hall_id' => $row1['hall_id']))->result_array();

                            foreach ($info as $row) { ?>
                                <option
                                    value="<?php echo $row1['room_fee'] ?>"

                                ><?php echo $row1['room_fee'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date"
                               value="<?php echo $row1['date'] ?>"
                               data-start-date="-2d" data-end-date="+4w">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                    <div class="col-sm-5">
                        <select name="status" class="form-control" style="width:100%;">
                            <option
                                value="1" <?php if ($row['status'] == 1) echo 'selected' ?>><?php echo get_phrase('reserved'); ?></option>
                            <option
                                value="2" <?php if ($row['status'] == 2) echo 'selected' ?>><?php echo get_phrase('paid & booked'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit"
                            class="btn btn-info"><?php echo get_phrase('edit_room_booking_info'); ?></button>
                </div>
            </div>
            </form>
        <?php endforeach; ?>
    </div>
</div>