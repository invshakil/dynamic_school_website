<?php
$edit_data = $this->db->get_where('student', array('student_id' => $param2))->result_array();

foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_student'); ?>
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(base_url().'index.php?admin_staff/student/' . $row['class_id'] . '/do_update/' . $row['student_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>


                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo'); ?></label>

                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;"
                                     data-trigger="fileinput">
                                    <img
                                        src="<?php echo $this->crud_model->get_image_url('student', $row['student_id']); ?>"
                                        alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px"></div>
                                <div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
                                    <a href="#" class="btn btn-orange fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" data-validate="required"
                                   data-message-required="<?php echo get_phrase('value_required'); ?>"
                                   value="<?php echo $row['name']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('class'); ?></label>

                        <div class="col-sm-5">
                            <select name="class_id" id="class_id" class="form-control" data-validate="required"
                                    data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <?php
                                $classes = $this->db->get('class')->result_array();
                                foreach ($classes as $row2):
                                    ?>
                                    <option value="<?php echo $row2['class_id']; ?>"
                                    <?php if ($row['class_id'] == $row2['class_id']) echo 'selected';?>>
                                        <?php echo $row2['name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('section'); ?></label>

                        <div class="col-sm-5">
                            <select name="section_id" id="section_id" class="form-control" data-live-search="true">
                                <?php
                                $sections = $this->db->get_where('section', array('class_id' => $row['class_id']))->result_array();
                                foreach ($sections as $row3):
                                    ?>
                                    <option value="<?php echo $row3['section_id']; ?>"
                                        <?php if ($row['section_id'] == $row3['section_id']) echo 'selected';?>>
                                        <?php echo $row3['section_name']; ?>
                                    </option>
                                    <?php
                                endforeach;
                                ?>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('registration_number'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="reg_id" value="<?php echo $row['reg_id']; ?>">
                            <div id="reg_id"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('roll'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="roll" value="<?php echo $row['roll']; ?>">
                            <div id="roll"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('birthday'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="birthday"
                                   value="<?php echo $row['birthday']; ?>" data-start-view="2">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>

                        <div class="col-sm-5">
                            <select name="sex" class="form-control">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <option
                                    value="male" <?php if ($row['sex'] == 'male') echo 'selected'; ?>><?php echo get_phrase('male'); ?></option>
                                <option
                                    value="female"<?php if ($row['sex'] == 'female') echo 'selected'; ?>><?php echo get_phrase('female'); ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="address"
                                   value="<?php echo $row['address']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="phone" value="<?php echo $row['phone']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="email" value="<?php echo $row['email']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('previous_school'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="previous_school"
                                   value="<?php echo $row['previous_school']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('edit_student'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>


<style>
    #reg_id, #roll {
        font-weight: bold;
        color: red;
    }

</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {
            var class_id = $(this).val();
            console.log(class_id);

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_class_id/' + class_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="section_id"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="section_id"]').append('<option value="' + value.section_id + '">' + value.section_name + '</option>');
                    });
                }
            });


        });
    });

    $(document).ready(function () {
        $('input[name="reg_id"]').on('keyup', function () {
            var reg_id = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_latest_reg_id/' + reg_id,
                type: "GET",
                dataType: "json",
                success: function (data) {

                    if (data.length != 0) {
                        student_name = '<?php echo $row['name'];?>';

                        if (data != student_name)
                        {
                            $('#reg_id').html('<p class="well">This Registration ID has been assigned to another student, please input new Registration ID!</p>');
                            $("button").attr('disabled', 'disabled');
                        }
                        else {
                            $('#reg_id').html('<p class="well">It is current Registration ID of this student!</p>');
                            $("button").removeAttr('disabled');
                        }


                    }
                    else {
                        $('#reg_id').html('');
                        $("button").removeAttr('disabled');
                    }
                }
            });


        });
    });

    $(document).ready(function () {
        $('input[name="roll"]').on('keyup', function () {
            var class_id = $("#class_id").val();
            var section_id = $('#section_id').val();
            var roll = $(this).val();

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_latest_roll_by_section/' + class_id + '/' + section_id + '/' + roll,
                type: "GET",
                dataType: "json",
                success: function (data) {

                    if (data.length != 0) {

                        if (data != student_name)
                        {
                            $('#roll').html('<p class="well">This Roll Number has been assigned to another student, please input new Roll Number!</p>');
                            $("button").attr('disabled', 'disabled');
                        }
                        else {
                            $('#roll').html('<p class="well">It is current Roll Number of this student!</p>');
                            $("button").removeAttr('disabled');
                        }
                    }
                    else {
                        $('#roll').html('');
                        $("button").removeAttr('disabled');
                    }
                }
            });

        });
    });
</script>
    <?php
endforeach;
?>

