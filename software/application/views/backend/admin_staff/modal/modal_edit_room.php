<?php
$edit_data = $this->db->get_where('hall_room_info', array(
    'room_info' => $param2
))->result_array();
?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <?php echo form_open(base_url().'index.php?admin_staff/room_information/'.$row['hall_id'].'/do_update/'.$row['room_info'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('room_number');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="room_id" value="<?php echo $row['room_id'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('floor_number');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="floor_id" value="<?php echo $row['floor_id'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('number_of_bed');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="number_of_bed" value="<?php echo $row['number_of_bed'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('room_rent_fee');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="room_fee" value="<?php echo $row['room_fee'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="description" value="<?php echo $row['description'];?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_room_info');?></button>
                </div>
            </div>
            </form>
        <?php endforeach;?>
    </div>
</div>