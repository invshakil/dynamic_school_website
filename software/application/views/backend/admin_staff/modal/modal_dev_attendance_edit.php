<?php
$edit_data = $this->db->get_where('device_attendance', array('device_attendance_id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <?php echo form_open(base_url() . 'index.php?admin_staff/manage_emp_attendance/do_update/' . $row['device_attendance_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('Staff'); ?></label>
                <div class="col-sm-5">
                    <select name="staff_id" class="form-control" disabled>
                        <?php
                        $teacher = $this->db->get('teacher')->result_array();
                        foreach ($teacher as $row2):
                            ?>
                            <option value="<?php echo $row2['staff_code']; ?>"
                                <?php if ($row['staff_id'] == $row2['staff_code']) echo 'selected'; ?>><?php echo $row2['name']; ?></option>
                            <?php
                        endforeach;

                        $staff = $this->db->get('staff')->result_array();
                        foreach ($staff as $row2):
                            ?>
                            <option value="<?php echo $row2['staff_code']; ?>"
                                <?php if ($row['staff_id'] == $row2['staff_code']) echo 'selected'; ?>><?php echo $row2['name']; ?></option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Time &amp; Date Picker</label>

                <div class="col-sm-8">

                    <?php $datetime = $row['datetime'];
                    $time = substr($datetime, 11);
                    ?>

                    <div class="date-and-time">
                        <input type="text" name="date" value="<?php echo $row['date']?>"  class="form-control datepicker" data-format="dd.mm.yyyy">
                        <input type="text" name="time" value="<?php echo $time?>"  class="form-control timepicker" data-template="dropdown" data-show-seconds="true" data-default-time="<?php echo $time?>" data-show-meridian="true" data-minute-step="1" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                <div class="col-sm-5">
                    <select name="status" class="form-control">
                        <option
                            value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>><?php echo get_phrase('present'); ?></option>
                        <option
                            value="0" <?php if ($row['status'] == 0) echo 'selected'; ?>><?php echo get_phrase('absent'); ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-info btn-icon"><?php echo get_phrase('edit_attendance_info'); ?><i class="entypo-check"></i></button>
                </div>
            </div>
            </form>


        <?php endforeach; ?>
    </div>
</div>

