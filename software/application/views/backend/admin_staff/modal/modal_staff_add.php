<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_staff'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url().'index.php?admin_staff/staff/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('staff_type'); ?></label>

                    <div class="col-sm-5">
                        <select name="staff_type" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="office staff"><?php echo get_phrase('office_staff'); ?></option>
                            <option value="non office staff"><?php echo get_phrase('non_office_staff'); ?></option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" data-validate="required"
                               data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('staff_id');?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="staff_code" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
                        <div id="staff_code"></div>
                    </div>

                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="2">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>

                    <div class="col-sm-5">
                        <select name="sex" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="male"><?php echo get_phrase('male'); ?></option>
                            <option value="female"><?php echo get_phrase('female'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="password" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo'); ?></label>

                    <div class="col-sm-5">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;"
                                 data-trigger="fileinput">
                                <img src="http://placehold.it/200x200" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('add_staff'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<style>
    #staff_code
    {
        font-weight: bold;
        color: red;
    }
</style>

<script>
    $(document).ready(function () {
        $('input[name="staff_code"]').on('keyup', function () {
            var staff_code = $(this).val();

            console.log(staff_code);

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_latest_staff_code/' + staff_code,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if(data.length != 0)
                    {

                        $('#staff_code').html('<p class="well">This ID has been assigned to <b style="color: #0c3c50;">'+ data +'</b>, please input new ID!</p>');
                        $("button").attr('disabled','disabled');

                    }
                    else
                    {
                        $('#staff_code').html('');
                        $("button").removeAttr('disabled');
                    }
                }
            });


        });
    });
</script>