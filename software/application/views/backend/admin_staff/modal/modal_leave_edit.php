<?php
$edit_data = $this->db->get_where('employee_leave', array('leave_id' => $param2))->result_array();
foreach ($edit_data as $row):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_salary'); ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php echo form_open(base_url().'index.php?admin_staff/leave_management/do_update/' . $row['leave_id'], array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('application_title'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="application_title" value="<?php echo $row['application_title']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('application_description'); ?></label>

                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="application_description" placeholder="Textarea" rows="12"><?php echo $row['application_description']; ?>
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('leave_date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="leave_date" value="<?php echo $row['leave_date']; ?>" data-start-date="-2d" data-end-date="+4w">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('rejoin_date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="rejoin_date" value="<?php echo $row['rejoin_date']; ?>" data-start-date="-2d" data-end-date="+4w">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('approve_status'); ?></label>

                        <div class="col-sm-5">
                            <select name="status" class="form-control">
                                <option value=""><?php echo get_phrase('select'); ?></option>
                                <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>><?php echo get_phrase('approved'); ?></option>
                                <option value="0" <?php if ($row['status'] == 2) echo 'selected'; ?>><?php echo get_phrase('pending'); ?></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('edit_staff'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
endforeach;
?>