<?php
$edit_data		=	$this->db->get_where('fee_category' , array('fee_category_id' => $param2) )->result_array();
?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <?php echo form_open(base_url().'index.php?admin_staff/fee_category/do_update/'.$row['fee_category_id'], array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('category_name');?></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="category_name" value="<?php echo $row['category_name'];?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('category_description'); ?></label>

                <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="category_description" placeholder="Textarea" rows="12"><?php echo $row['category_description']; ?>
                            </textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo get_phrase('status');?></label>
                <div class="col-sm-5">
                    <select name="status" class="form-control">
                        <option value="1" <?php if($row['status']==1)echo 'selected';?>><?php echo get_phrase('visible');?></option>
                        <option value="2" <?php if($row['status']==2)echo 'selected';?>><?php echo get_phrase('private');?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-info"><?php echo get_phrase('edit_fee_category');?></button>
                </div>
            </div>
            </form>
        <?php endforeach;?>
    </div>
</div>