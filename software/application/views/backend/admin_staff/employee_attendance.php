<?php
$this->session->flashdata('message');
?>
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
    <thead>
    <tr>
        <th><?php echo get_phrase('select_date'); ?></th>
        <th><?php echo get_phrase('select_month'); ?></th>
        <th><?php echo get_phrase('select_year'); ?></th>
        <th><?php echo get_phrase('select_employee_type'); ?></th>
        <th><?php echo get_phrase('manage_attendance'); ?></th>
    </tr>
    </thead>
    <tbody>
    <form method="post" action="<?php echo base_url(); ?>index.php?admin_staff/emp_attendance_selector" class="form">
        <tr class="gradeA">
            <td>
                <select name="date" class="form-control">
                    <?php for ($i = 1; $i <= 31; $i++): ?>
                        <option value="<?php echo $i; ?>"
                            <?php if (isset($date) && $date == $i) echo 'selected="selected"'; ?>>
                            <?php echo $i; ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </td>
            <td>
                <select name="month" class="form-control">
                    <?php
                    for ($i = 1; $i <= 12; $i++):
                        if ($i == 1) $m = 'january';
                        else if ($i == 2) $m = 'february';
                        else if ($i == 3) $m = 'march';
                        else if ($i == 4) $m = 'april';
                        else if ($i == 5) $m = 'may';
                        else if ($i == 6) $m = 'june';
                        else if ($i == 7) $m = 'july';
                        else if ($i == 8) $m = 'august';
                        else if ($i == 9) $m = 'september';
                        else if ($i == 10) $m = 'october';
                        else if ($i == 11) $m = 'november';
                        else if ($i == 12) $m = 'december';
                        ?>
                        <option value="<?php echo $i; ?>"
                            <?php if ($month == $i) echo 'selected="selected"'; ?>>
                            <?php echo $m; ?>
                        </option>
                        <?php
                    endfor;
                    ?>
                </select>
            </td>
            <td>
                <select name="year" class="form-control">
                    <?php for ($i = 2020; $i >= 2010; $i--): ?>
                        <option value="<?php echo $i; ?>"
                            <?php if (isset($year) && $year == $i) echo 'selected="selected"'; ?>>
                            <?php echo $i; ?>
                        </option>
                    <?php endfor; ?>
                </select>
            </td>
            <td>
                <select name="employee_type" class="form-control">
                    <?php
                    if ($employee_type == NULL) { ?>
                        <option value=""><?php echo get_phrase('select'); ?></option>
                        <option value="1"><?php echo get_phrase('admin_staff'); ?></option>
                        <option value="2"><?php echo get_phrase('teacher'); ?></option>
                        <option value="3"><?php echo get_phrase('staff'); ?></option>
                    <?php } ?>
                    <?php
                    if ($employee_type != NULL) { ?>
                        <option value="<?php echo $employee_type; ?>" selected>
                            <?php
                            if ($employee_type == 1) {
                                echo get_phrase('admin_staff');
                            } elseif ($employee_type == 2) {
                                echo get_phrase('teacher');
                            } elseif ($employee_type == 3) {
                                echo get_phrase('staff');
                            } else {
                                echo 'something is wrong';
                            }
                            ?></option>
                    <?php } ?>
                </select>
            </td>
            <td align="center"><input type="submit" value="<?php echo get_phrase('manage_attendance'); ?>"
                                      class="btn btn-info"/></td>
        </tr>
    </form>
    </tbody>
</table>

<?php if ($date != '' && $month != '' && $year != '' && $employee_type != ''): ?>

    <center>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">

                <div class="tile-stats tile-white-gray">
                    <div class="icon"><i class="entypo-suitcase"></i></div>
                    <?php
                    $full_date = $year . '-' . $month . '-' . $date;
                    $timestamp = strtotime($full_date);
                    $day = strtolower(date('l', $timestamp));
                    ?>
                    <h2><?php echo ucwords($day); ?></h2>

                    <h3>Attendance
                        of <?php if ($employee_type == 1) echo '<b>Admin Staff</b>'; elseif ($employee_type == 2) echo '<b>Teacher</b>';
                        else echo '<b>Staff</b>'; ?>
                        <p><?php echo $date . '-' . $month . '-' . $year; ?></p>
                </div>
            </div>
        </div>
    </center>
    <div class="row">
        <div class="col-sm-offset-3 col-md-6">
            <table class="table table-bordered">
                <thead>
                <tr class="gradeA">
                    <th><?php echo get_phrase('employee_type'); ?></th>
                    <th><?php echo get_phrase('employee_name'); ?></th>
                    <th><?php echo get_phrase('status'); ?></th>
                </tr>
                </thead>
                <tbody>

                <?php
                //Employee ATTENDANCE
                if ($employee_type == 1) {
                    $emp = $this->db->get_where('admin_staff')->result_array();
                } elseif ($employee_type == 2) {
                    $emp = $this->db->get_where('teacher')->result_array();
                } else {
                    $emp = $this->db->get_where('staff')->result_array();
                }

                foreach ($emp as $row) {
                ?>
                <form method="post"
                      action="<?php echo base_url(); ?>index.php?admin_staff/employee_attendance/<?php echo $date . '/' . $month . '/' . $year . '/' . $employee_type; ?>">
                    <tr class="gradeA">
                        <td><?php if ($employee_type == 1) echo '<b>Admin Staff</b>'; elseif ($employee_type == 2) echo '<b>Teacher</b>';
                            else echo '<b>Staff</b>'; ?></td>
                        <td><?php echo $row['name']; ?></td>

                        <td align="center">
                            <select name="status[]" class="form-control" style="width:100px; float:left;">
                                <option value="1" <?php if ($status == 1) echo 'selected="selected"'; ?>>Present
                                </option>
                                <option value="2" <?php if ($status == 2) echo 'selected="selected"'; ?>>Absent
                                </option>
                            </select>
                            <?php if ($employee_type == 1) { ?>
                                <input type="hidden" name="employee_id[]"
                                       value="<?php echo $row['admin_staff_id']; ?>"/>
                                <input type="hidden" name="employee_type[]"
                                       value="<?php echo 1; ?>"/>
                                <input type="hidden" name="employee_name[]"
                                       value="<?php echo $row['name']; ?>"/>
                            <?php } elseif ($employee_type == 2) { ?>
                                <input type="hidden" name="employee_id[]" value="<?php echo $row['teacher_id']; ?>"/>
                                <input type="hidden" name="employee_type[]" value="<?php echo 2; ?>"/>
                                <input type="hidden" name="employee_name[]" value="<?php echo $row['name']; ?>"/>
                            <?php } else { ?>
                                <input type="hidden" name="employee_id[]" value="<?php echo $row['staff_id']; ?>"/>
                                <input type="hidden" name="employee_type[]" value="<?php echo 3; ?>"/>
                                <input type="hidden" name="employee_name[]" value="<?php echo $row['name']; ?>"/>
                            <?php } ?>
                            <input type="hidden" name="date[]" value="<?php echo $full_date; ?>"/>

                        </td>
                    </tr>

                    <?php
                    }
                    ?>
                </tbody>

            </table>
            <div class="pull-right">
                <input type="submit" class="btn btn-default" value="Save Attendance"
                       style="float:left; margin:0px 10px;">
                </form></div>

        </div>
    </div>
<?php endif; ?>
