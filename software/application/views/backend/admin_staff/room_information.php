<div class="row">
    <div class="col-md-12">

        <?php
             $this->session->flashdata('message');
        ?>

        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('room_list');?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_room');?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------->

        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box <?php if(!isset($edit_data))echo 'active';?>" id="list">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th><div><?php echo get_phrase('room_number');?></div></th>
                        <th><div><?php echo get_phrase('floor_number');?></div></th>
                        <th><div><?php echo get_phrase('number_of_bed');?></div></th>
                        <th><div><?php echo get_phrase('room_fee');?></div></th>
                        <th><div><?php echo get_phrase('description');?></div></th>
                        <th><div><?php echo get_phrase('options');?></div></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1;foreach($hall_rooms as $row):?>
                        <tr>
                            <td><?php echo $row['room_id'];?></td>
                            <td><?php echo $row['floor_id'];?></td>
                            <td><?php echo $row['number_of_bed'];?></td>
                            <td><?php echo $row['room_fee'].' BDT';?></td>
                            <td><?php echo $row['description'];?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_room/<?php echo $row['room_info'];?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin_staff/room_information/<?php echo $hall_id?>/delete/<?php echo $row['room_info'];?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>

                <div class="col-md-12">
                    <B style="color:#3718ff;">Note: This hall can have maximum: <?php echo $total_room;?> beds.</B><br/>
                    <B style="color:red;">Total information of bed added here: <?php if($total_room_created != '') {echo $total_room_created;} else{ echo 0;}?></B>
                </div>
            </div>
            <!----TABLE LISTING ENDS--->

            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url().'index.php?admin_staff/room_information/'.$hall_id.'/create' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('enter_room_number');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="room_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('floor_number');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="floor_id"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('number_of_bed');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="number_of_bed"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('room_rent_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="room_fee"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="description"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('add_room');?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>