<div class="row">

    <?php if ($date1 == '' && $date2 == ''): ?>

        <div class="col-md-12">
            <h1>Attendance report by date</h1>
            <hr/>
            <div class="box-content">
                <?php echo form_open('admin_staff/emp_attendance_report/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Choose Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date1" value=""
                               data-start-view="1">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('get_report'); ?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($employee_type == '' && $employee_id == '' && $date1 == '' && $date2 == ''): ?>

        <div class="col-md-12">
            <h1>Attendance report by employee</h1>
            <hr/>
            <div class="box-content">
                <?php echo form_open(base_url().'index.php?admin_staff/emp_attendance_report/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                <div class="form-group">
                    <label for="field-2"
                           class="col-sm-3 control-label"><?php echo get_phrase('employee_type'); ?></label>

                    <div class="col-sm-5">
                        <select name="employee_type" class="form-control">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="1"><?php echo get_phrase('admin_staff'); ?></option>
                            <option value="2"><?php echo get_phrase('teacher'); ?></option>
                            <option value="3"><?php echo get_phrase('staff'); ?></option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                    <div class="col-sm-5">
                        <select name="employee_id" class="form-control">

                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">First Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date1" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Last Date</label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date2" value=""
                               data-start-view="1">
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('get_report'); ?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($date1 != '' && $employee_type == ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h1 class="text-center"><?php echo 'Attendance Report: ' . date('d-F-Y', $date1); ?></h1>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th class="text-center">
                        <div><?php echo get_phrase('employee_type'); ?></div>
                    </th class="text-center">
                    <th class="text-center">
                        <div><?php echo get_phrase('employee_name'); ?></div>
                    </th class="text-center">

                    <th class="text-center">Date</th>
                    <th class="text-center">Status</th>

                </tr>
                </thead>
                <tbody>


                </tbody><?php foreach ($admin_attendance as $row) { ?>

                    <tr>
                        <td class="text-center"><?php if ($row->employee_type = 1) echo '<p style="color:#ff1c08"><b>Admin Staff</b></p>'; ?></td>
                        <td class="text-center"><?php echo '<b>' . $row->employee_name . '</b>' ?></td>
                        <td class="text-center"><?php $date = date('d-M', $row->date);
                            echo $date; ?></td>
                        <td class="text-center">
                            <?php if ($row->status == 1) {
                                echo '<div class="label label-success">Present</div>';
                            } else echo '<div class="label label-danger">Absent</div>'; ?>
                        </td>

                    </tr>
                <?php } ?>

                <?php foreach ($teacher_attendance as $row) { ?>

                    <tr>
                        <td class="text-center"><?php if ($row->employee_type = 2) echo '<p style="color:#00a6fc"><b>Teacher</b></p>'; ?></td>
                        <td class="text-center"><?php echo '<b>' . $row->employee_name . '</b>' ?></td>
                        <td class="text-center"><?php $date = date('d-M', $row->date);
                            echo $date; ?></td>
                        <td class="text-center">
                            <?php if ($row->status == 1) {
                                echo '<div class="label label-success">Present</div>';
                            } else echo '<div class="label label-danger">Absent</div>'; ?>
                        </td>

                    </tr>
                <?php } ?>

                <?php foreach ($staff_attendance as $row) { ?>

                    <tr>
                        <td class="text-center"><?php if ($row->employee_type = 1) echo '<p style="color:#00a65a"><b>Staff</b></p>'; ?></td>
                        <td class="text-center"><?php echo '<b>' . $row->employee_name . '</b>' ?></td>
                        <td class="text-center"><?php $date = date('d-M', $row->date);
                            echo $date; ?></td>
                        <td class="text-center">
                            <?php if ($row->status == 1) {
                                echo '<div class="label label-success">Present</div>';
                            } else echo '<div class="label label-danger">Absent</div>'; ?>
                        </td>

                    </tr>
                <?php } ?>
            </table>


            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Attendance Report
                <i class="entypo-doc-text"></i>
            </a>
            <br/><br/>
            <div style="opacity: .3;">
                <img src="<?php echo base_url()?>bg-logo.jpg" id="mainImg" height="75px" width="75" class="pull-right" />
            </div>
        </div>

    <?php endif; ?>

    <?php if ($employee_name != '' && $date1 != '' && $date2 != ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <h3 class="text-center"><?php echo 'Attendance Report of ' . $employee_name . ': Between ' . date('d.M.Y', $date1) . ' - ' . date('d.M.Y', $date2); ?></h3>
                    <h3 class="text-center">Employee Type: <b><?php if ($employee_type == 1) {
                            echo 'Admin';
                        } elseif ($employee_type == 2) {
                            echo 'Teacher';
                        } else {
                            echo 'Staff';
                        }; ?></b></h3>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
                        <table class="table table-bordered datatable">
                            <thead>
                            <tr>
                                <th class="text-center">Date</th>
                                <th class="text-center">Status</th>

                            </tr>
                            </thead>
                            <tbody>


                            </tbody><?php foreach ($attendance_info_by_employee as $row) {?>

                                <tr>
                                    <td class="text-center">
            <?php $date = date('d-M-Y', $row->date); echo $date;?></td>
                                    <td class="text-center">
                                        <?php if($row->status == 1) {echo '<div class="label label-success">Present</div>';} else echo '<div class="label label-danger">Absent</div>'; ?>
                                    </td>

                                </tr>
                            <?php } ?>

                        </table>


            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Attendance Report
                <i class="entypo-doc-text"></i>
            </a>
<br/><br/>
            <div style="opacity: .3;">
                <img src="<?php echo base_url()?>bg-logo.jpg" id="mainImg" height="75px" width="75" class="pull-right" />
            </div>
        </div>

    <?php endif; ?>


</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="employee_type"]').on('change', function () {
            var employee_type = $(this).val();
            console.log(employee_type);
            if (employee_type == 1) {
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/' + employee_type,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="employee_id"]').append('<option value="' + value.admin_staff_id + '">' + value.name + '</option>');
                        });
                    }
                });
            }
            else if (employee_type == 2) {
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/' + employee_type,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="employee_id"]').append('<option value="' + value.teacher_id + '">' + value.name + '</option>');
                        });
                    }
                });
            }
            else if (employee_type == 3) {
                $.ajax({
                    url: '<?php echo base_url()?>index.php?admin_staff/getid/' + employee_type,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="employee_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="employee_id"]').append('<option value="' + value.staff_id + '">' + value.name + '</option>');
                        });
                    }
                });
            }
            else {
                $('select[name="employee_id"]').empty();
            }
        });
    });
</script>

<script type="text/javascript">
    function printImg() {
        pwin = window.open(document.getElementById("mainImg").src,"_blank");
        pwin.onload = function () {window.print();}
    }
</script>