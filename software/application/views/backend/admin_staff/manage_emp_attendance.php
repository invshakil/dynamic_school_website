<?php
$this->session->flashdata('message');
?>

<br><br>
<table class="table table-bordered datatable" id="table_export">
    <thead>
    <tr>
        <th width="80">
            <div><?php echo get_phrase('sl_no'); ?></div>
        </th>
        <th>
            <div><?php echo get_phrase('name'); ?></div>
        </th>
        <th>
            <div><?php echo get_phrase('entry_time'); ?></div>
        </th>
<!--        <th>-->
<!--            <div>Date</div>-->
<!--        </th>-->
        <th>
            <div><?php echo get_phrase('present_status'); ?></div>
        </th>
        <th>
            <div><?php echo get_phrase('options'); ?></div>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php
    $device_attendance = $this->db->order_by('device_attendance_id','desc')->get('device_attendance')->result_array();

    $i = 1;
    foreach ($device_attendance as $row):?>
        <tr>
            <td>
                <?php echo $i++; ?>
            </td>
            <td><?php
                $staff_code = $row['staff_id'];
                $emp_name = $this->db->get_where('teacher', array('staff_code' => $staff_code))->row('name');

                if (!$emp_name)
                {
                    $emp_name = $this->db->get_where('staff', array('staff_code' => $staff_code))->row('name');
                }
                echo $emp_name;
                ?></td>
            <td>
                <?php
                if ($row['datetime'])
                {
                    echo date('D, d.m.Y h:i A', $row['datetime']+6*60*60);
                }
                else
                {
                    echo 'Not available!';
                }
                ?>
            </td>
<!--            <td>--><?php //echo$row['date']; ?><!--</td>-->
            <td>
                <?php if ($row['status'] == 0) {
                    echo '<div class="label label-success">Present</div>';
                } else { echo '<div class="label label-danger">Absent</div>';} ?>
            </td>
            <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!-- staff EDITING LINK -->
                        <li>
                            <a href="#"
                               onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_dev_attendance_edit/<?php echo $row['device_attendance_id']; ?>');">
                                <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- staff DELETION LINK -->
                        <li>
                            <a href="#"
                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin_staff/manage_emp_attendance/delete/<?php echo $row['device_attendance_id']; ?>');">
                                <i class="entypo-trash"></i>
                                <?php echo get_phrase('delete'); ?>
                            </a>
                        </li>
                    </ul>
                </div>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ----->
<script type="text/javascript">

    jQuery(document).ready(function ($) {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [

                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(3, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },

                    },
                ]
            },

        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>
