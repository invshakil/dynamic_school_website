<div class="row">
    <?php $exam_info = $this->db->get('exam')->result_array(); ?>
    <?php if ($exam_id == ''): ?>

    <div class="col-md-12">
        <h1>Admit Card Issue by Term/Semester</h1>
        <hr/>
        <div class="box-content">
            <?php echo form_open(base_url().'index.php?admin_staff/student_admit_issue/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

            <div class="form-group">
                <label for="field-2" class="col-sm-3 control-label">Choose Exam</label>

                <div class="col-sm-5">
                    <select name="exam_id" class="form-control" style="width:100%;">
                        <?php foreach ($exam_info as $row) { ?>
                            <option value="<?php echo $row['exam_id']; ?>"><?php echo $row['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-info"><?php echo get_phrase('get_report'); ?></button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <?php endif; ?>

    <?php if ($exam_id != ''): ?>

    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box active" id="list">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th width="15%">
                            <div><?php echo get_phrase('student_name'); ?></div>
                        </th>
                        <th width="15%">
                            <div><?php echo get_phrase('student_roll'); ?></div>
                        </th>
                        <th width="10%">
                            <div><?php echo get_phrase('exam_fee'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('date'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($admit_card_info as $row): ?>
                        <tr>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->roll; ?></td>
                            <td><?php echo $row->amount . ' TK'; ?></td>
                            <td><?php echo date('d M,Y', $row->date); ?></td>
                            <td>
                                <?php
                                $status = $row->status;
                                if ($status == 1) {
                                    echo '<span class="label label-success">Ready</span>';
                                } else {
                                    echo '<span class="label label-danger">Unpaid</span>';
                                } ?>
                            </td>
                            <td>
                                <div class="btn-group">

                                        <a class="btn btn-default" target="_blank"
                                           href="<?php echo base_url(); ?>index.php?admin_staff/student_admit_card/<?php echo $row->student_id; ?>/<?php echo $exam_id;?>">
                                            Print Admit Card
                                        </a>

                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->

        </div>
    </div>

    <?php endif; ?>

</div>
