<?php

if ($date_attendance != '') { ?>

    <div class="row">
        <div class="col-md-12">

            <div class="tile-stats tile-white-gray">
                <h2 class="text-center"><?php echo 'Attendance Report: ' . date('l, d F Y', strtotime($date1)); ?></h2>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <br><br>
        <table class="table table-bordered datatable">
            <thead>
            <tr>

                <th class="text-center">
                    <div>Employee Name</div>
                </th>
                <th class="text-center">
                    <div>Employee Type</div>
                </th>

                <th class="text-center">Date</th>
                <th class="text-center">Entry Time</th>
                <th class="text-center">Leave Time</th>
                <th class="text-center">Status</th>

            </tr>
            </thead>
            <tbody>


            </tbody>

            <?php foreach ($date_attendance as $row) { ?>

                <tr>

                    <td class="text-center">
                        <?php
                        echo $row['employee_name'];

                        ?>
                    </td>
                    <td class="text-center">
                        <?php

                        if ($row['designation'] == 'Teacher')
                        {
                            echo '<div class="alert alert-success">Teacher</div>';
                        }
                        else
                        {
                            echo '<div class="alert alert-info">Staff</div>';
                        };

                        ?>
                    </td>
                    <td class="text-center">
                        <?php echo $row['date']; ?>
                    </td>
                    <td class="text-center">
                        <?php

                        echo $row['entry_time'];

                        ?>
                    </td>
                    <td class="text-center">
                        <?php
                        echo $row['leave_time'];

                        ?>
                    </td>
                    <td class="text-center">
                        <?php if ($row['status'] == 1) {
                            echo '<div class="label label-success">Present</div>';
                        } else echo '<div class="label label-danger">Absent</div>'; ?>
                    </td>

                </tr>
            <?php } ?>


        </table>


        <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
            Print Attendance Report
            <i class="entypo-doc-text"></i>
        </a>
    </div>


<?php } elseif ($attendance_info_by_employee != '') { ?>

    <div class="row">
        <div class="col-md-12">

            <div class="tile-stats tile-white-gray">
                <h3 class="text-center"><?php echo 'Attendance Report of ' . $employee_name; ?></h3>
                <h4 class="text-center"><?php echo  'Employee Type: ' . $designation; ?></h4>
                <h4 class="text-center"><?php echo  'Date Range: ' . $date1 . ' - ' . $date2; ?></h4>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <br><br>
        <table class="table table-bordered datatable">
            <thead>
            <tr>
                <th class="text-center">Date</th>
                <th class="text-center">Entry Time</th>
                <th class="text-center">Leave Time</th>
                <th class="text-center">Status</th>

            </tr>
            </thead>
            <tbody>


            </tbody><?php foreach ($attendance_info_by_employee as $row) { ?>

                <tr>
                    <td class="text-center">
                        <?php echo $row['date']; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $row['entry_time']; ?>
                    </td>
                    <td class="text-center">
                        <?php
                        if ($row['leave_time'] == '06:00 AM')
                        {
                            echo 'No Log Found!';
                        }
                        else{
                            echo $row['leave_time'];
                        }

                        ?>
                    </td>
                    <td class="text-center">
                        <?php if ($row['status'] == 1) {
                            echo '<div class="label label-success">Present</div>';
                        } else echo '<div class="label label-danger">Absent</div>'; ?>
                    </td>

                </tr>
            <?php } ?>

        </table>


        <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
            Print Attendance Report
            <i class="entypo-doc-text"></i>
        </a>
    </div>

<?php } else {
    echo 'No Attendance Log Found!';
}; ?>

