<div class="sidebar-menu">
    <header class="logo-env">
        <?php
        $this->session->flashdata('message');
        ?>
        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="uploads/logo.png" style="max-height:60px;"/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->


        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/dashboard">
                <i class="entypo-gauge"></i>
                <span><?php echo get_phrase('admin_dashboard'); ?></span>
            </a>
        </li>


        <!-- HR/PAYROLL -->

        <li class="<?php if ($page_name == 'teacher' ||
            $page_name == 'staff' ||
            $page_name == 'salary' ||
            $page_name == 'salary_report' ||
            $page_name == 'employee_attendance' ||
            $page_name == 'leave_management'     ||
            $page_name == 'manage_emp_attendance' ||
            $page_name == 'device_attendance_report'
        )
            echo 'opened active has-sub'; ?> ">
            <a href="#">
                <i class="fa fa-eye-slash"></i>
                <span><?php echo get_phrase('hr_&_payroll'); ?></span>
            </a>
            <ul>
                <!--List -->
                <!-- TEACHER -->
                <li class="<?php if ($page_name == 'teacher') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/teacher">
                        <i class="entypo-users"></i>
                        <span><?php echo get_phrase('teacher'); ?></span>
                    </a>
                </li>

                <!--List -->
                <!-- STAFF -->
                <li class="<?php if ($page_name == 'staff') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/staff">
                        <i class="entypo-user-add"></i>
                        <span><?php echo get_phrase('staff'); ?></span>
                    </a>
                </li>

                <!--List -->
                <!-- SALARY -->
                <li class="<?php if ($page_name == 'salary' ||
                    $page_name == 'salary_report'
                )
                    echo 'opened active has-sub'; ?> ">
                    <a href="#">
                        <i class="fa fa-shopping-cart"></i>
                        <span><?php echo get_phrase('salary_management'); ?></span>
                    </a>

                    <ul>
                        <li class="<?php if ($page_name == 'salary') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>index.php?admin_staff/salary">
                                <i class="entypo-bag"></i>
                                <span><?php echo get_phrase('salary'); ?></span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'salary_report') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>index.php?admin_staff/salary_report">
                                <i class="entypo-credit-card"></i>
                                <span><?php echo get_phrase('salary_report'); ?></span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!--Attendance of employee -->
                <!-- Attendance -->
                <li class="<?php if ($page_name == 'manage_emp_attendance') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/manage_emp_attendance">
                        <i class="entypo-box"></i>
                        <span><?php echo get_phrase('manage_employee_attendance'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'device_attendance_report') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/device_attendance_report">
                        <i class="entypo-check"></i>
                        <span><?php echo get_phrase('attendance_report'); ?></span>
                    </a>
                </li>

<!--                <li class="--><?php //if ($page_name == 'employee_attendance' ||
//                    $page_name == 'emp_attendance_report'
//                )
//                    echo 'opened active has-sub'; ?><!-- ">-->
<!--                    <a href="#">-->
<!--                        <i class="fa fa-check-circle"></i>-->
<!--                        <span>--><?php //echo get_phrase('attendance_management'); ?><!--</span>-->
<!--                    </a>-->
<!---->
<!--                    <ul>-->
<!---->
<!--                        <li class="--><?php //if ($page_name == 'employee_attendance') echo 'active'; ?><!-- ">-->
<!--                            <a href="--><?php //echo base_url(); ?><!--index.php?admin_staff/employee_attendance/--><?php //echo date("d/m/Y"); ?><!--">-->
<!--                                <i class="entypo-archive"></i>-->
<!--                                <span>--><?php //echo get_phrase('employee_attendance'); ?><!--</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!---->
<!--                        <li class="--><?php //if ($page_name == 'emp_attendance_report') echo 'active'; ?><!-- ">-->
<!--                            <a href="--><?php //echo base_url(); ?><!--index.php?admin_staff/emp_attendance_report">-->
<!--                                <i class="entypo-archive"></i>-->
<!--                                <span>--><?php //echo get_phrase('employee_attendance_report'); ?><!--</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </li>-->


                <!--Leave application of employee -->
                <!-- Leave -->
                <li class="<?php if ($page_name == 'leave_management') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/leave_management">
                        <i class="entypo-alert"></i>
                        <span><?php echo get_phrase('leave_management'); ?></span>
                    </a>
                </li>

            </ul>
        </li>

        <!-- Finance -->

        <li class="<?php if ($page_name == 'invoice' ||
            $page_name == 'income' ||
            $page_name == 'income_category' ||
            $page_name == 'expense' ||
            $page_name == 'expense_category' ||
            $page_name == 'fee_category' ||
            $page_name == 'student_admit_issue' ||
            $page_name == 'bank_details' ||
            $page_name == 'issue_student_fee' ||
            $page_name == 'deposit_status'
        )
            echo 'opened active has-sub'; ?> ">
            <a href="#">
                <i class="fa fa-barcode"></i>
                <span><?php echo get_phrase('finance_&_accounting'); ?></span>
            </a>
            <ul>
                <!--List -->
                <li class="<?php if ($page_name == 'invoice') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/invoice">
                        <i class="fa fa-credit-card"></i>
                        <span><?php echo get_phrase('student_invoice'); ?></span>
                    </a>
                </li>
                <!-- Invoice -->
                <li class="<?php if (
                    $page_name == 'income_category' ||
                    $page_name == 'income' ||
                    $page_name == 'expense_category' ||
                    $page_name == 'expense' ||
                    $page_name == 'bank_details' ||
                    $page_name == 'deposit_status'

                )
                    echo 'opened active has-sub'; ?> ">
                    <a href="#">
                        <i class="fa fa-check-circle"></i>
                        <span><?php echo get_phrase('accounting_management'); ?></span>
                    </a>

                    <ul>


                        <li class="<?php if ($page_name == 'income_category' ||
                            $page_name == 'income'
                        )
                            echo 'opened active has-sub'; ?> ">
                            <a href="#">
                                <i class="fa fa-arrow-circle-o-up"></i>
                                <span><?php echo get_phrase('cash_in'); ?></span>
                            </a>
                            <ul>
                                <li class="<?php if ($page_name == 'income_category') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>index.php?admin_staff/income_category">
                                        <i class="fa fa-ticket"></i>
                                        <span><?php echo get_phrase('cash_in_category'); ?></span>
                                    </a>
                                </li>
                                <li class="<?php if ($page_name == 'income') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>index.php?admin_staff/income">
                                        <i class="fa fa-bookmark"></i>
                                        <span><?php echo get_phrase('cash_in_management'); ?></span>
                                    </a>
                                </li>
                            </ul>

                        </li>

                        <li class="<?php if ($page_name == 'expense_category' ||
                            $page_name == 'expense'
                        )
                            echo 'opened active has-sub'; ?> ">
                            <a href="#">
                                <i class="fa fa-arrow-circle-o-down"></i>
                                <span><?php echo get_phrase('cash_out'); ?></span>
                            </a>
                            <ul>
                                <li class="<?php if ($page_name == 'expense_category') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>index.php?admin_staff/expense_category">
                                        <i class="fa fa-ticket"></i>
                                        <span><?php echo get_phrase('cash_out_category'); ?></span>
                                    </a>
                                </li>
                                <li class="<?php if ($page_name == 'expense') echo 'active'; ?> ">
                                    <a href="<?php echo base_url(); ?>index.php?admin_staff/expense">
                                        <i class="fa fa-bookmark"></i>
                                        <span><?php echo get_phrase('cash_out_report'); ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

<!--                        <li class="--><?php //if ($page_name == 'bank_details' ||
//                            $page_name == 'deposit_status'
//                        )
//                            echo 'opened active has-sub'; ?><!-- ">-->
<!--                            <a href="#">-->
<!--                                <i class="fa fa-arrow-circle-o-right"></i>-->
<!--                                <span>--><?php //echo get_phrase('bank_deposit'); ?><!--</span>-->
<!--                            </a>-->
<!--                            <ul>-->
<!--                                <li class="--><?php //if ($page_name == 'bank_details') echo 'active'; ?><!-- ">-->
<!--                                    <a href="--><?php //echo base_url(); ?><!--index.php?admin_staff/bank_details">-->
<!--                                        <i class="fa fa-ticket"></i>-->
<!--                                        <span>--><?php //echo get_phrase('bank_details'); ?><!--</span>-->
<!--                                    </a>-->
<!--                                </li>-->
<!--                                <li class="--><?php //if ($page_name == 'deposit_status') echo 'active'; ?><!-- ">-->
<!--                                    <a href="--><?php //echo base_url(); ?><!--index.php?admin_staff/deposit_status">-->
<!--                                        <i class="fa fa-bookmark"></i>-->
<!--                                        <span>--><?php //echo get_phrase('deposit_status'); ?><!--</span>-->
<!--                                    </a>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </li>-->
                    </ul>
                </li>

                <li class="<?php if ($page_name == 'fee_category' ||
                    $page_name == 'issue_student_fee'
                )
                    echo 'opened active has-sub'; ?> ">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span><?php echo get_phrase('student_fees'); ?></span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'fee_category') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>index.php?admin_staff/fee_category">
                                <i class="fa fa-ticket"></i>
                                <span><?php echo get_phrase('fee_category'); ?></span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'issue_student_fee') echo 'active'; ?> ">
                            <a href="<?php echo base_url(); ?>index.php?admin_staff/issue_student_fee">
                                <i class="fa fa-bookmark"></i>
                                <span><?php echo get_phrase('issue_student_fee'); ?></span>
                            </a>
                        </li>
                    </ul>
                </li>
<!--                <li class="--><?php //if ($page_name == 'student_admin_issue' ||
//                    $page_name == 'student_admit_issue'
//                )
//                    echo 'opened active has-sub'; ?><!-- ">-->
<!--                    <a href="#">-->
<!--                        <i class="fa fa-file-text"></i>-->
<!--                        <span>--><?php //echo get_phrase('issue_admit_card'); ?><!--</span>-->
<!--                    </a>-->
<!--                    <ul>-->
<!--                        --><?php //$classes = $this->db->get('class')->result_array();
//                        foreach ($classes as $row):?>
<!--                            <li class="--><?php //if ($page_name == 'student_admit_issue' && $class_id == $row['class_id']) echo 'active'; ?><!--">-->
<!--                                <a href="--><?php //echo base_url(); ?><!--index.php?admin_staff/student_admit_issue/--><?php //echo $row['class_id']; ?><!--">-->
<!--                                    <span>--><?php //echo get_phrase('class'); ?><!-- <i-->
<!--                                            class="fa fa-arrow-circle-o-right"></i>--><?php //echo $row['name']; ?><!--</span>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        --><?php //endforeach; ?>
<!--                    </ul>-->
<!--                </li>-->

            </ul>
        </li>


        <!-- STUDENT -->
        <li class="<?php if ($page_name == 'modal/student_add' ||
            $page_name == 'student_information' ||
            $page_name == 'student_marksheet'
        )
            echo 'opened active has-sub'; ?> ">
            <a href="#">
                <i class="fa fa-group"></i>
                <span><?php echo get_phrase('student'); ?></span>
            </a>
            <ul>
                <!-- STUDENT ADMISSION -->
                <li class="<?php if ($page_name == 'modal/student_add') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/student_add">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('admit_student'); ?></span>
                    </a>
                </li>

                <!-- STUDENT INFORMATION -->
                <li class="<?php if ($page_name == 'student_information') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('student_information'); ?></span>
                    </a>
                    <ul>
                        <?php $classes = $this->db->get('class')->result_array();
                        foreach ($classes as $row):?>
                            <li class="<?php if ($page_name == 'student_information' && $class_id == $row['class_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin_staff/student_information/<?php echo $row['class_id']; ?>">
                                    <span><?php echo get_phrase('class'); ?> <i
                                            class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>

                <!-- STUDENT MARKSHEET -->
                <li class="<?php if ($page_name == 'student_marksheet') echo 'opened active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/student_marksheet/<?php echo $row['class_id']; ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('student_marksheet'); ?></span>
                    </a>
                    <ul>
                        <?php $classes = $this->db->get('class')->result_array();
                        foreach ($classes as $row):?>
                            <li class="<?php if ($page_name == 'student_marksheet' && $class_id == $row['class_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin_staff/student_marksheet/<?php echo $row['class_id']; ?>">
                                    <span><?php echo get_phrase('class'); ?> <i
                                            class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </li>


        <!-- PARENT -->
        <li class="<?php if ($page_name == 'parent') echo 'opened active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/parent">
                <i class="entypo-user"></i>
                <span><?php echo get_phrase('parent'); ?></span>
            </a>
            <ul>
                <?php $classes = $this->db->get('class')->result_array();
                foreach ($classes as $row):?>
                    <li class="<?php if ($page_name == 'parent' && $class_id == $row['class_id']) echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php?admin_staff/parent/<?php echo $row['class_id']; ?>">
                            <span><?php echo get_phrase('class'); ?> <i
                                    class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- CLASS -->
        <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/classes">
                <i class="entypo-flow-tree"></i>
                <span><?php echo get_phrase('class'); ?></span>
            </a>

        </li>

        <!-- SECTION -->
        <li class="<?php if ($page_name == 'section') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/sections">
                <i class="entypo-flow-tree"></i>
                <span><?php echo get_phrase('section'); ?></span>
            </a>
        </li>

        <!-- SUBJECT -->
        <li class="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-docs"></i>
                <span><?php echo get_phrase('subject'); ?></span>
            </a>
            <ul>
                <?php $classes = $this->db->get('class')->result_array();
                foreach ($classes as $row):?>
                    <li class="<?php if ($page_name == 'subject' && $class_id == $row['class_id']) echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php?admin_staff/subject/<?php echo $row['class_id']; ?>">
                            <span><?php echo get_phrase('class'); ?> <i
                                    class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- CLASS ROUTINE -->
        <li class="<?php if ($page_name == 'class_routine') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/class_routine">
                <i class="entypo-target"></i>
                <span><?php echo get_phrase('class_routine'); ?></span>
            </a>
        </li>

        <!-- DAILY ATTENDANCE -->
        <li class="<?php if ($page_name == 'manage_attendance') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/manage_attendance/<?php echo date("d/m/Y"); ?>">
                <i class="entypo-chart-area"></i>
                <span><?php echo get_phrase('daily_attendance'); ?></span>
            </a>

        </li>

        <!-- ATTENDANCE REPORT -->
        <li class="<?php if ($page_name == 'student_attendance_report') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/student_attendance_report">
                <i class="entypo-chart-area"></i>
                <span><?php echo get_phrase('student_attendance_report'); ?></span>
            </a>

        </li>

        <!-- EXAMS -->
        <li class="<?php if ($page_name == 'exam' ||
            $page_name == 'grade' ||
            $page_name == 'marks'
        ) echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-graduation-cap"></i>
                <span><?php echo get_phrase('exam'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'exam') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/exam">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'grade') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/grade">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_grades'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'marks') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/marks">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_marks'); ?></span>
                    </a>
                </li>
            </ul>
        </li>


        <!-- LIBRARY -->
        <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/book">
                <i class="entypo-book"></i>
                <span><?php echo get_phrase('library'); ?></span>
            </a>
        </li>

        <!-- TRANSPORT -->
        <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/transport">
                <i class="entypo-location"></i>
                <span><?php echo get_phrase('transport'); ?></span>
            </a>
        </li>

        <!-- DORMITORY -->

        <li class="<?php if ($page_name == 'dormitory' ||
            $page_name == 'room_information' ||
            $page_name == 'room_booking'

        )
            echo 'opened active has-sub'; ?> ">
            <a href="#">
                <i class="entypo-home"></i>
                <span><?php echo get_phrase('Dormitory'); ?></span>
            </a>
            <ul>
                <!--List -->
                <!-- Creation and manage of halls -->
                <li class="<?php if ($page_name == 'dormitory') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/dormitory">
                        <i class="fa fa-credit-card"></i>
                        <span><?php echo get_phrase('hall_information'); ?></span>
                    </a>
                </li>

                <!--List -->
                <!-- Hall List with room creation-->
                <li class="<?php if ($page_name == 'room_information')
                    echo 'opened active has-sub'; ?> ">
                    <a href="#">
                        <i class="fa fa-info-circle"></i>
                        <span><?php echo get_phrase('room_information'); ?></span>
                    </a>

                    <ul>
                        <?php $hall_info = $this->db->get('dormitory')->result_array();
                        foreach ($hall_info as $row):?>
                            <li class="<?php if ($page_name == 'room_information' && $hall_id == $row['hall_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin_staff/room_information/<?php echo $row['hall_id']; ?>">
                                    <span><?php echo get_phrase('hall_room_of_'); ?> <i
                                            class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>

                <!-- Room Booking hall wise-->
                <li class="<?php if ($page_name == 'room_booking')
                    echo 'opened active has-sub'; ?> ">
                    <a href="#">
                        <i class="fa fa-pencil-square-o"></i>
                        <span><?php echo get_phrase('room_booking'); ?></span>
                    </a>

                    <ul>
                        <?php $hall_info = $this->db->get('dormitory')->result_array();
                        foreach ($hall_info as $row):?>
                            <li class="<?php if ($page_name == 'room_booking' && $hall_id == $row['hall_id']) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php?admin_staff/room_booking/<?php echo $row['hall_id']; ?>">
                                    <span><?php echo get_phrase('room_from_'); ?> <i
                                            class="fa fa-arrow-circle-o-right"></i><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </li>


        <!-- NOTICEBOARD -->
        <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/noticeboard">
                <i class="entypo-doc-text-inv"></i>
                <span><?php echo get_phrase('noticeboard'); ?></span>
            </a>
        </li>

        <!-- SETTINGS -->
        <li class="<?php if ($page_name == 'system_settings' ||
            $page_name == 'manage_language'
        ) echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-lifebuoy"></i>
                <span><?php echo get_phrase('settings'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/system_settings">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('general_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'manage_language') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?admin_staff/manage_language">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('language_settings'); ?></span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- ACCOUNT -->
        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?admin_staff/manage_profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>


    </ul>

</div>