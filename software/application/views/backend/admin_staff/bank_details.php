<div class="row">
    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('bank_information'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_bank_information'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------->
        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box active" id="list">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th>
                            <div><?php echo get_phrase('bank_name'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('branch_address'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($bank_details as $row): ?>
                        <tr>
                            <td><?php echo $row['bank_name']; ?></td>
                            <td><?php echo $row['branch_address']; ?></td>
                            <td>
                                <?php
                                $status = $row['status'];
                                if($status==1)
                                {
                                    echo '<span class="label label-success">In use</span>';
                                }
                                else
                                {
                                    echo '<span class="label label-danger">Not in Use</span>';
                                }?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <li class="divider"></li>

                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_bank/<?php echo $row['bank_id']; ?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin_staff/bank_details/delete/<?php echo $row['bank_id']; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url().'index.php?admin_staff/bank_details/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('bank_name'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="bank_name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('branch_address'); ?></label>

                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="branch_address" placeholder="Textarea" rows="6"></textarea>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                        <div class="col-sm-5">
                            <select name="status" class="form-control" style="width:100%;">
                                <option value="1"><?php echo get_phrase('In Use'); ?></option>
                                <option value="2"><?php echo get_phrase('Not in Use'); ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('add_bank_information'); ?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>

