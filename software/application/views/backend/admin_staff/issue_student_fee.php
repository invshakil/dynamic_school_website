<div class="row">
    <div class="col-md-12">
        <?php
        $this->session->flashdata('message');
        ?>
        <!------CONTROL TABS START------->
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    <?php echo get_phrase('manage_fee_/_fee_management'); ?>
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_fee_/_fee_management'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------->
        <div class="tab-content">
            <!----TABLE LISTING STARTS--->
            <div class="tab-pane box active" id="list">

                <table class="table table-bordered datatable" id="table_export">
                    <thead>
                    <tr>
                        <th width="13%">
                            <div><?php echo get_phrase('fee_type'); ?></div>
                        </th>
                        <th width="15%">
                            <div><?php echo get_phrase('fee_title'); ?></div>
                        </th>
                        <th width="15%">
                            <div><?php echo get_phrase('student_name'); ?></div>
                        </th>
                        <th width="15%">
                            <div><?php echo get_phrase('class_name'); ?></div>
                        </th>
                        <th width="10%">
                            <div><?php echo get_phrase('amount'); ?></div>
                        </th>
                        <th width="12%">
                            <div><?php echo get_phrase('date'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('status'); ?></div>
                        </th>
                        <th>
                            <div><?php echo get_phrase('options'); ?></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($fee_info as $row): ?>
                        <tr>
                            <td>
                                <?php echo $row->category_name; ?><br/>
                                <?php
                                if ($row->exam_id != '') {
                                    $exam_id = $row->exam_id;
                                    $info = $this->db->get_where('exam', array('exam_id' => $exam_id))->row();
                                    echo '<br>Term: '.$info->name;
                                }
                                ?>
                            </td>
                            <td><?php echo $row->fee_title; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td>Class-<?php echo $row->class_id; ?></td>
                            <td><?php echo $row->amount . ' BDT'; ?></td>
                            <td><?php echo date('d M,Y', $row->date); ?></td>
                            <td>
                                <?php
                                $status = $row->status;
                                if ($status == 1) {
                                    echo '<span class="label label-success">Paid</span>';
                                } else {
                                    echo '<span class="label label-danger">Unpaid</span>';
                                } ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                            data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <li class="divider"></li>

                                        <!-- EDITING LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_fee/<?php echo $row->fee_id; ?>');">
                                                <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit'); ?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>

                                        <!-- DELETION LINK -->
                                        <li>
                                            <a href="#"
                                               onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin_staff/issue_student_fee/delete/<?php echo $row->fee_id; ?>');">
                                                <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(base_url().'index.php?admin_staff/issue_student_fee/create', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('select_class'); ?></label>
                        <div class="col-sm-5">
                            <select name="class_id" class="form-control">
                                <option value="">Select a class</option>
                                <?php
                                $classes	=	$this->db->get('class')->result_array();
                                foreach($classes as $row):?>
                                    <option value="<?php echo $row['class_id'];?>"
                                        <?php if(isset($class_id) && $class_id==$row['class_id'])echo 'selected="selected"';?>>
                                        <?php echo $row['name'];?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('select_section'); ?></label>
                        <div class="col-sm-5">
                            <select name="section_id" class="form-control" data-live-search="true">
                                <option selected>select section</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('enter_student_name'); ?></label>
                        <div class="col-sm-5">
                            <select name="student_id" class="select2" data-allow-clear="true"
                                    data-placeholder="Select student...">
                                <option></option>
                                <optgroup label="Student List">
                                    <!--                                    --><?php
                                    //                                    $this->db->order_by('class_id','asc');
                                    //                                    $students = $this->db->get('student')->result_array();
                                    //                                    foreach($students as $row):
                                    //                                        ?>
                                    <!--                                        <option value="--><?php //echo $row['student_id'];?><!--">-->
                                    <!--                                            --><?php //echo $this->crud_model->get_class_name($row['class_id']);?><!-- --->
                                    <!--                                            roll --><?php //echo $row['roll'];?><!-- --->
                                    <!--                                            --><?php //echo $row['name'];?>
                                    <!--                                        </option>-->
                                    <!--                                        --><?php
                                    //                                    endforeach;
                                    //                                    ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('fee_category'); ?></label>

                        <div class="col-sm-5">
                            <select name="fee_category_id" id="fee_type" onchange="feeType(this)" class="form-control" style="width:150px; float:left;">
                                <option>--Select Category--</option>
                                <?php $categories = $this->db->get('fee_category')->result_array();
                                foreach ($categories as $row) {
                                    ?>
                                    <option
                                        value="<?php echo $row['fee_category_id']; ?>"><?php echo $row['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="exam_id" style="display: none;">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('select_term_exam'); ?></label>

                        <div class="col-sm-5">
                            <select name="exam_id" class="form-control" style="width:150px; float:left;">
                                <option>--Select term--</option>
                                <?php $categories = $this->db->get('exam')->result_array();
                                foreach ($categories as $row) {
                                    ?>
                                    <option
                                        value="<?php echo $row['exam_id']; ?>"><?php echo $row['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('fee_title'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="fee_title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2"
                               class="col-sm-3 control-label"><?php echo get_phrase('fee_description'); ?></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" id="field-ta" name="fee_description"
                                      placeholder="Textarea" rows="6"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" name="amount"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control datepicker" name="date" value=""
                                   data-start-date="-4w" data-end-date="+4w">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>
                        <div class="col-sm-5">
                            <select name="status" class="form-control" style="width:100%;">
                                <option value="1"><?php echo get_phrase('Paid'); ?></option>
                                <option value="2"><?php echo get_phrase('Unpaid'); ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit"
                                    class="btn btn-info"><?php echo get_phrase('issue_student_fee'); ?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS--->

        </div>
    </div>
</div>

<script>
    function feeType(value) {
        var fee_type = value.value;

        if (fee_type != 3)
        {
            $("#exam_id").hide();
        }
        else
        {
            $("#exam_id").show();
        }
    }
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="class_id"]').on('change', function () {
            var class_id = $(this).val();
            console.log(class_id);
            $('select[name="section_id"]').html('<option>select section</option>');
            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_class_id/' + class_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
//                    $('select[name="section_id"]').empty();
                    $.each(data, function (key, value) {

                        $('select[name="section_id"]').append('<option value="' + value.section_id + '">' + value.section_name + '</option>');
                    });
                }
            });


        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="section_id"]').on('change', function () {
            var section_id = $(this).val();
            console.log(section_id);

            $.ajax({
                url: '<?php echo base_url()?>index.php?admin_staff/get_student_id/' + section_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="student_id"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="student_id"]').append('<option value="' + value.student_id + '">' + value.name + '</option>');
                    });
                }
            });

        });
    });
</script>
