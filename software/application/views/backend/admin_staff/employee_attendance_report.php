<?php
$this->session->flashdata('message');
?>
<br><br>
<table class="table table-bordered datatable" id="table_export">
    <thead>
    <tr>
        <th width="20%">
            <div><?php echo get_phrase('employee_type'); ?></div>
        </th>
        <th width="20%">
            <div><?php echo get_phrase('employee_name'); ?></div>
        </th>
        <th>
            <div><?php echo get_phrase('date'); ?></div>
        </th>
        <th class="span3">
            <div><?php echo get_phrase('status'); ?></div>
        </th>
<!--        <th class="span3">-->
<!--            <div>--><?php //echo get_phrase('options'); ?><!--</div>-->
<!--        </th>-->
    </tr>
    </thead>
    <tbody>
    <?php
    if (is_array($attendance_info_admin) && count($attendance_info_admin) >= 1) {
    foreach ($attendance_info_admin as $row) {?>
        <tr>
            <td><?php if($row->employee_type =1) echo '<p style="color:#ff1c08"><b>Admin Staff</b></p>' ; ?></td>
            <td><?php echo $row->employee_name; ?></td>
            <td><?php $date = date("d-M-Y", $row->date); echo $date;?></td>
            <td><?php if($row->status == 1) {echo '<div class="label label-success">Present</div>';} else echo '<div class="label label-danger">Absent</div>'; ?></td>

        </tr>
    <?php } } ?>

  <?php

  if (is_array($attendance_info_teacher) && count($attendance_info_teacher) >= 1) {
  foreach ($attendance_info_teacher as $row) {?>
    <tr>
        <td><?php if($row->employee_type =2) echo '<p style="color:#00a6fc"><b>Teacher</b></p>' ; ?></td>
        <td><?php echo $row->employee_name; ?></td>
        <td><?php $date = date("d-M-y", $row->date); echo $date;?></td>
        <td><?php if($row->status == 1) {echo '<div class="label label-success">Present</div>';}  else echo '<div class="label label-danger">Absent</div>'; ?></td>

    </tr>
    <?php } } ?>

    <?php
    if (is_array($attendance_info_staff) && count($attendance_info_staff) >= 1) {
    foreach ($attendance_info_staff as $row) {?>
        <tr>
            <td><?php if($row->employee_type =3) echo '<p style="color:#00a65a"><b>Staff</b></p>' ; ?></td>
            <td><?php echo $row->employee_name; ?></td>
            <td><?php $date = date("d-M-Y", $row->date); echo $date;?></td>
            <td><?php if($row->status == 1) {echo '<div class="label label-success">Present</div>';}  else echo '<div class="label label-danger">Absent</div>'; ?></td>

        </tr>
    <?php } }?>
    </tbody>
</table>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ----->
<script type="text/javascript">

    jQuery(document).ready(function ($) {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [

                    {
                        "sExtends": "xls",
                        "mColumns": [0, 2, 3, 4]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [0, 2, 3, 4]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(1, false);
                            datatable.fnSetColumnVis(5, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(1, true);
                                    datatable.fnSetColumnVis(5, true);
                                }
                            });
                        },

                    },
                ]
            },

        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>