<?php
$this->session->flashdata('message');
?><div class="invoice">

    <div class="row">

        <div class="col-sm-6 invoice-left">

            <a href="#">
                <img src="<?php echo $this->crud_model->get_image_url('student', $student_id); ?>"
                     class="img-responsive img-circle"/>
            </a>

        </div>


        <div class="col-sm-6 invoice-right">

            <h3>TOTAL INVOICE #<?php echo $student_total_invoice; ?></h3>
            <span><b>TIME: </b><?php date_default_timezone_set('Asia/Dhaka');
                $today = date("F j, Y, g:i a");
                echo $today; ?></span>

        </div>

    </div>


    <hr class="margin"/>

    <?php foreach ($student_profile_details as $profile) { ?>
    <div class="row">

        <div class="col-sm-3 invoice-left">

            <h4><b style="color:red" ;>Profile Details</b></h4>
            Student Name: <?php echo $profile->name; ?>
            <br/>
            Email: <?php echo $profile->email; ?>
            <br/>
            Previous School: <?php echo $profile->previous_school; ?>

        </div>

        <div class="col-sm-3 invoice-left">

            <h4><b style="color:red" ;>Contact Details</b></h4>
            Address: <?php echo $profile->address; ?>
            <br/>
            Phone: <?php echo $profile->phone; ?>
            <br/>
        </div>

        <div class="col-md-6 invoice-right">
            <?php foreach ($student_parent_details as $parent) { ?>
                <h4><b style="color:red" ;>Parents Details:</b></h4>
                <strong>Parent Name:</strong> <?php echo $parent->name; ?>
                <br/>
                <strong>Relation:</strong> <?php echo $parent->relation_with_student; ?>
                <br/>
                <strong>Phone:</strong> <?php echo $parent->phone; ?>
                <br/>
                <strong>Address:</strong> <?php echo $parent->address; ?>
            <?php } ?>
        </div>

    </div>

    <div class="margin"></div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center">#ID</th>
            <th width="20%">Title</th>
            <th>Description</th>
            <th>Amount</th>
            <th>Due</th>
            <th>Buy Date</th>
            <th>Payment Status</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($student_invoice_details as $invoice) { ?>
            <tr>
                <td class="text-center"><?php echo $invoice->invoice_id; ?></td>
                <td><?php echo $invoice->title; ?></td>
                <td><?php echo $invoice->description; ?></td>
                <td><?php echo $invoice->amount . ' BDT'; ?></td>
                <td><?php echo $invoice->due . ' BDT'; ?></td>
                <td><?php echo date('d M,Y', $invoice->creation_timestamp); ?></td>
                <td class="text-center">
                    <?php
                    $status = $invoice->status;
                    if($status=='paid')
                    {
                        echo '<span class="label label-success">Paid</span>';
                    }
                    else
                    {
                        echo '<span class="label label-danger">Unpaid</span>';
                    }?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <div class="margin"></div>

    <div class="row">

        <div class="col-sm-6">

        </div>

        <div class="col-sm-6">

            <div class="invoice-right">

                <ul class="list-unstyled">
                    <li>
                        TOTAL PAID:
                        <strong><?php $paid = ($total_amount_by_student->total_amount - $total_due_by_student->due_amount);
                            echo $paid . ' BDT' ?></strong>
                    </li>
                    <li>
                        TOTAL DUE:
                        <strong><?php echo $total_due_by_student->due_amount . ' BDT'; ?></strong>
                    </li>
                    <li>
                        TOTAL AMOUNT:
                        <strong><?php echo $total_amount_by_student->total_amount . ' BDT'; ?></strong>
                    </li>
                </ul>

                <br/>

                <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
                    Print Invoice
                    <i class="entypo-doc-text"></i>
                </a>
                <?php } ?>
                &nbsp;

                <!--                <a href="mailbox-compose.html" class="btn btn-success btn-icon icon-left hidden-print">-->
                <!--                    Send Invoice-->
                <!--                    <i class="entypo-mail"></i>-->
                <!--                </a>-->
            </div>

        </div>

    </div>

</div>