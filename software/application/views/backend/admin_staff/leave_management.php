<?php
$this->session->flashdata('message');
?>
<a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_leave_add/');"
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('submit_leave_application');?>
</a>
<br><br>
<table class="table table-bordered datatable text-center" id="table_export">
    <thead>
    <tr>
        <!--        <th width="80"><div>--><?php //echo get_phrase('photo');?><!--</div></th>-->
        <th class="text-center"><div><?php echo get_phrase('staff_type');?></div></th>
        <th class="text-center" width="10%"><div><?php echo get_phrase('employee_name');?></div></th>
        <th class="text-center"><div><?php echo get_phrase('application_title');?></div></th>
        <th class="text-center" width="30%"><div><?php echo get_phrase('application_description');?></div></th>
        <th class="text-center"><div><?php echo get_phrase('leave_date');?></div></th>
        <th class="text-center"><div><?php echo get_phrase('rejoin_date');?></div></th>
        <th class="text-center"><div><?php echo get_phrase('leave_status');?></div></th>
        <th class="text-center"><div><?php echo get_phrase('options');?></div></th>
    </tr>
    </thead>
    <tbody>
    <?php

    //Admins Leave
    if (is_array($leave_for_admin) && count($leave_for_admin) >= 1) {
    foreach($leave_for_admin as $row):?>
        <tr>
            <td><?php if($row->employee_type = 1) echo '<p style="color:red"><b>Admin Staff</b></p>';?></td>
            <td><?php echo strtoupper($row->name);?></td>
            <td><?php echo $row->application_title;?></td>
            <td><?php echo $row->application_description;?></td>
            <td><?php echo date('d M,Y', strtotime($row->leave_date));?></td>
            <td><?php echo date('d M,Y', strtotime($row->rejoin_date));?></td>
            <td>
                <?php
                $status = $row->status;
                if($status==1)
                {
                    echo '<span class="label label-success">Approved</span>';
                }
                else
                {
                    echo '<span class="label label-danger">Pending</span>';
                }?>
            </td>
            <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!-- staff EDITING LINK -->
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_leave_edit/<?php echo $row->leave_id;?>');">
                                <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit');?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- staff DELETION LINK -->
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin_staff/leave/delete/<?php echo $row->leave_id;?>');">
                                <i class="entypo-trash"></i>
                                <?php echo get_phrase('delete');?>
                            </a>
                        </li>
                    </ul>
                </div>

            </td>
        </tr>
    <?php endforeach; }?>
    <?php
    //Teachers Leave
    if (is_array($leave_for_teacher) && count($leave_for_teacher) >= 1) {
    foreach($leave_for_teacher as $row):?>
        <tr>
            <td><?php if($row->employee_type = 2) echo '<p style="color:#00a6fc"><b>Teacher</b></p>';?></td>
            <td><?php echo strtoupper($row->name);?></td>
            <td><?php echo $row->application_title;?></td>
            <td><?php echo $row->application_description;?></td>
            <td><?php echo date('d M,Y', strtotime($row->leave_date));?></td>
            <td><?php echo date('d M,Y', strtotime($row->rejoin_date));?></td>
            <td>
                <?php
                $status = $row->status;
                if($status==1)
                {
                    echo '<span class="label label-success">Approved</span>';
                }
                else
                {
                    echo '<span class="label label-danger">Pending</span>';
                }?>
            </td>
            <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!-- staff EDITING LINK -->
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_leave_edit/<?php echo $row->leave_id;?>');">
                                <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit');?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- staff DELETION LINK -->
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin_staff/leave/delete/<?php echo $row->leave_id;?>');">
                                <i class="entypo-trash"></i>
                                <?php echo get_phrase('delete');?>
                            </a>
                        </li>
                    </ul>
                </div>

            </td>
        </tr>
    <?php endforeach; }?>

    <?php
    //Staffs Leave.
    if (is_array($leave_for_staff) && count($leave_for_staff) >= 1) {
    foreach($leave_for_staff as $row):?>
        <tr>
            <td><?php if($row->employee_type = 3) echo '<p style="color:#00a65a"><b>Staff</b></p>';?></td>
            <td><?php echo strtoupper($row->name);?></td>
            <td><?php echo $row->application_title;?></td>
            <td><?php echo $row->application_description;?></td>
            <td><?php echo date('d M,Y', strtotime($row->leave_date));?></td>
            <td><?php echo date('d M,Y', strtotime($row->rejoin_date));?></td>
            <td>
                <?php
                $status = $row->status;
                if($status==1)
                {
                    echo '<span class="label label-success"><b>Approved</b></span>';
                }
                else
                {
                    echo '<span class="label label-danger"><b>Pending</b></span>';
                }?>
            </td>
            <td>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!-- staff EDITING LINK -->
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_leave_edit/<?php echo $row->leave_id;?>');">
                                <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit');?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- staff DELETION LINK -->
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin_staff/leave/delete/<?php echo $row->leave_id;?>');">
                                <i class="entypo-trash"></i>
                                <?php echo get_phrase('delete');?>
                            </a>
                        </li>
                    </ul>
                </div>

            </td>
        </tr>
    <?php endforeach; }?>
    </tbody>
</table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ----->
<script type="text/javascript">

    jQuery(document).ready(function($)
    {


        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [

                    {
                        "sExtends": "xls",
                        "mColumns": [1,2]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1,2]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText"	   : "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(0, false);
                            datatable.fnSetColumnVis(3, false);

                            this.fnPrint( true, oConfig );

                            window.print();

                            $(window).keyup(function(e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(0, true);
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },

                    },
                ]
            },

        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>

