<div class="row">

    <?php if ($date1 == '' && $date2 == ''): ?>

        <div class="col-md-12">
            <div class="box-content">
                <?php echo form_open(base_url().'index.php?admin_staff/salary_report/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('first_date'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date1" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('last_date'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="date2" value=""
                               data-start-view="1">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('get_report'); ?></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($date1 != '' && $date2 != ''): ?>

        <div class="row">
            <div class="col-md-12">

                <div class="tile-stats tile-white-gray">
                    <div class="icon"><i class="entypo-suitcase"></i></div>
                    <h2><?php echo 'Salary Report Between ' . date('d/M/Y', strtotime($date1)) . ' - ' . date('d/M/Y', strtotime($date2)); ?></h2>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $this->session->flashdata('message');
            ?>
            <br><br>
            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th width="20%">
                        <div><?php echo get_phrase('employee_type'); ?></div>
                    </th>
                    <th width="20%">
                        <div><?php echo get_phrase('employee_name'); ?></div>
                    </th>

                    <th>
                        <div><?php echo get_phrase('salary'); ?></div>
                    </th>
                    <th>
                        <div><?php echo get_phrase('date'); ?></div>
                    </th>
                    <th class="span3">
                        <div><?php echo get_phrase('status'); ?></div>
                    </th>

                </tr>
                </thead>
                <tbody>
                <?php
                if (is_array($salary_report_admin) && count($salary_report_admin) >= 1) {
                    foreach ($salary_report_admin as $row) { ?>
                        <tr>
                            <td><?php if ($row->employee_type = 1) echo '<p style="color:#ff1c08"><b>Admin</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->amount . ' TK'; ?></td>
                            <td><?php $date = date("d-M-y", strtotime($row->date));
                                echo $date; ?></td>
                            <td><?php if ($row->status == 1) {
                                    echo '<div class="label label-success">Paid</div>';
                                } else echo '<div class="label label-danger">Absent</div>'; ?></td>

                        </tr>
                    <?php }
                } ?>

                <?php

                if (is_array($salary_report_teacher) && count($salary_report_teacher) >= 1) {
                    foreach ($salary_report_teacher as $row) { ?>
                        <tr>
                            <td><?php if ($row->employee_type = 2) echo '<p style="color:#00a6fc"><b>Teacher</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->amount . ' TK'; ?></td>
                            <td><?php $date = date("d-M-y", strtotime($row->date));
                                echo $date; ?></td>
                            <td><?php if ($row->status == 1) {
                                    echo '<div class="label label-success">Paid</div>';
                                } else echo '<div class="label label-danger">Unpaid</div>'; ?></td>

                        </tr>
                    <?php }
                } ?>

                <?php

                if (is_array($salary_report_staff) && count($salary_report_staff) >= 1) {
                    foreach ($salary_report_staff as $row) { ?>
                        <tr>
                            <td><?php if ($row->employee_type = 3) echo '<p style="color:#00a65a"><b>Staff</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->amount . ' TK'; ?></td>
                            <td><?php $date = date("d-M-y", strtotime($row->date));
                                echo $date; ?></td>
                            <td><?php if ($row->status == 1) {
                                    echo '<div class="label label-success">Paid</div>';
                                } else echo '<div class="label label-danger">Unpaid</div>'; ?></td>

                        </tr>
                    <?php }
                } ?>

                </tbody>
            </table>

            <table class="table table-bordered datatable">
                <thead>
                <tr>
                    <th width="33%">
                        <div><?php echo get_phrase('employee_type'); ?></div>
                    </th>
                    <th width="33%">
                        <div><?php echo get_phrase('employee_name'); ?></div>
                    </th>
                    </th>
                    <th class="span3">
                        <div><?php echo get_phrase('payment_status'); ?></div>
                    </th>

                </tr>
                </thead>
                <tbody>

                <!--Unpaid Status-->
                <?php

                if (is_array($unpaid_report_admin) && count($unpaid_report_admin) >= 1) {
                    foreach ($unpaid_report_admin as $row) { ?>
                        <tr>
                            <td><?php echo '<p style="color:#ff1c08"><b>Admin</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td>
                                <div class="label label-danger">Unpaid</div>
                            </td>

                        </tr>
                    <?php }
                } ?>

                <?php

                if (is_array($unpaid_report_teacher) && count($unpaid_report_teacher) >= 1) {
                    foreach ($unpaid_report_teacher as $row) { ?>
                        <tr>
                            <td><?php echo '<p style="color:#00a6fc"><b>Teacher</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td>
                                <div class="label label-danger">Unpaid</div>
                            </td>

                        </tr>
                    <?php }
                } ?>

                <?php

                if (is_array($unpaid_report_staff) && count($unpaid_report_staff) >= 1) {
                    foreach ($unpaid_report_staff as $row) { ?>
                        <tr>
                            <td><?php echo '<p style="color:#00a65a"><b>Staff</b></p>'; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td>
                                <div class="label label-danger">Unpaid</div>
                            </td>

                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>

            <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print pull-right">
                Print Salary Report
                <i class="entypo-doc-text"></i>
            </a>
        </div>
    <?php endif; ?>


</div>
